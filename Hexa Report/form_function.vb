﻿'This for importing MySql module to create a 
'connection between the Program And MySql Server
Imports MySql.Data.MySqlClient
Module form_function
    'Strings
    Public a1, b1, c1, c2, d1, d2, f1, g1, h1, i1, j1, k1, l1,
        m1, n1, o1, p1, q1, r1, s1, t1, u1, v1, w1, x1, y1, z1 As String
    Public query As String
    Public nam, kel, jur, lok, wal, nip, id_mapel, jenkel, account_id As String
    Public restoreMenu As Boolean = False
    Public pai, pkn, indo As String
    Public id(0 To 100) As String
    Public Sub getId_mapel(nextRow As Integer)
        query = "SELECT * FROM guru_mapel WHERE id_kelas = '" & kel & "' AND id_jurusan = '" & jur & "' AND id_lokal = '" & lok & "' AND id_mapel = (SELECT MIN(id_mapel) FROM guru_mapel WHERE id_mapel > '" & nextRow & "')"
        con.Open()
        cmd = New MySqlCommand(query, con)
        dr = cmd.ExecuteReader()

        Do While dr.Read()
            nip = (dr.Item("nip").ToString())
            id_mapel = (dr.Item("id_mapel").ToString())
        Loop
        dr.Close()
        cmd.Dispose()
        con.Close()
    End Sub

    Public Sub retRepData(nextItem As Integer, studentID As Integer)
        query = "SELECT master_siswa.nis, master_siswa.nisn, master_siswa.nama_siswa, kelas.kelas, kelas.rom, jurusan.jurusan, jurusan.jurusan_sngkt, lokal.lokal, master_siswa.nip, master_guru.nama, raport.id_mapel, raport.id_semester, raport.nilai_peng, raport.nilai_hurPeng, raport.nilai_ket, raport.nilai_hurKet
FROM master_siswa
INNER JOIN kelas ON kelas.id_kelas = master_siswa.id_kelas
INNER JOIN jurusan ON jurusan.id_jurusan = master_siswa.id_jurusan
INNER JOIN lokal ON lokal.id_lokal = master_siswa.id_lokal
INNER JOIN master_guru ON master_siswa.nip = master_guru.nip
INNER JOIN wali_kelas ON master_siswa.nip = wali_kelas.nip
INNER JOIN raport ON raport.nis = master_siswa.nis
WHERE wali_kelas.nip = '" & wal & "' AND raport.id_mapel = '" & nextItem & "' AND master_siswa.nis = '" & studentID & "'
ORDER BY master_siswa.nis ASC
LIMIT 1"
        con.Open()
        cmd = New MySqlCommand(query, con)
        dr = cmd.ExecuteReader()

        Do While dr.Read()
            a1 = (dr.Item("nis").ToString())
            t1 = (dr.Item("nisn").ToString())
            b1 = (dr.Item("nama_siswa").ToString())
            c1 = (dr.Item("kelas").ToString())
            c2 = (dr.Item("rom").ToString())
            d1 = (dr.Item("jurusan").ToString())
            d2 = (dr.Item("jurusan_sngkt").ToString())
            f1 = (dr.Item("lokal").ToString())
            g1 = (dr.Item("nip").ToString())
            h1 = (dr.Item("nama").ToString())
            i1 = (dr.Item("id_mapel").ToString())
            j1 = (dr.Item("id_semester").ToString())
            k1 = (dr.Item("nilai_peng").ToString())
            l1 = (dr.Item("nilai_hurPeng").ToString())
            m1 = (dr.Item("nilai_ket").ToString())
            n1 = (dr.Item("nilai_hurKet").ToString())
        Loop
        dr.Close()
        cmd.Dispose()
        con.Close()

        query = "SELECT * FROM supporting_grade WHERE nis = '" & studentID & "'"
        con.Open()
        cmd = New MySqlCommand(query, con)
        dr = cmd.ExecuteReader()

        Do While dr.Read()
            o1 = (dr.Item("ket_sakit").ToString())
            p1 = (dr.Item("ket_izin").ToString())
            q1 = (dr.Item("ket_alpha").ToString())
            r1 = (dr.Item("ekskul").ToString())
            s1 = (dr.Item("komen").ToString())
        Loop
        dr.Close()
        cmd.Dispose()
        con.Close()
    End Sub

    Public Sub prntRepData(id As String, subjectID As String)
        query = "SELECT * FROM raport WHERE nis = '" & id & "' AND id_mapel = '" & subjectID & "' ORDER BY nis, id_mapel ASC"
        con.Open()
        cmd = New MySqlCommand(query, con)
        dr = cmd.ExecuteReader()

        Do While dr.Read()
            a1 = (dr.Item("nis").ToString())
            i1 = (dr.Item("id_mapel").ToString())
            j1 = (dr.Item("id_semester").ToString())
            k1 = (dr.Item("nilai_peng").ToString())
            l1 = (dr.Item("nilai_hurPeng").ToString())
            m1 = (dr.Item("nilai_ket").ToString())
            n1 = (dr.Item("nilai_hurKet").ToString())
        Loop
        dr.Close()
        cmd.Dispose()
        con.Close()

        query = "SELECT * FROM supporting_grade WHERE nis = '" & id & "'"
        con.Open()
        cmd = New MySqlCommand(query, con)
        dr = cmd.ExecuteReader()

        Do While dr.Read()
            o1 = (dr.Item("ket_sakit").ToString())
            p1 = (dr.Item("ket_izin").ToString())
            q1 = (dr.Item("ket_alpha").ToString())
            r1 = (dr.Item("ekskul").ToString())
            s1 = (dr.Item("komen").ToString())
        Loop
        dr.Close()
        cmd.Dispose()
        con.Close()
    End Sub
End Module
