﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class home_form
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(home_form))
        Me.nisn = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label_tahun = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label_wali = New System.Windows.Forms.Label()
        Me.Label_kelas = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Panel_fill = New System.Windows.Forms.Panel()
        Me.MetroTabControl1 = New MetroFramework.Controls.MetroTabControl()
        Me.MetroTabPage1 = New MetroFramework.Controls.MetroTabPage()
        Me.ListView1 = New System.Windows.Forms.ListView()
        Me.nis = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.nama = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ttl = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.hp = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ayah = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.hp_ayah = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ibu = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.hp_ibu = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.MetroTabPage2 = New MetroFramework.Controls.MetroTabPage()
        Me.ListView2 = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Panel_top = New System.Windows.Forms.Panel()
        Me.PictureBox_loading = New System.Windows.Forms.PictureBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button_search = New System.Windows.Forms.Button()
        Me.TextBox_searchStudent = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Timer_dateTime = New System.Windows.Forms.Timer(Me.components)
        Me.Timer_autoRefresh_data = New System.Windows.Forms.Timer(Me.components)
        Me.Panel_footer = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label_dateTime = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Panel_fill.SuspendLayout()
        Me.MetroTabControl1.SuspendLayout()
        Me.MetroTabPage1.SuspendLayout()
        Me.MetroTabPage2.SuspendLayout()
        Me.Panel_top.SuspendLayout()
        CType(Me.PictureBox_loading, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel_footer.SuspendLayout()
        Me.SuspendLayout()
        '
        'nisn
        '
        Me.nisn.Text = "NISN"
        Me.nisn.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nisn.Width = 140
        '
        'Label_tahun
        '
        Me.Label_tahun.AutoSize = True
        Me.Label_tahun.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_tahun.Location = New System.Drawing.Point(145, 148)
        Me.Label_tahun.Name = "Label_tahun"
        Me.Label_tahun.Size = New System.Drawing.Size(58, 20)
        Me.Label_tahun.TabIndex = 12
        Me.Label_tahun.Text = "-tahun-"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(127, 148)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(12, 20)
        Me.Label12.TabIndex = 11
        Me.Label12.Text = ":"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(10, 148)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(111, 20)
        Me.Label13.TabIndex = 10
        Me.Label13.Text = "Tahun Pelajaran"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(145, 108)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(126, 20)
        Me.Label10.TabIndex = 9
        Me.Label10.Text = "SMKN 1 Cibinong"
        '
        'Label_wali
        '
        Me.Label_wali.AutoSize = True
        Me.Label_wali.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_wali.Location = New System.Drawing.Point(145, 65)
        Me.Label_wali.Name = "Label_wali"
        Me.Label_wali.Size = New System.Drawing.Size(48, 20)
        Me.Label_wali.TabIndex = 8
        Me.Label_wali.Text = "-wali-"
        '
        'Label_kelas
        '
        Me.Label_kelas.AutoSize = True
        Me.Label_kelas.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_kelas.Location = New System.Drawing.Point(145, 28)
        Me.Label_kelas.Name = "Label_kelas"
        Me.Label_kelas.Size = New System.Drawing.Size(54, 20)
        Me.Label_kelas.TabIndex = 7
        Me.Label_kelas.Text = "-kelas-"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(127, 108)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(12, 20)
        Me.Label9.TabIndex = 6
        Me.Label9.Text = ":"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(127, 65)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(12, 20)
        Me.Label8.TabIndex = 5
        Me.Label8.Text = ":"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(127, 28)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(12, 20)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = ":"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(10, 108)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(61, 20)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "Sekolah"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(10, 65)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(77, 20)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Wali Kelas"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(10, 28)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(44, 20)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Kelas"
        '
        'Panel_fill
        '
        Me.Panel_fill.BackColor = System.Drawing.Color.Transparent
        Me.Panel_fill.Controls.Add(Me.MetroTabControl1)
        Me.Panel_fill.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel_fill.Location = New System.Drawing.Point(0, 207)
        Me.Panel_fill.Name = "Panel_fill"
        Me.Panel_fill.Size = New System.Drawing.Size(1050, 354)
        Me.Panel_fill.TabIndex = 20
        '
        'MetroTabControl1
        '
        Me.MetroTabControl1.Controls.Add(Me.MetroTabPage1)
        Me.MetroTabControl1.Controls.Add(Me.MetroTabPage2)
        Me.MetroTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MetroTabControl1.FontWeight = MetroFramework.MetroTabControlWeight.Regular
        Me.MetroTabControl1.Location = New System.Drawing.Point(0, 0)
        Me.MetroTabControl1.Name = "MetroTabControl1"
        Me.MetroTabControl1.SelectedIndex = 0
        Me.MetroTabControl1.Size = New System.Drawing.Size(1050, 354)
        Me.MetroTabControl1.TabIndex = 0
        Me.MetroTabControl1.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MetroTabControl1.UseSelectable = True
        Me.MetroTabControl1.UseStyleColors = True
        '
        'MetroTabPage1
        '
        Me.MetroTabPage1.Controls.Add(Me.ListView1)
        Me.MetroTabPage1.HorizontalScrollbar = True
        Me.MetroTabPage1.HorizontalScrollbarBarColor = False
        Me.MetroTabPage1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroTabPage1.HorizontalScrollbarSize = 0
        Me.MetroTabPage1.Location = New System.Drawing.Point(4, 38)
        Me.MetroTabPage1.Name = "MetroTabPage1"
        Me.MetroTabPage1.Size = New System.Drawing.Size(1042, 312)
        Me.MetroTabPage1.TabIndex = 0
        Me.MetroTabPage1.Text = "Data Siswa-Siswi"
        Me.MetroTabPage1.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MetroTabPage1.VerticalScrollbar = True
        Me.MetroTabPage1.VerticalScrollbarBarColor = False
        Me.MetroTabPage1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroTabPage1.VerticalScrollbarSize = 0
        '
        'ListView1
        '
        Me.ListView1.BackColor = System.Drawing.SystemColors.Window
        Me.ListView1.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.nis, Me.nisn, Me.nama, Me.ttl, Me.hp, Me.ayah, Me.hp_ayah, Me.ibu, Me.hp_ibu})
        Me.ListView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ListView1.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.ListView1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.ListView1.FullRowSelect = True
        Me.ListView1.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.ListView1.Location = New System.Drawing.Point(0, 0)
        Me.ListView1.MultiSelect = False
        Me.ListView1.Name = "ListView1"
        Me.ListView1.Size = New System.Drawing.Size(1042, 312)
        Me.ListView1.TabIndex = 1
        Me.ListView1.UseCompatibleStateImageBehavior = False
        Me.ListView1.View = System.Windows.Forms.View.Details
        '
        'nis
        '
        Me.nis.Text = "NIS"
        Me.nis.Width = 140
        '
        'nama
        '
        Me.nama.Text = "NAMA SISWA"
        Me.nama.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nama.Width = 350
        '
        'ttl
        '
        Me.ttl.Text = "Tempat Tanggal Lahir"
        Me.ttl.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ttl.Width = 200
        '
        'hp
        '
        Me.hp.Text = "No. Handphone"
        Me.hp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.hp.Width = 170
        '
        'ayah
        '
        Me.ayah.Text = "Nama Ayah"
        Me.ayah.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ayah.Width = 350
        '
        'hp_ayah
        '
        Me.hp_ayah.Text = "No. Hp Ayah"
        Me.hp_ayah.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.hp_ayah.Width = 170
        '
        'ibu
        '
        Me.ibu.Text = "Nama Ibu"
        Me.ibu.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ibu.Width = 350
        '
        'hp_ibu
        '
        Me.hp_ibu.Text = "No. Hp Ibu"
        Me.hp_ibu.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.hp_ibu.Width = 170
        '
        'MetroTabPage2
        '
        Me.MetroTabPage2.Controls.Add(Me.ListView2)
        Me.MetroTabPage2.HorizontalScrollbarBarColor = False
        Me.MetroTabPage2.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroTabPage2.HorizontalScrollbarSize = 0
        Me.MetroTabPage2.Location = New System.Drawing.Point(4, 38)
        Me.MetroTabPage2.Name = "MetroTabPage2"
        Me.MetroTabPage2.Size = New System.Drawing.Size(1042, 312)
        Me.MetroTabPage2.TabIndex = 1
        Me.MetroTabPage2.Text = "Ranking Siswa-Siswi"
        Me.MetroTabPage2.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MetroTabPage2.VerticalScrollbarBarColor = False
        Me.MetroTabPage2.VerticalScrollbarHighlightOnWheel = False
        Me.MetroTabPage2.VerticalScrollbarSize = 0
        '
        'ListView2
        '
        Me.ListView2.BackColor = System.Drawing.SystemColors.Window
        Me.ListView2.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader4})
        Me.ListView2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ListView2.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.ListView2.FullRowSelect = True
        Me.ListView2.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.ListView2.Location = New System.Drawing.Point(0, 0)
        Me.ListView2.MultiSelect = False
        Me.ListView2.Name = "ListView2"
        Me.ListView2.Size = New System.Drawing.Size(1042, 312)
        Me.ListView2.TabIndex = 2
        Me.ListView2.UseCompatibleStateImageBehavior = False
        Me.ListView2.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Ranking"
        Me.ColumnHeader1.Width = 70
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "NIS"
        Me.ColumnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ColumnHeader2.Width = 200
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Nama Siswa"
        Me.ColumnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ColumnHeader3.Width = 350
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Total Nilai"
        Me.ColumnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ColumnHeader4.Width = 150
        '
        'Panel_top
        '
        Me.Panel_top.BackColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        Me.Panel_top.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel_top.Controls.Add(Me.PictureBox_loading)
        Me.Panel_top.Controls.Add(Me.Label_tahun)
        Me.Panel_top.Controls.Add(Me.Label3)
        Me.Panel_top.Controls.Add(Me.Label12)
        Me.Panel_top.Controls.Add(Me.Button_search)
        Me.Panel_top.Controls.Add(Me.Label13)
        Me.Panel_top.Controls.Add(Me.TextBox_searchStudent)
        Me.Panel_top.Controls.Add(Me.Label1)
        Me.Panel_top.Controls.Add(Me.Label10)
        Me.Panel_top.Controls.Add(Me.Label_wali)
        Me.Panel_top.Controls.Add(Me.Label_kelas)
        Me.Panel_top.Controls.Add(Me.Label9)
        Me.Panel_top.Controls.Add(Me.Label8)
        Me.Panel_top.Controls.Add(Me.Label7)
        Me.Panel_top.Controls.Add(Me.Label6)
        Me.Panel_top.Controls.Add(Me.Label5)
        Me.Panel_top.Controls.Add(Me.Label4)
        Me.Panel_top.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel_top.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel_top.ForeColor = System.Drawing.Color.White
        Me.Panel_top.Location = New System.Drawing.Point(0, 0)
        Me.Panel_top.Name = "Panel_top"
        Me.Panel_top.Size = New System.Drawing.Size(1050, 207)
        Me.Panel_top.TabIndex = 19
        '
        'PictureBox_loading
        '
        Me.PictureBox_loading.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox_loading.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox_loading.Image = CType(resources.GetObject("PictureBox_loading.Image"), System.Drawing.Image)
        Me.PictureBox_loading.Location = New System.Drawing.Point(797, 21)
        Me.PictureBox_loading.Name = "PictureBox_loading"
        Me.PictureBox_loading.Size = New System.Drawing.Size(45, 45)
        Me.PictureBox_loading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox_loading.TabIndex = 26
        Me.PictureBox_loading.TabStop = False
        Me.PictureBox_loading.Visible = False
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 11.25!)
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(847, 33)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(110, 20)
        Me.Label3.TabIndex = 25
        Me.Label3.Text = "Harap tunggu..."
        Me.Label3.Visible = False
        '
        'Button_search
        '
        Me.Button_search.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button_search.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer))
        Me.Button_search.BackgroundImage = Global.Hexa_Report.My.Resources.Resources.search_button
        Me.Button_search.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Button_search.FlatAppearance.BorderSize = 0
        Me.Button_search.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.Button_search.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(42, Byte), Integer), CType(CType(42, Byte), Integer))
        Me.Button_search.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_search.Location = New System.Drawing.Point(1004, 30)
        Me.Button_search.Name = "Button_search"
        Me.Button_search.Size = New System.Drawing.Size(40, 26)
        Me.Button_search.TabIndex = 24
        Me.Button_search.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.ToolTip1.SetToolTip(Me.Button_search, "Cari Data Siswa")
        Me.Button_search.UseVisualStyleBackColor = False
        '
        'TextBox_searchStudent
        '
        Me.TextBox_searchStudent.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox_searchStudent.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer))
        Me.TextBox_searchStudent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox_searchStudent.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.TextBox_searchStudent.ForeColor = System.Drawing.Color.White
        Me.TextBox_searchStudent.Location = New System.Drawing.Point(732, 28)
        Me.TextBox_searchStudent.Multiline = True
        Me.TextBox_searchStudent.Name = "TextBox_searchStudent"
        Me.TextBox_searchStudent.Size = New System.Drawing.Size(314, 30)
        Me.TextBox_searchStudent.TabIndex = 23
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 11.25!)
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(728, 5)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(149, 20)
        Me.Label1.TabIndex = 22
        Me.Label1.Text = "Pencarian Data Siswa"
        '
        'Timer_dateTime
        '
        Me.Timer_dateTime.Enabled = True
        '
        'Timer_autoRefresh_data
        '
        Me.Timer_autoRefresh_data.Enabled = True
        Me.Timer_autoRefresh_data.Interval = 5000
        '
        'Panel_footer
        '
        Me.Panel_footer.BackColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        Me.Panel_footer.Controls.Add(Me.Label2)
        Me.Panel_footer.Controls.Add(Me.Label_dateTime)
        Me.Panel_footer.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel_footer.Location = New System.Drawing.Point(0, 561)
        Me.Panel_footer.Name = "Panel_footer"
        Me.Panel_footer.Size = New System.Drawing.Size(1050, 39)
        Me.Panel_footer.TabIndex = 21
        '
        'Label2
        '
        Me.Label2.Dock = System.Windows.Forms.DockStyle.Left
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(0, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(180, 39)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Jumlah Siswa : 0 Orang"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label_dateTime
        '
        Me.Label_dateTime.Dock = System.Windows.Forms.DockStyle.Right
        Me.Label_dateTime.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label_dateTime.ForeColor = System.Drawing.Color.White
        Me.Label_dateTime.Location = New System.Drawing.Point(976, 0)
        Me.Label_dateTime.Name = "Label_dateTime"
        Me.Label_dateTime.Size = New System.Drawing.Size(74, 39)
        Me.Label_dateTime.TabIndex = 9
        Me.Label_dateTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'home_form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        Me.Controls.Add(Me.Panel_fill)
        Me.Controls.Add(Me.Panel_top)
        Me.Controls.Add(Me.Panel_footer)
        Me.Name = "home_form"
        Me.Size = New System.Drawing.Size(1050, 600)
        Me.Panel_fill.ResumeLayout(False)
        Me.MetroTabControl1.ResumeLayout(False)
        Me.MetroTabPage1.ResumeLayout(False)
        Me.MetroTabPage2.ResumeLayout(False)
        Me.Panel_top.ResumeLayout(False)
        Me.Panel_top.PerformLayout()
        CType(Me.PictureBox_loading, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel_footer.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents nisn As ColumnHeader
    Friend WithEvents Label_tahun As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label_wali As Label
    Friend WithEvents Label_kelas As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Panel_fill As Panel
    Friend WithEvents MetroTabControl1 As MetroFramework.Controls.MetroTabControl
    Friend WithEvents MetroTabPage1 As MetroFramework.Controls.MetroTabPage
    Friend WithEvents nis As ColumnHeader
    Friend WithEvents nama As ColumnHeader
    Friend WithEvents ttl As ColumnHeader
    Friend WithEvents hp As ColumnHeader
    Friend WithEvents ayah As ColumnHeader
    Friend WithEvents hp_ayah As ColumnHeader
    Friend WithEvents ibu As ColumnHeader
    Friend WithEvents hp_ibu As ColumnHeader
    Friend WithEvents MetroTabPage2 As MetroFramework.Controls.MetroTabPage
    Friend WithEvents ListView2 As ListView
    Friend WithEvents ColumnHeader1 As ColumnHeader
    Friend WithEvents ColumnHeader2 As ColumnHeader
    Friend WithEvents ColumnHeader3 As ColumnHeader
    Friend WithEvents ColumnHeader4 As ColumnHeader
    Friend WithEvents Panel_top As Panel
    Friend WithEvents Timer_dateTime As Timer
    Friend WithEvents Panel_footer As Panel
    Friend WithEvents Label2 As Label
    Friend WithEvents Label_dateTime As Label
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents Timer_autoRefresh_data As Timer
    Friend WithEvents ListView1 As ListView
    Friend WithEvents PictureBox_loading As PictureBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Button_search As Button
    Friend WithEvents TextBox_searchStudent As TextBox
    Friend WithEvents Label1 As Label
End Class
