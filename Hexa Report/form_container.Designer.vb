﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class form_container
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_container))
        Me.Panel_header = New System.Windows.Forms.Panel()
        Me.Button_minimize = New System.Windows.Forms.Button()
        Me.Button_minMax = New System.Windows.Forms.Button()
        Me.Button_closeProgram = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label_title = New System.Windows.Forms.Label()
        Me.Panel_container = New System.Windows.Forms.Panel()
        Me.Timer_minimizeFadeOut = New System.Windows.Forms.Timer(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Timer_closeAnimation = New System.Windows.Forms.Timer(Me.components)
        Me.Timer_loadAnimation = New System.Windows.Forms.Timer(Me.components)
        Me.NotifyIcon1 = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.Panel_header.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel_header
        '
        Me.Panel_header.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(9, Byte), Integer), CType(CType(9, Byte), Integer))
        Me.Panel_header.Controls.Add(Me.Button_minimize)
        Me.Panel_header.Controls.Add(Me.Button_minMax)
        Me.Panel_header.Controls.Add(Me.Button_closeProgram)
        Me.Panel_header.Controls.Add(Me.PictureBox1)
        Me.Panel_header.Controls.Add(Me.Label_title)
        Me.Panel_header.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel_header.Location = New System.Drawing.Point(0, 0)
        Me.Panel_header.Name = "Panel_header"
        Me.Panel_header.Size = New System.Drawing.Size(1200, 36)
        Me.Panel_header.TabIndex = 0
        '
        'Button_minimize
        '
        Me.Button_minimize.BackColor = System.Drawing.Color.Transparent
        Me.Button_minimize.Dock = System.Windows.Forms.DockStyle.Right
        Me.Button_minimize.FlatAppearance.BorderSize = 0
        Me.Button_minimize.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.Button_minimize.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(32, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(32, Byte), Integer))
        Me.Button_minimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_minimize.Image = CType(resources.GetObject("Button_minimize.Image"), System.Drawing.Image)
        Me.Button_minimize.Location = New System.Drawing.Point(1050, 0)
        Me.Button_minimize.Name = "Button_minimize"
        Me.Button_minimize.Size = New System.Drawing.Size(50, 36)
        Me.Button_minimize.TabIndex = 3
        Me.Button_minimize.TabStop = False
        Me.ToolTip1.SetToolTip(Me.Button_minimize, "Minimize")
        Me.Button_minimize.UseVisualStyleBackColor = False
        '
        'Button_minMax
        '
        Me.Button_minMax.BackColor = System.Drawing.Color.Transparent
        Me.Button_minMax.Dock = System.Windows.Forms.DockStyle.Right
        Me.Button_minMax.FlatAppearance.BorderSize = 0
        Me.Button_minMax.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.Button_minMax.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(32, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(32, Byte), Integer))
        Me.Button_minMax.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_minMax.Image = CType(resources.GetObject("Button_minMax.Image"), System.Drawing.Image)
        Me.Button_minMax.Location = New System.Drawing.Point(1100, 0)
        Me.Button_minMax.Name = "Button_minMax"
        Me.Button_minMax.Size = New System.Drawing.Size(50, 36)
        Me.Button_minMax.TabIndex = 4
        Me.Button_minMax.TabStop = False
        Me.ToolTip1.SetToolTip(Me.Button_minMax, "Restore Down")
        Me.Button_minMax.UseVisualStyleBackColor = False
        '
        'Button_closeProgram
        '
        Me.Button_closeProgram.BackColor = System.Drawing.Color.Transparent
        Me.Button_closeProgram.Dock = System.Windows.Forms.DockStyle.Right
        Me.Button_closeProgram.FlatAppearance.BorderSize = 0
        Me.Button_closeProgram.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(200, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button_closeProgram.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red
        Me.Button_closeProgram.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_closeProgram.Image = CType(resources.GetObject("Button_closeProgram.Image"), System.Drawing.Image)
        Me.Button_closeProgram.Location = New System.Drawing.Point(1150, 0)
        Me.Button_closeProgram.Name = "Button_closeProgram"
        Me.Button_closeProgram.Size = New System.Drawing.Size(50, 36)
        Me.Button_closeProgram.TabIndex = 2
        Me.Button_closeProgram.TabStop = False
        Me.ToolTip1.SetToolTip(Me.Button_closeProgram, "Close")
        Me.Button_closeProgram.UseVisualStyleBackColor = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Enabled = False
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(13, 3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(30, 30)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'Label_title
        '
        Me.Label_title.AutoSize = True
        Me.Label_title.Font = New System.Drawing.Font("Segoe UI", 9.5!)
        Me.Label_title.ForeColor = System.Drawing.Color.White
        Me.Label_title.Location = New System.Drawing.Point(49, 9)
        Me.Label_title.Name = "Label_title"
        Me.Label_title.Size = New System.Drawing.Size(39, 17)
        Me.Label_title.TabIndex = 1
        Me.Label_title.Text = "-title-"
        '
        'Panel_container
        '
        Me.Panel_container.BackColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        Me.Panel_container.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel_container.Location = New System.Drawing.Point(0, 36)
        Me.Panel_container.Name = "Panel_container"
        Me.Panel_container.Size = New System.Drawing.Size(1200, 664)
        Me.Panel_container.TabIndex = 1
        '
        'Timer_minimizeFadeOut
        '
        Me.Timer_minimizeFadeOut.Interval = 20
        '
        'Timer_closeAnimation
        '
        Me.Timer_closeAnimation.Interval = 20
        '
        'Timer_loadAnimation
        '
        Me.Timer_loadAnimation.Interval = 20
        '
        'NotifyIcon1
        '
        Me.NotifyIcon1.Icon = CType(resources.GetObject("NotifyIcon1.Icon"), System.Drawing.Icon)
        Me.NotifyIcon1.Text = "Hexa Report"
        Me.NotifyIcon1.Visible = True
        '
        'form_container
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1200, 700)
        Me.Controls.Add(Me.Panel_container)
        Me.Controls.Add(Me.Panel_header)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "form_container"
        Me.Opacity = 0R
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Login - Hexa Report"
        Me.Panel_header.ResumeLayout(False)
        Me.Panel_header.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel_header As Panel
    Friend WithEvents Button_closeProgram As Button
    Friend WithEvents Label_title As Label
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents Panel_container As Panel
    Friend WithEvents Timer_minimizeFadeOut As Timer
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents Button_minMax As Button
    Friend WithEvents Timer_closeAnimation As Timer
    Friend WithEvents Timer_loadAnimation As Timer
    Friend WithEvents Button_minimize As Button
    Public WithEvents NotifyIcon1 As NotifyIcon
End Class
