-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 24, 2016 at 11:43 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `database_raport`
--
CREATE DATABASE IF NOT EXISTS `database_raport` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `database_raport`;

-- --------------------------------------------------------

--
-- Table structure for table `guru_mapel`
--

CREATE TABLE IF NOT EXISTS `guru_mapel` (
  `nip` int(30) NOT NULL,
  `id_mapel` int(10) NOT NULL,
  `id_kelas` int(2) NOT NULL,
  `id_jurusan` int(2) NOT NULL,
  `id_lokal` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guru_mapel`
--

INSERT INTO `guru_mapel` (`nip`, `id_mapel`, `id_kelas`, `id_jurusan`, `id_lokal`) VALUES
(100, 1, 3, 1, 2),
(101, 2, 3, 1, 2),
(102, 3, 3, 1, 2),
(103, 4, 3, 1, 2),
(104, 5, 3, 1, 2),
(105, 6, 3, 1, 2),
(106, 7, 3, 1, 2),
(107, 8, 3, 1, 2),
(108, 9, 3, 1, 2),
(109, 10, 3, 1, 2),
(110, 24, 3, 1, 2),
(110, 25, 3, 1, 2),
(111, 26, 3, 1, 2),
(112, 27, 3, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `jurusan`
--

CREATE TABLE IF NOT EXISTS `jurusan` (
  `id_jurusan` int(2) NOT NULL,
  `jurusan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jurusan`
--

INSERT INTO `jurusan` (`id_jurusan`, `jurusan`) VALUES
(1, 'Rekayasa Perangkat Lunak'),
(2, 'Multimedia'),
(3, 'Teknik Komputer Jaringan'),
(4, 'Teknik Kendaraan Ringan'),
(5, 'Teknik Permesinan'),
(6, 'Teknik Otomasi Industri'),
(7, 'Teknik Gambar Sipil Arsitektur'),
(8, 'Teknik Konstruksi Bangunan');

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE IF NOT EXISTS `kelas` (
  `id_kelas` int(2) NOT NULL,
  `kelas` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `kelas`) VALUES
(1, 10),
(2, 11),
(3, 12);

-- --------------------------------------------------------

--
-- Table structure for table `lokal`
--

CREATE TABLE IF NOT EXISTS `lokal` (
  `id_lokal` int(2) NOT NULL,
  `lokal` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lokal`
--

INSERT INTO `lokal` (`id_lokal`, `lokal`) VALUES
(1, 1),
(2, 2),
(3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `master_guru`
--

CREATE TABLE IF NOT EXISTS `master_guru` (
`nip` int(30) NOT NULL,
  `nama` varchar(150) NOT NULL,
  `jk` enum('L','P') NOT NULL,
  `alamat` text NOT NULL,
  `tlpn` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=115 ;

--
-- Dumping data for table `master_guru`
--

INSERT INTO `master_guru` (`nip`, `nama`, `jk`, `alamat`, `tlpn`) VALUES
(100, 'Sukirjan', 'L', 'bogor', '1763491374619'),
(101, 'Sukirman', 'L', 'bogor', '1247180246702124'),
(102, 'Sumirna', 'P', 'cibinong', '73266134176741'),
(103, 'Maimunah', 'P', 'citeureup', '175613651369'),
(104, 'Mirjan', 'P', 'gfewu', '123456789'),
(105, 'Desu', 'P', 'gfewu', '123456789'),
(106, 'Wowo', 'L', 'gfewu', '123456789'),
(107, 'Desa', 'L', 'gfewu', '123456789'),
(108, 'Desi', 'P', 'gfewu', '123456789'),
(109, 'Deso', 'L', 'gfewu', '123456789'),
(110, 'Wakdoyok', 'L', 'gfewu', '123456789'),
(111, 'Saeb', 'L', 'gfewu', '123456789'),
(112, 'Suaeb', 'L', 'gfewu', '123456789'),
(113, 'Lili', 'P', 'gfewu', '123456789'),
(114, 'Lala', 'P', 'gfewu', '123456789');

-- --------------------------------------------------------

--
-- Table structure for table `master_mapel`
--

CREATE TABLE IF NOT EXISTS `master_mapel` (
`id_mapel` int(5) NOT NULL,
  `mapel` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `master_mapel`
--

INSERT INTO `master_mapel` (`id_mapel`, `mapel`) VALUES
(1, 'Pendidikan Agama dan Budi Pekerti'),
(2, 'Pendidikan Pancasila dan Kewarganegaraan'),
(3, 'Bahasa Indonesia'),
(4, 'Matematika'),
(5, 'Sejarah Indonesia'),
(6, 'Bahasa Inggris'),
(7, 'Seni Budaya'),
(8, 'Pendidikan Jasmani dan Olah Raga'),
(9, 'Prakarya Kewirausahaan'),
(10, 'Bahasa Mandarin'),
(11, 'Bahasa Sunda'),
(12, 'Bahasa Jepang'),
(13, 'Fisika'),
(14, 'Kimia'),
(15, 'Pemrograman Dasar'),
(16, 'Sistem Komputer'),
(17, 'Perakitan Komputer'),
(18, 'Simulasi Digital'),
(19, 'Sistem Operasi'),
(20, 'Jaringan Dasar'),
(21, 'Pemrograman Web'),
(22, 'Pemodelan Perangkat Lunak'),
(23, 'Pemrograman Desktop'),
(24, 'Pemrograman Berorientasi Obyek'),
(25, 'Basis Data'),
(26, 'Pemrograman Web Dinamis'),
(27, 'Pemrograman Grafik'),
(28, 'Pemrograman Perangkat Bergerak'),
(29, 'Administrasi Basis Data');

-- --------------------------------------------------------

--
-- Table structure for table `master_siswa`
--

CREATE TABLE IF NOT EXISTS `master_siswa` (
`nis` int(10) NOT NULL,
  `nisn` int(10) NOT NULL,
  `nama_siswa` varchar(200) NOT NULL,
  `tempat_lahir` text NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jk` enum('L','P') NOT NULL,
  `no_hp` int(14) NOT NULL,
  `nama_ayah` varchar(200) NOT NULL,
  `no_hp_ayah` int(14) NOT NULL,
  `nama_ibu` varchar(200) NOT NULL,
  `no_hp_ibu` int(14) NOT NULL,
  `alamat` text NOT NULL,
  `id_kelas` int(2) NOT NULL,
  `id_jurusan` int(2) NOT NULL,
  `id_lokal` int(2) NOT NULL,
  `nip` int(30) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=109 ;

--
-- Dumping data for table `master_siswa`
--

INSERT INTO `master_siswa` (`nis`, `nisn`, `nama_siswa`, `tempat_lahir`, `tgl_lahir`, `jk`, `no_hp`, `nama_ayah`, `no_hp_ayah`, `nama_ibu`, `no_hp_ibu`, `alamat`, `id_kelas`, `id_jurusan`, `id_lokal`, `nip`) VALUES
(100, 190, 'Aditya Bwonoharto', 'eew', '1999-11-01', 'L', 2147483647, 'yaya', 2147483647, 'yiyi', 2147483647, 'weffw', 3, 1, 2, 100),
(101, 212, 'Alif Nur Rachman', 'eew', '1999-11-01', 'L', 2147483647, 'jewg', 2147483647, 'dsk', 2147483647, 'weffw', 3, 1, 2, 100),
(102, 1012, 'Alif Yoga Darmawan', 'eew', '1999-11-01', 'L', 2147483647, 'gjorg', 2147483647, 'dyweu', 2147483647, 'weffw', 3, 1, 2, 100),
(103, 712, 'Alifia Dinda Novarry', 'eew', '1999-11-01', 'L', 2147483647, 'dfbi', 2147483647, 'kdgf', 2147483647, 'weffw', 3, 1, 2, 100),
(104, 99, 'Andi Juanda Putra Mapelawa', 'eew', '1999-11-01', 'L', 2147483647, 'geer', 2147483647, 'hfwe', 2147483647, 'weffw', 3, 1, 2, 100),
(105, 200, 'Andika Mardiansyah', 'eew', '1999-11-01', 'L', 2147483647, 'dgo', 2147483647, 'fywu', 2147483647, 'weffw', 3, 1, 2, 100),
(106, 2012, 'Awalludin Akbar', 'eew', '1999-11-01', 'L', 126410264, 'pqwoy', 2147483647, 'flji', 2147483647, 'weffw', 3, 1, 2, 100),
(107, 861, 'Cakra Sagiarta Lee', 'eew', '1999-11-01', 'L', 2147483647, 'sgka', 2147483647, 'osfga', 2147483647, 'weffw', 3, 1, 2, 100),
(108, 122, 'Daud Deniro Sitorus', 'eew', '1999-11-01', 'L', 2147483647, 'efh', 2147483647, 'jhfe', 2147483647, 'weffw', 3, 1, 2, 100);

-- --------------------------------------------------------

--
-- Table structure for table `raport`
--

CREATE TABLE IF NOT EXISTS `raport` (
  `nis` int(10) NOT NULL,
  `id_mapel` int(10) NOT NULL,
  `id_semester` int(1) NOT NULL,
  `nilai_peng` int(100) NOT NULL,
  `nilai_hurPeng` varchar(100) NOT NULL,
  `nilai_ket` int(100) NOT NULL,
  `nilai_hurKet` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `raport`
--

INSERT INTO `raport` (`nis`, `id_mapel`, `id_semester`, `nilai_peng`, `nilai_hurPeng`, `nilai_ket`, `nilai_hurKet`) VALUES
(100, 1, 5, 80, 'B', 80, 'B'),
(100, 2, 5, 80, 'B', 80, 'B'),
(100, 3, 5, 80, 'B', 80, 'B'),
(100, 4, 5, 80, 'B', 80, 'B'),
(100, 5, 5, 80, 'B', 80, 'B'),
(100, 6, 5, 80, 'B', 80, 'B'),
(100, 7, 5, 80, 'B', 80, 'B'),
(100, 8, 5, 80, 'B', 80, 'B'),
(100, 9, 5, 80, 'B', 80, 'B'),
(100, 10, 5, 80, 'B', 80, 'B'),
(100, 24, 5, 80, 'B', 80, 'B'),
(100, 25, 5, 80, 'B', 80, 'B'),
(100, 26, 5, 80, 'B', 80, 'B'),
(100, 27, 5, 80, 'B', 80, 'B'),
(101, 1, 5, 80, 'B', 80, 'B'),
(101, 2, 5, 80, 'B', 80, 'B'),
(101, 3, 5, 80, 'B', 80, 'B'),
(101, 4, 5, 80, 'B', 80, 'B'),
(101, 5, 5, 90, 'A', 90, 'A'),
(101, 6, 5, 90, 'A', 90, 'A'),
(101, 7, 5, 90, 'A', 90, 'A'),
(101, 8, 5, 90, 'A', 90, 'A'),
(101, 9, 5, 90, 'A', 90, 'A'),
(101, 10, 5, 90, 'A', 90, 'A'),
(101, 24, 5, 90, 'A', 90, 'A'),
(101, 25, 5, 90, 'A', 90, 'A'),
(101, 26, 5, 90, 'A', 90, 'A'),
(101, 27, 5, 90, 'A', 90, 'A'),
(102, 1, 5, 90, 'A', 90, 'A'),
(102, 2, 5, 90, 'A', 90, 'A'),
(102, 3, 5, 90, 'A', 90, 'A'),
(102, 4, 5, 90, 'A', 90, 'A'),
(102, 5, 5, 90, 'A', 90, 'A'),
(102, 6, 5, 90, 'A', 90, 'A'),
(102, 7, 5, 90, 'A', 90, 'A'),
(102, 8, 5, 90, 'A', 90, 'A'),
(102, 9, 5, 90, 'A', 90, 'A'),
(102, 10, 5, 90, 'A', 90, 'A'),
(102, 24, 5, 90, 'A', 90, 'A'),
(102, 25, 5, 90, 'A', 90, 'A'),
(102, 26, 5, 90, 'A', 90, 'A'),
(102, 27, 5, 90, 'A', 90, 'A');

-- --------------------------------------------------------

--
-- Table structure for table `semester`
--

CREATE TABLE IF NOT EXISTS `semester` (
`id_semester` int(1) NOT NULL,
  `semester` int(1) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `semester`
--

INSERT INTO `semester` (`id_semester`, `semester`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6);

-- --------------------------------------------------------

--
-- Table structure for table `supporting_grade`
--

CREATE TABLE IF NOT EXISTS `supporting_grade` (
  `nis` int(10) NOT NULL,
  `id_semester` int(1) NOT NULL,
  `ket_sakit` int(3) NOT NULL,
  `ket_izin` int(3) NOT NULL,
  `ket_alpha` int(3) NOT NULL,
  `ekskul` varchar(50) NOT NULL,
  `komen` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supporting_grade`
--

INSERT INTO `supporting_grade` (`nis`, `id_semester`, `ket_sakit`, `ket_izin`, `ket_alpha`, `ekskul`, `komen`) VALUES
(100, 5, 1, 1, 1, 'Rohis', '-'),
(101, 5, 3, 1, 0, 'Rohis', '-'),
(102, 5, 3, 3, 3, 'Paskibra', '-');

-- --------------------------------------------------------

--
-- Table structure for table `user_account`
--

CREATE TABLE IF NOT EXISTS `user_account` (
`user_id` int(100) NOT NULL,
  `username` varchar(150) NOT NULL,
  `pass` varchar(150) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user_account`
--

INSERT INTO `user_account` (`user_id`, `username`, `pass`) VALUES
(1, 'user123', '123456');

-- --------------------------------------------------------

--
-- Table structure for table `wali_kelas`
--

CREATE TABLE IF NOT EXISTS `wali_kelas` (
  `user_id` int(100) NOT NULL,
`nip` int(30) NOT NULL,
  `id_kelas` int(2) NOT NULL,
  `id_jurusan` int(2) NOT NULL,
  `id_lokal` int(2) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=101 ;

--
-- Dumping data for table `wali_kelas`
--

INSERT INTO `wali_kelas` (`user_id`, `nip`, `id_kelas`, `id_jurusan`, `id_lokal`) VALUES
(1, 100, 3, 1, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `guru_mapel`
--
ALTER TABLE `guru_mapel`
 ADD PRIMARY KEY (`nip`,`id_mapel`,`id_kelas`,`id_jurusan`,`id_lokal`), ADD KEY `id_kelas` (`id_kelas`), ADD KEY `id_jurusan` (`id_jurusan`), ADD KEY `id_lokal` (`id_lokal`), ADD KEY `id_mapel` (`id_mapel`);

--
-- Indexes for table `jurusan`
--
ALTER TABLE `jurusan`
 ADD PRIMARY KEY (`id_jurusan`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
 ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `lokal`
--
ALTER TABLE `lokal`
 ADD PRIMARY KEY (`id_lokal`);

--
-- Indexes for table `master_guru`
--
ALTER TABLE `master_guru`
 ADD PRIMARY KEY (`nip`);

--
-- Indexes for table `master_mapel`
--
ALTER TABLE `master_mapel`
 ADD PRIMARY KEY (`id_mapel`);

--
-- Indexes for table `master_siswa`
--
ALTER TABLE `master_siswa`
 ADD PRIMARY KEY (`nis`,`nisn`,`id_kelas`,`id_jurusan`,`id_lokal`,`nip`), ADD KEY `id_kelas` (`id_kelas`), ADD KEY `id_jurusan` (`id_jurusan`), ADD KEY `id_lokal` (`id_lokal`), ADD KEY `nip` (`nip`);

--
-- Indexes for table `raport`
--
ALTER TABLE `raport`
 ADD PRIMARY KEY (`nis`,`id_mapel`,`id_semester`), ADD KEY `id_mapel` (`id_mapel`), ADD KEY `id_semester` (`id_semester`);

--
-- Indexes for table `semester`
--
ALTER TABLE `semester`
 ADD PRIMARY KEY (`id_semester`);

--
-- Indexes for table `supporting_grade`
--
ALTER TABLE `supporting_grade`
 ADD PRIMARY KEY (`nis`,`id_semester`), ADD KEY `id_semester` (`id_semester`);

--
-- Indexes for table `user_account`
--
ALTER TABLE `user_account`
 ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `wali_kelas`
--
ALTER TABLE `wali_kelas`
 ADD PRIMARY KEY (`nip`,`id_kelas`,`id_jurusan`,`id_lokal`,`user_id`), ADD KEY `id_kelas` (`id_kelas`), ADD KEY `id_jurusan` (`id_jurusan`), ADD KEY `id_lokal` (`id_lokal`), ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `master_guru`
--
ALTER TABLE `master_guru`
MODIFY `nip` int(30) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=115;
--
-- AUTO_INCREMENT for table `master_mapel`
--
ALTER TABLE `master_mapel`
MODIFY `id_mapel` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `master_siswa`
--
ALTER TABLE `master_siswa`
MODIFY `nis` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=109;
--
-- AUTO_INCREMENT for table `semester`
--
ALTER TABLE `semester`
MODIFY `id_semester` int(1) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `user_account`
--
ALTER TABLE `user_account`
MODIFY `user_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wali_kelas`
--
ALTER TABLE `wali_kelas`
MODIFY `nip` int(30) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=101;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `guru_mapel`
--
ALTER TABLE `guru_mapel`
ADD CONSTRAINT `guru_mapel_ibfk_1` FOREIGN KEY (`nip`) REFERENCES `master_guru` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `guru_mapel_ibfk_10` FOREIGN KEY (`id_mapel`) REFERENCES `master_mapel` (`id_mapel`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `guru_mapel_ibfk_7` FOREIGN KEY (`id_kelas`) REFERENCES `kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `guru_mapel_ibfk_8` FOREIGN KEY (`id_jurusan`) REFERENCES `jurusan` (`id_jurusan`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `guru_mapel_ibfk_9` FOREIGN KEY (`id_lokal`) REFERENCES `lokal` (`id_lokal`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `master_siswa`
--
ALTER TABLE `master_siswa`
ADD CONSTRAINT `master_siswa_ibfk_1` FOREIGN KEY (`id_kelas`) REFERENCES `kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `master_siswa_ibfk_2` FOREIGN KEY (`id_jurusan`) REFERENCES `jurusan` (`id_jurusan`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `master_siswa_ibfk_3` FOREIGN KEY (`id_lokal`) REFERENCES `lokal` (`id_lokal`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `master_siswa_ibfk_4` FOREIGN KEY (`nip`) REFERENCES `wali_kelas` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `raport`
--
ALTER TABLE `raport`
ADD CONSTRAINT `raport_ibfk_1` FOREIGN KEY (`nis`) REFERENCES `master_siswa` (`nis`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `raport_ibfk_2` FOREIGN KEY (`id_mapel`) REFERENCES `guru_mapel` (`id_mapel`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `raport_ibfk_3` FOREIGN KEY (`id_semester`) REFERENCES `semester` (`id_semester`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `supporting_grade`
--
ALTER TABLE `supporting_grade`
ADD CONSTRAINT `supporting_grade_ibfk_1` FOREIGN KEY (`nis`) REFERENCES `raport` (`nis`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `supporting_grade_ibfk_2` FOREIGN KEY (`id_semester`) REFERENCES `semester` (`id_semester`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wali_kelas`
--
ALTER TABLE `wali_kelas`
ADD CONSTRAINT `wali_kelas_ibfk_3` FOREIGN KEY (`id_kelas`) REFERENCES `kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `wali_kelas_ibfk_4` FOREIGN KEY (`id_jurusan`) REFERENCES `jurusan` (`id_jurusan`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `wali_kelas_ibfk_5` FOREIGN KEY (`id_lokal`) REFERENCES `lokal` (`id_lokal`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `wali_kelas_ibfk_6` FOREIGN KEY (`nip`) REFERENCES `master_guru` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `wali_kelas_ibfk_7` FOREIGN KEY (`user_id`) REFERENCES `user_account` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
