﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class login_form
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(login_form))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBox_username = New System.Windows.Forms.TextBox()
        Me.TextBox_password = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Button_showPass = New System.Windows.Forms.Button()
        Me.Button_login = New System.Windows.Forms.Button()
        Me.Timer_dateTime = New System.Windows.Forms.Timer(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label_dateTime = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label_capsWarn = New System.Windows.Forms.Label()
        Me.Label_welcome = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox_loading = New System.Windows.Forms.PictureBox()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox_loading, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 16.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(474, 178)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(71, 30)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "HEXA"
        '
        'TextBox_username
        '
        Me.TextBox_username.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.TextBox_username.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer))
        Me.TextBox_username.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox_username.Font = New System.Drawing.Font("Segoe UI", 15.0!)
        Me.TextBox_username.ForeColor = System.Drawing.Color.White
        Me.TextBox_username.Location = New System.Drawing.Point(372, 264)
        Me.TextBox_username.Multiline = True
        Me.TextBox_username.Name = "TextBox_username"
        Me.TextBox_username.Size = New System.Drawing.Size(350, 35)
        Me.TextBox_username.TabIndex = 1
        Me.TextBox_username.Tag = "Username anda"
        '
        'TextBox_password
        '
        Me.TextBox_password.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.TextBox_password.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer))
        Me.TextBox_password.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox_password.Font = New System.Drawing.Font("Segoe UI", 15.0!)
        Me.TextBox_password.ForeColor = System.Drawing.Color.White
        Me.TextBox_password.Location = New System.Drawing.Point(372, 350)
        Me.TextBox_password.Name = "TextBox_password"
        Me.TextBox_password.PasswordChar = Global.Microsoft.VisualBasic.ChrW(8226)
        Me.TextBox_password.Size = New System.Drawing.Size(350, 34)
        Me.TextBox_password.TabIndex = 2
        Me.TextBox_password.Tag = "Password anda"
        '
        'Label2
        '
        Me.Label2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(369, 236)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(71, 19)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Username"
        '
        'Label3
        '
        Me.Label3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(369, 321)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(67, 19)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Password"
        '
        'Button_showPass
        '
        Me.Button_showPass.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Button_showPass.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer))
        Me.Button_showPass.BackgroundImage = Global.Hexa_Report.My.Resources.Resources.showPass_button
        Me.Button_showPass.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Button_showPass.FlatAppearance.BorderSize = 0
        Me.Button_showPass.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.Button_showPass.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(32, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(32, Byte), Integer))
        Me.Button_showPass.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_showPass.Location = New System.Drawing.Point(653, 351)
        Me.Button_showPass.Name = "Button_showPass"
        Me.Button_showPass.Size = New System.Drawing.Size(34, 32)
        Me.Button_showPass.TabIndex = 15
        Me.ToolTip1.SetToolTip(Me.Button_showPass, "Tampilkan Password")
        Me.Button_showPass.UseVisualStyleBackColor = False
        Me.Button_showPass.Visible = False
        '
        'Button_login
        '
        Me.Button_login.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Button_login.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer))
        Me.Button_login.BackgroundImage = CType(resources.GetObject("Button_login.BackgroundImage"), System.Drawing.Image)
        Me.Button_login.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Button_login.FlatAppearance.BorderSize = 0
        Me.Button_login.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.Button_login.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(32, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(32, Byte), Integer))
        Me.Button_login.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_login.Location = New System.Drawing.Point(687, 351)
        Me.Button_login.Name = "Button_login"
        Me.Button_login.Size = New System.Drawing.Size(34, 32)
        Me.Button_login.TabIndex = 16
        Me.ToolTip1.SetToolTip(Me.Button_login, "Login")
        Me.Button_login.UseVisualStyleBackColor = False
        '
        'Timer_dateTime
        '
        Me.Timer_dateTime.Enabled = True
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.Controls.Add(Me.Label_dateTime)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 561)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1050, 39)
        Me.Panel1.TabIndex = 10
        '
        'Label_dateTime
        '
        Me.Label_dateTime.Dock = System.Windows.Forms.DockStyle.Right
        Me.Label_dateTime.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label_dateTime.ForeColor = System.Drawing.Color.White
        Me.Label_dateTime.Location = New System.Drawing.Point(976, 0)
        Me.Label_dateTime.Name = "Label_dateTime"
        Me.Label_dateTime.Size = New System.Drawing.Size(74, 39)
        Me.Label_dateTime.TabIndex = 9
        Me.Label_dateTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 16.0!)
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(537, 178)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(78, 30)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Report"
        '
        'Label_capsWarn
        '
        Me.Label_capsWarn.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label_capsWarn.AutoSize = True
        Me.Label_capsWarn.BackColor = System.Drawing.Color.Transparent
        Me.Label_capsWarn.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label_capsWarn.ForeColor = System.Drawing.Color.White
        Me.Label_capsWarn.Location = New System.Drawing.Point(420, 389)
        Me.Label_capsWarn.Name = "Label_capsWarn"
        Me.Label_capsWarn.Size = New System.Drawing.Size(244, 19)
        Me.Label_capsWarn.TabIndex = 12
        Me.Label_capsWarn.Text = "Tombol Caps Lock sedang menyala"
        Me.Label_capsWarn.Visible = False
        '
        'Label_welcome
        '
        Me.Label_welcome.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label_welcome.AutoSize = True
        Me.Label_welcome.BackColor = System.Drawing.Color.Transparent
        Me.Label_welcome.Font = New System.Drawing.Font("Segoe UI", 16.0!)
        Me.Label_welcome.ForeColor = System.Drawing.Color.White
        Me.Label_welcome.Location = New System.Drawing.Point(483, 300)
        Me.Label_welcome.Name = "Label_welcome"
        Me.Label_welcome.Size = New System.Drawing.Size(165, 30)
        Me.Label_welcome.TabIndex = 13
        Me.Label_welcome.Text = "Selamat Datang"
        Me.Label_welcome.Visible = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(60, 191)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(264, 245)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 7
        Me.PictureBox1.TabStop = False
        '
        'PictureBox_loading
        '
        Me.PictureBox_loading.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PictureBox_loading.Image = CType(resources.GetObject("PictureBox_loading.Image"), System.Drawing.Image)
        Me.PictureBox_loading.Location = New System.Drawing.Point(422, 292)
        Me.PictureBox_loading.Name = "PictureBox_loading"
        Me.PictureBox_loading.Size = New System.Drawing.Size(50, 50)
        Me.PictureBox_loading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox_loading.TabIndex = 14
        Me.PictureBox_loading.TabStop = False
        Me.PictureBox_loading.Visible = False
        '
        'login_form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Controls.Add(Me.Button_showPass)
        Me.Controls.Add(Me.Button_login)
        Me.Controls.Add(Me.Label_capsWarn)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TextBox_password)
        Me.Controls.Add(Me.TextBox_username)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PictureBox_loading)
        Me.Controls.Add(Me.Label_welcome)
        Me.DoubleBuffered = True
        Me.Name = "login_form"
        Me.Size = New System.Drawing.Size(1050, 600)
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox_loading, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents TextBox_password As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents Timer_dateTime As Timer
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label_dateTime As Label
    Friend WithEvents TextBox_username As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label_capsWarn As Label
    Friend WithEvents Label_welcome As Label
    Friend WithEvents PictureBox_loading As PictureBox
    Friend WithEvents Button_showPass As Button
    Friend WithEvents Button_login As Button
End Class
