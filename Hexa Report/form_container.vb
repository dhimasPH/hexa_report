﻿Public Class form_container
    Dim restoreForm As Boolean = False
    Dim x, y As Integer
    Dim NewLocation As New System.Drawing.Point
    Dim opening = False
    Dim max As Boolean = False
    Dim min As Boolean = False

    Private Sub form_container_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'This is for loading the notify balloon which will appeared on system tray
        SetBalloonTip()

        'The form will automatically fullscreen when its opened
        Me.WindowState = FormWindowState.Maximized

        'A boolean for opening animation
        opening = False

        Dim log As New login_form()
        'The form_container will handle, load, and display the login_form
        Me.Panel_container.Controls.Add(log)
        log.Focus()
    End Sub

    Public Sub SetBalloonTip()
        'To set the notify so it can be displayed later
        NotifyIcon1.BalloonTipIcon = ToolTipIcon.None
        NotifyIcon1.BalloonTipTitle = "HEXA Report"
    End Sub

    Private Sub Button_closeProgram_Click(sender As Object, e As EventArgs) Handles Button_closeProgram.Click
        If MsgBox("Anda yakin ingin menutup program?", vbQuestion + vbYesNo, "HEXA Report") = vbYes Then
            If opening = False Then
                'We dispose the Timer_loadAnimation so the load animation won't start
                NotifyIcon1.Dispose()
                Timer_loadAnimation.Dispose()

                'This is the animation to trigger the form to close
                Timer_closeAnimation.Start()
            End If
        Else
            Exit Sub
        End If
    End Sub

    Private Sub Timer_minimizeFadeOut_Tick(sender As Object, e As EventArgs) Handles Timer_minimizeFadeOut.Tick
        'This is the minimize animation
        If Me.Opacity <= 1.0 Then
            Me.Opacity = Me.Opacity - 0.1
            If Me.Opacity = 0 Then
                Timer_minimizeFadeOut.Stop()
                Me.WindowState = FormWindowState.Minimized
                Me.Opacity = 100
            End If
        End If
    End Sub

    Private Sub Timer_closeAnimation_Tick(sender As Object, e As EventArgs) Handles Timer_closeAnimation.Tick
        Me.Opacity = Me.Opacity - 0.1
        If Me.Opacity = 0 Then
            'This is the animation to trigger the form to close
            Timer_closeAnimation.Stop()

            'The "End" statement is used to close the Program properly
            End
        End If
    End Sub

    Private Sub Button_minimize_Click(sender As Object, e As EventArgs) Handles Button_minimize.Click
        If opening = False Then
            'We dispose the Timer_loadAnimation so the load animation won't start
            Timer_loadAnimation.Dispose()

            'This is the animation to trigger the form to minimize
            Timer_minimizeFadeOut.Start()
        End If
    End Sub

    Private Sub Button_minMax_Click(sender As Object, e As EventArgs) Handles Button_minMax.Click
        If restoreForm = False Then
            'Me.Invalidate()
            ToolTip1.SetToolTip(Button_minMax, "Maximize")
            Button_minMax.Image = My.Resources.Max_icon
            Me.WindowState = FormWindowState.Normal
            restoreForm = True
        ElseIf restoreForm = True Then
            'Me.Invalidate()
            ToolTip1.SetToolTip(Button_minMax, "Restore Down")
            Button_minMax.Image = My.Resources.Min_icon
            Me.WindowState = FormWindowState.Maximized
            restoreForm = False
        End If
    End Sub

    Private Sub Panel_header_MouseMove(sender As Object, e As MouseEventArgs) Handles Panel_header.MouseMove
        'This syntax is used to make the form draggable
        If e.Button = MouseButtons.Left Then
            NewLocation = Control.MousePosition
            NewLocation.X -= (x)
            NewLocation.Y -= (y)
            Me.Location = NewLocation
        End If
    End Sub

    Private Sub Panel_header_DoubleClick(sender As Object, e As EventArgs) Handles Panel_header.DoubleClick
        'Maximize/minimize form function
        If Me.WindowState = FormWindowState.Maximized Then
            ToolTip1.SetToolTip(Button_minMax, "Maximize")
            Button_minMax.Image = My.Resources.Max_icon
            Me.WindowState = FormWindowState.Normal
            restoreForm = True
        ElseIf Me.WindowState = FormWindowState.Normal Then
            ToolTip1.SetToolTip(Button_minMax, "Restore Down")
            Button_minMax.Image = My.Resources.Min_icon
            Me.WindowState = FormWindowState.Maximized
            restoreForm = False
        End If
    End Sub

    Private Sub form_container_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        'This is the animation to trigger the form to open
        Timer_loadAnimation.Start()
    End Sub

    Private Sub Timer_loadAnimation_Tick(sender As Object, e As EventArgs) Handles Timer_loadAnimation.Tick
        'This is the load animation
        Me.Opacity = Me.Opacity + 0.1
        If Me.Opacity = 100 Then
            Timer_closeAnimation.Stop()
            opening = True
        End If
    End Sub

    Private Sub Label_title_MouseDown(sender As Object, e As MouseEventArgs) Handles Label_title.MouseDown
        'This to locate the cursor
        x = Control.MousePosition.X - Me.Location.X
        y = Control.MousePosition.Y - Me.Location.Y
    End Sub

    Private Sub Label_title_MouseMove(sender As Object, e As MouseEventArgs) Handles Label_title.MouseMove
        'When mouse left button clicked, the form
        'will be moved based on the cursor
        If e.Button = MouseButtons.Left Then
            NewLocation = Control.MousePosition
            NewLocation.X -= (x)
            NewLocation.Y -= (y)
            Me.Location = NewLocation
        End If
    End Sub

    Private Sub Label_title_DoubleClick(sender As Object, e As EventArgs) Handles Label_title.DoubleClick
        'Maximize/minimize form function
        If Me.WindowState = FormWindowState.Maximized Then
            ToolTip1.SetToolTip(Button_minMax, "Maximize")
            Button_minMax.Image = My.Resources.Max_icon
            Me.WindowState = FormWindowState.Normal
            restoreForm = True
        ElseIf Me.WindowState = FormWindowState.Normal Then
            ToolTip1.SetToolTip(Button_minMax, "Restore Down")
            Button_minMax.Image = My.Resources.Min_icon
            Me.WindowState = FormWindowState.Maximized
            restoreForm = False
        End If
    End Sub

    'Private Sub form_container_Paint(sender As Object, e As PaintEventArgs) Handles MyBase.Paint
    '    Dim rect As New Rectangle(0, 0, Width, Height)

    '    If rect.Width > 0 And rect.Height > 0 Then
    '        Dim c1 As Color = Color.FromArgb(21, 28, 255)
    '        Dim c2 As Color = Color.FromArgb(0, 255, 93)
    '        Dim c3 As Color = Color.FromArgb(108, 77, 232)

    '        Dim grad As New Drawing2D.LinearGradientBrush(rect, c1, c2, Drawing2D.LinearGradientMode.BackwardDiagonal)
    '        e.Graphics.FillRectangle(grad, rect)
    '    End If
    'End Sub

    Private Sub Panel_header_MouseDown(sender As Object, e As MouseEventArgs) Handles Panel_header.MouseDown
        'This to locate the cursor
        x = Control.MousePosition.X - Me.Location.X
        y = Control.MousePosition.Y - Me.Location.Y
    End Sub
End Class
