﻿Imports MySql.Data.MySqlClient
Public Class home_form
    Public a, b, kls, jrs, lkl As String
    Public Sub searchData()
        Label1.Visible = False
        TextBox_searchStudent.Visible = False
        Button_search.Visible = False
        Label3.Visible = True
        PictureBox_loading.Visible = True
        query = "SELECT * FROM master_siswa WHERE nis LIKE '" & TextBox_searchStudent.Text.Replace("'", "''") & "' AND id_kelas = '" & kel & "' AND id_jurusan = '" & jur & "' AND id_lokal = '" & lok & "' AND nip = '" & wal & "' OR nisn LIKE '" & TextBox_searchStudent.Text.Replace("'", "''") & "'" _
            & "AND id_kelas = '" & kel & "' AND id_jurusan = '" & jur & "' AND id_lokal = '" & lok & "' AND nip = '" & wal & "' OR nama_siswa LIKE '" & TextBox_searchStudent.Text.Replace("'", "''") & "' AND id_kelas = '" & kel & "' AND id_jurusan = '" & jur & "' AND id_lokal = '" & lok & "' AND nip = '" & wal & "' OR tempat_lahir LIKE '" & TextBox_searchStudent.Text.Replace("'", "''") & "' AND id_kelas = '" & kel & "' AND id_jurusan = '" & jur & "' AND id_lokal = '" & lok & "' AND nip = '" & wal & "' OR tgl_lahir LIKE '" & TextBox_searchStudent.Text.Replace("'", "''") & "'" _
            & "AND id_kelas = '" & kel & "' AND id_jurusan = '" & jur & "' AND id_lokal = '" & lok & "' AND nip = '" & wal & "' OR no_hp LIKE '" & TextBox_searchStudent.Text.Replace("'", "''") & "' AND id_kelas = '" & kel & "' AND id_jurusan = '" & jur & "' AND id_lokal = '" & lok & "' AND nip = '" & wal & "' OR nama_ayah LIKE '" & TextBox_searchStudent.Text.Replace("'", "''") & "' AND id_kelas = '" & kel & "' AND id_jurusan = '" & jur & "' AND id_lokal = '" & lok & "' AND nip = '" & wal & "' OR no_hp_ayah LIKE '" & TextBox_searchStudent.Text.Replace("'", "''") & "'" _
            & "AND id_kelas = '" & kel & "' AND id_jurusan = '" & jur & "' AND id_lokal = '" & lok & "' AND nip = '" & wal & "' OR nama_ibu LIKE '" & TextBox_searchStudent.Text.Replace("'", "''") & "' AND id_kelas = '" & kel & "' AND id_jurusan = '" & jur & "' AND id_lokal = '" & lok & "' AND nip = '" & wal & "' OR no_hp_ibu LIKE '" & TextBox_searchStudent.Text.Replace("'", "''") & "' AND id_kelas = '" & kel & "' AND id_jurusan = '" & jur & "' AND id_lokal = '" & lok & "' AND nip = '" & wal & "' OR alamat LIKE '" & TextBox_searchStudent.Text.Replace("'", "''") & "'" _
            & "AND id_kelas = '" & kel & "' AND id_jurusan = '" & jur & "' AND id_lokal = '" & lok & "' AND nip = '" & wal & "'"
        con.Open()
        cmd = New MySqlCommand(query, con)
        dr = cmd.ExecuteReader()
        ListView1.Items.Clear()

        If dr.HasRows() Then
            Do While dr.Read()
                Dim a, b, c, d, f, h, i, j, k, l As String

                a = (dr.Item("nis").ToString())
                b = (dr.Item("nisn").ToString())
                c = (dr.Item("nama_siswa").ToString())
                d = (dr.Item("tempat_lahir").ToString())
                f = (dr.Item("tgl_lahir").ToString())
                h = (dr.Item("no_hp").ToString())
                i = (dr.Item("nama_ayah").ToString())
                j = (dr.Item("no_hp_ayah").ToString())
                k = (dr.Item("nama_ibu").ToString())
                l = (dr.Item("no_hp_ibu").ToString())


                Dim tdItem As ListViewItem = ListView1.Items.Add(a)
                tdItem.SubItems.Add(b)
                tdItem.SubItems.Add(c)
                tdItem.SubItems.Add(d & ", " & f)
                tdItem.SubItems.Add(h)
                tdItem.SubItems.Add(i)
                tdItem.SubItems.Add(j)
                tdItem.SubItems.Add(k)
                tdItem.SubItems.Add(l)
            Loop
            dr.Close()
            cmd.Dispose()
            con.Close()
            Label3.Visible = False
            PictureBox_loading.Visible = False
            Label1.Visible = True
            TextBox_searchStudent.Visible = True
            Button_search.Visible = True
            TextBox_searchStudent.Focus()
        Else
            dr.Close()
            cmd.Dispose()
            con.Close()
            Label3.Visible = False
            PictureBox_loading.Visible = False
            Label1.Visible = True
            TextBox_searchStudent.Visible = True
            Button_search.Visible = True
            TextBox_searchStudent.Focus()
            TextBox_searchStudent.Clear()
            MsgBox("Hasil pencarian tidak ditemukan.", MsgBoxStyle.Information, "HEXA Report")
        End If
    End Sub

    Public Sub classInfo()
        query = "SELECT wali_kelas.nip, master_guru.nama, master_guru.jk, kelas.rom, jurusan.jurusan_sngkt, lokal.lokal 
FROM wali_kelas 
INNER JOIN master_guru ON wali_kelas.nip = master_guru.nip 
INNER JOIN user_account ON wali_kelas.user_id = user_account.user_id
INNER JOIN kelas ON kelas.id_kelas = wali_kelas.id_kelas
INNER JOIN jurusan ON jurusan.id_jurusan = wali_kelas.id_jurusan
INNER JOIN lokal ON lokal.id_lokal = wali_kelas.id_lokal
WHERE user_account.user_id = '" & account_id & "'"
        con.Open()
        cmd = New MySqlCommand(query, con)
        dr = cmd.ExecuteReader()

        If dr.HasRows() Then
            Do While dr.Read()
                nam = (dr.Item("nama").ToString())
                kls = (dr.Item("rom").ToString())
                jrs = (dr.Item("jurusan_sngkt").ToString())
                lkl = (dr.Item("lokal").ToString())
                wal = (dr.Item("nip").ToString())
                jenkel = (dr.Item("jk").ToString())
            Loop
            Label_kelas.Text = kls & " " & jrs & " " & lkl
            Label_wali.Text = nam
            dr.Close()
            cmd.Dispose()
            con.Close()
        End If
    End Sub

    Public Sub rankingScore()
        query = "SELECT raport.nis, master_siswa.nama_siswa, master_siswa.nip, master_siswa.id_kelas, master_siswa.id_jurusan, master_siswa.id_lokal, SUM(raport.nilai_peng) AS Total
FROM raport
INNER JOIN master_siswa ON master_siswa.nis = raport.nis
WHERE master_siswa.nip = '" & wal & "'
GROUP BY raport.nis
ORDER BY Total DESC"
        con.Open()
        cmd = New MySqlCommand(query, con)
        dr = cmd.ExecuteReader()
        ListView2.Items.Clear()

        If dr.HasRows() Then
            Do While dr.Read()
                Dim Totrank As Integer = 0
                Dim nis, nama, nip, kelas, jurusan, lokal, total As String

                nis = (dr.Item("nis").ToString())
                nama = (dr.Item("nama_siswa").ToString())
                nip = (dr.Item("nip").ToString())
                kelas = (dr.Item("id_kelas").ToString())
                jurusan = (dr.Item("id_jurusan").ToString())
                lokal = (dr.Item("id_lokal").ToString())
                total = (dr.Item("Total").ToString())

                For rank As Integer = 0 To ListView2.Items.Count
                    Totrank = Totrank + 1
                Next

                Dim tdItem As ListViewItem = ListView2.Items.Add(Totrank)
                tdItem.SubItems.Add(nis)
                tdItem.SubItems.Add(nama)
                tdItem.SubItems.Add(total)

            Loop
            dr.Close()
            cmd.Dispose()
            con.Close()
        Else
            ListView2.Items.Clear()
            MsgBox("Ranking kosong. Tidak ada data", MsgBoxStyle.Information, "HEXA Report")
            dr.Close()
            cmd.Dispose()
            con.Close()
        End If
    End Sub

    Public Sub getData()
        query = "SELECT * FROM master_siswa WHERE id_kelas = '" & kel & "' AND id_jurusan = '" & jur & "' AND id_lokal = '" _
            & lok & "'"
        con.Open()
        cmd = New MySqlCommand(query, con)
        dr = cmd.ExecuteReader()
        ListView1.Items.Clear()

        Do While dr.Read()
            Dim a, b, c, d, f, h, i, j, k, l As String

            a = (dr.Item("nis").ToString())
            b = (dr.Item("nisn").ToString())
            c = (dr.Item("nama_siswa").ToString())
            d = (dr.Item("tempat_lahir").ToString())
            f = (dr.Item("tgl_lahir").ToString())
            h = (dr.Item("no_hp").ToString())
            i = (dr.Item("nama_ayah").ToString())
            j = (dr.Item("no_hp_ayah").ToString())
            k = (dr.Item("nama_ibu").ToString())
            l = (dr.Item("no_hp_ibu").ToString())


            Dim tdItem As ListViewItem = ListView1.Items.Add(a)
            tdItem.SubItems.Add(b)
            tdItem.SubItems.Add(c)
            tdItem.SubItems.Add(d & ", " & f)
            tdItem.SubItems.Add(h)
            tdItem.SubItems.Add(i)
            tdItem.SubItems.Add(j)
            tdItem.SubItems.Add(k)
            tdItem.SubItems.Add(l)
        Loop
        dr.Close()
        cmd.Dispose()
        con.Close()
    End Sub

    Private Sub Button_search_Click(sender As Object, e As EventArgs) Handles Button_search.Click
        If TextBox_searchStudent.Text.Trim.Length = 0 Then
            Timer_autoRefresh_data.Stop()
            MsgBox("Kolom pencarian tidak boleh kosong." & vbNewLine &
                   "Harap isi kolom pencarian terlebih dahulu.", MsgBoxStyle.Information, "HEXA Report")
            Timer_autoRefresh_data.Start()
        Else
            searchData()
        End If
    End Sub

    Private Sub TextBox_searchStudent_Enter(sender As Object, e As EventArgs) Handles TextBox_searchStudent.Enter
        TextBox_searchStudent.BackColor = Color.White
        TextBox_searchStudent.ForeColor = Color.Black
    End Sub

    Private Sub TextBox_searchStudent_Leave(sender As Object, e As EventArgs) Handles TextBox_searchStudent.Leave
        TextBox_searchStudent.BackColor = Color.FromArgb(10, 10, 10)
        TextBox_searchStudent.ForeColor = Color.White
    End Sub

    Private Sub TextBox_searchStudent_TextChanged(sender As Object, e As EventArgs) Handles TextBox_searchStudent.TextChanged
        If TextBox_searchStudent.Text.Trim.Length = 0 Or TextBox_searchStudent.Text.Length = 0 Then
            getData()
            Timer_autoRefresh_data.Start()
        Else
            Exit Sub
        End If
    End Sub

    Private Sub TextBox_searchStudent_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox_searchStudent.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            If TextBox_searchStudent.Text.Trim.Length = 0 Then
                e.Handled = True
                MsgBox("Kolom pencarian tidak boleh kosong." & vbNewLine &
                   "Harap isi kolom pencarian terlebih dahulu.", MsgBoxStyle.Information, "HEXA Report")
            Else
                e.Handled = True
                searchData()
            End If
        End If
    End Sub

    Public Sub countData()
        Try
            con.Open()
            cmd.Connection = con
            cmd.CommandText = "SELECT COUNT(nis) FROM master_siswa WHERE 
id_kelas = '" & kel & "' AND id_jurusan = '" & jur & "' AND id_lokal = '" & lok & "'"
            Dim res As Object
            res = cmd.ExecuteScalar()
            Dim count_row As String
            count_row = res
            Label2.Text = "Jumlah Siswa : " & count_row & " Orang"
            dr.Close()
            cmd.Dispose()
            con.Close()
        Catch buf As Exception
            Label2.Text = "Jumlah Siswa : 0"
        End Try
    End Sub

    Private Sub Timer_autoRefresh_data_Tick_1(sender As Object, e As EventArgs) Handles Timer_autoRefresh_data.Tick
        getData()
        rankingScore()
        countData()
        classInfo()
    End Sub

    Private Sub Timer_dateTime_Tick(sender As Object, e As EventArgs) Handles Timer_dateTime.Tick
        Label_dateTime.Text = Format(Now, "H:mm tt" & vbNewLine & "M/dd/yyyy")
    End Sub

    Private Sub home_form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        form_container.Label_title.Text = "Menu Utama - HEXA Report"
        form_container.Text = "Menu Utama - HEXA Report"

        getData()
        rankingScore()
        Timer_autoRefresh_data.Start()
        countData()
        classInfo()

        ToolTip1.SetToolTip(Label_dateTime, Format(Now, "dddd, MMMM d, yyyy"))
        Label_tahun.Text = Format(Now.Year).ToString() & "/" & Format(Now.Year + 1).ToString()
        Me.Dock = DockStyle.Fill
    End Sub
End Class
