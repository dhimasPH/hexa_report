﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class report_form
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Panel_top = New System.Windows.Forms.Panel()
        Me.Button_firstItem = New System.Windows.Forms.Button()
        Me.Button_lastItem = New System.Windows.Forms.Button()
        Me.Label93 = New System.Windows.Forms.Label()
        Me.Label87 = New System.Windows.Forms.Label()
        Me.Label_nis = New System.Windows.Forms.Label()
        Me.Button_nextItem = New System.Windows.Forms.Button()
        Me.Button_backItem = New System.Windows.Forms.Button()
        Me.TextBox_searchNis = New System.Windows.Forms.TextBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Panel_fill = New System.Windows.Forms.Panel()
        Me.CrystalReportViewer1 = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.data_report1 = New Hexa_Report.data_report()
        Me.Panel_top.SuspendLayout()
        Me.Panel_fill.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel_top
        '
        Me.Panel_top.BackColor = System.Drawing.Color.FromArgb(CType(CType(8, Byte), Integer), CType(CType(7, Byte), Integer), CType(CType(7, Byte), Integer))
        Me.Panel_top.Controls.Add(Me.Button_firstItem)
        Me.Panel_top.Controls.Add(Me.Button_lastItem)
        Me.Panel_top.Controls.Add(Me.Label93)
        Me.Panel_top.Controls.Add(Me.Label87)
        Me.Panel_top.Controls.Add(Me.Label_nis)
        Me.Panel_top.Controls.Add(Me.Button_nextItem)
        Me.Panel_top.Controls.Add(Me.Button_backItem)
        Me.Panel_top.Controls.Add(Me.TextBox_searchNis)
        Me.Panel_top.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel_top.Location = New System.Drawing.Point(0, 0)
        Me.Panel_top.Name = "Panel_top"
        Me.Panel_top.Size = New System.Drawing.Size(1050, 66)
        Me.Panel_top.TabIndex = 0
        '
        'Button_firstItem
        '
        Me.Button_firstItem.BackgroundImage = Global.Hexa_Report.My.Resources.Resources.firstItem_button
        Me.Button_firstItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button_firstItem.FlatAppearance.BorderSize = 0
        Me.Button_firstItem.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(47, Byte), Integer))
        Me.Button_firstItem.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(32, Byte), Integer))
        Me.Button_firstItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_firstItem.Font = New System.Drawing.Font("Wingdings 3", 21.5!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Button_firstItem.ForeColor = System.Drawing.Color.White
        Me.Button_firstItem.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button_firstItem.Location = New System.Drawing.Point(157, 13)
        Me.Button_firstItem.Name = "Button_firstItem"
        Me.Button_firstItem.Size = New System.Drawing.Size(50, 40)
        Me.Button_firstItem.TabIndex = 17
        Me.ToolTip1.SetToolTip(Me.Button_firstItem, "Lihat Data Rapor Terawal")
        Me.Button_firstItem.UseVisualStyleBackColor = True
        '
        'Button_lastItem
        '
        Me.Button_lastItem.BackgroundImage = Global.Hexa_Report.My.Resources.Resources.lastItem_button
        Me.Button_lastItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Button_lastItem.FlatAppearance.BorderSize = 0
        Me.Button_lastItem.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(47, Byte), Integer))
        Me.Button_lastItem.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(32, Byte), Integer))
        Me.Button_lastItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_lastItem.Font = New System.Drawing.Font("Wingdings 3", 21.5!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Button_lastItem.ForeColor = System.Drawing.Color.White
        Me.Button_lastItem.Location = New System.Drawing.Point(425, 13)
        Me.Button_lastItem.Name = "Button_lastItem"
        Me.Button_lastItem.Size = New System.Drawing.Size(50, 40)
        Me.Button_lastItem.TabIndex = 16
        Me.ToolTip1.SetToolTip(Me.Button_lastItem, "Lihat Data Rapor Terakhir")
        Me.Button_lastItem.UseVisualStyleBackColor = True
        '
        'Label93
        '
        Me.Label93.AutoSize = True
        Me.Label93.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label93.ForeColor = System.Drawing.Color.White
        Me.Label93.Location = New System.Drawing.Point(271, 25)
        Me.Label93.Name = "Label93"
        Me.Label93.Size = New System.Drawing.Size(37, 19)
        Me.Label93.TabIndex = 15
        Me.Label93.Text = "NIS :"
        '
        'Label87
        '
        Me.Label87.AutoSize = True
        Me.Label87.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label87.ForeColor = System.Drawing.Color.White
        Me.Label87.Location = New System.Drawing.Point(11, 23)
        Me.Label87.Name = "Label87"
        Me.Label87.Size = New System.Drawing.Size(140, 19)
        Me.Label87.TabIndex = 14
        Me.Label87.Text = "Pencarian Data Rapor"
        '
        'Label_nis
        '
        Me.Label_nis.AutoSize = True
        Me.Label_nis.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.Label_nis.ForeColor = System.Drawing.Color.White
        Me.Label_nis.Location = New System.Drawing.Point(311, 25)
        Me.Label_nis.Name = "Label_nis"
        Me.Label_nis.Size = New System.Drawing.Size(26, 19)
        Me.Label_nis.TabIndex = 11
        Me.Label_nis.Text = "nis"
        Me.ToolTip1.SetToolTip(Me.Label_nis, "NIS")
        '
        'Button_nextItem
        '
        Me.Button_nextItem.BackgroundImage = Global.Hexa_Report.My.Resources.Resources.next_button
        Me.Button_nextItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Button_nextItem.FlatAppearance.BorderSize = 0
        Me.Button_nextItem.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(47, Byte), Integer))
        Me.Button_nextItem.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(32, Byte), Integer))
        Me.Button_nextItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_nextItem.Font = New System.Drawing.Font("Wingdings 3", 21.5!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Button_nextItem.ForeColor = System.Drawing.Color.White
        Me.Button_nextItem.Location = New System.Drawing.Point(372, 13)
        Me.Button_nextItem.Name = "Button_nextItem"
        Me.Button_nextItem.Size = New System.Drawing.Size(50, 40)
        Me.Button_nextItem.TabIndex = 12
        Me.ToolTip1.SetToolTip(Me.Button_nextItem, "Lihat Data Rapor Selanjutnya")
        Me.Button_nextItem.UseVisualStyleBackColor = True
        '
        'Button_backItem
        '
        Me.Button_backItem.BackgroundImage = Global.Hexa_Report.My.Resources.Resources.back_button
        Me.Button_backItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Button_backItem.FlatAppearance.BorderSize = 0
        Me.Button_backItem.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(47, Byte), Integer))
        Me.Button_backItem.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(32, Byte), Integer))
        Me.Button_backItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_backItem.Font = New System.Drawing.Font("Wingdings 3", 21.5!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Button_backItem.ForeColor = System.Drawing.Color.White
        Me.Button_backItem.Location = New System.Drawing.Point(210, 13)
        Me.Button_backItem.Name = "Button_backItem"
        Me.Button_backItem.Size = New System.Drawing.Size(50, 40)
        Me.Button_backItem.TabIndex = 10
        Me.ToolTip1.SetToolTip(Me.Button_backItem, "Lihat Data Rapor Sebelumnya")
        Me.Button_backItem.UseVisualStyleBackColor = True
        '
        'TextBox_searchNis
        '
        Me.TextBox_searchNis.Font = New System.Drawing.Font("Segoe UI", 9.5!)
        Me.TextBox_searchNis.Location = New System.Drawing.Point(312, 23)
        Me.TextBox_searchNis.Name = "TextBox_searchNis"
        Me.TextBox_searchNis.Size = New System.Drawing.Size(50, 24)
        Me.TextBox_searchNis.TabIndex = 13
        Me.TextBox_searchNis.Visible = False
        '
        'Panel_fill
        '
        Me.Panel_fill.Controls.Add(Me.CrystalReportViewer1)
        Me.Panel_fill.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel_fill.Location = New System.Drawing.Point(0, 66)
        Me.Panel_fill.Name = "Panel_fill"
        Me.Panel_fill.Size = New System.Drawing.Size(1050, 534)
        Me.Panel_fill.TabIndex = 1
        '
        'CrystalReportViewer1
        '
        Me.CrystalReportViewer1.ActiveViewIndex = 0
        Me.CrystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CrystalReportViewer1.Cursor = System.Windows.Forms.Cursors.Default
        Me.CrystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CrystalReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.CrystalReportViewer1.Name = "CrystalReportViewer1"
        Me.CrystalReportViewer1.ReportSource = Me.data_report1
        Me.CrystalReportViewer1.ShowCloseButton = False
        Me.CrystalReportViewer1.ShowGroupTreeButton = False
        Me.CrystalReportViewer1.ShowLogo = False
        Me.CrystalReportViewer1.ShowParameterPanelButton = False
        Me.CrystalReportViewer1.ShowRefreshButton = False
        Me.CrystalReportViewer1.Size = New System.Drawing.Size(1050, 534)
        Me.CrystalReportViewer1.TabIndex = 0
        Me.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        '
        'report_form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        Me.Controls.Add(Me.Panel_fill)
        Me.Controls.Add(Me.Panel_top)
        Me.Name = "report_form"
        Me.Size = New System.Drawing.Size(1050, 600)
        Me.Panel_top.ResumeLayout(False)
        Me.Panel_top.PerformLayout()
        Me.Panel_fill.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel_top As Panel
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents Button_firstItem As Button
    Friend WithEvents Button_lastItem As Button
    Friend WithEvents Label93 As Label
    Friend WithEvents Label87 As Label
    Friend WithEvents Label_nis As Label
    Friend WithEvents Button_nextItem As Button
    Friend WithEvents Button_backItem As Button
    Friend WithEvents TextBox_searchNis As TextBox
    Friend WithEvents Panel_fill As Panel
    Friend WithEvents CrystalReportViewer1 As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents data_report1 As data_report
End Class
