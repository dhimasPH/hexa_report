﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class mainMenu_form
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Panel_menu = New System.Windows.Forms.Panel()
        Me.Panel_clicked_Main = New System.Windows.Forms.Panel()
        Me.Button_home = New System.Windows.Forms.Button()
        Me.Panel_clicked_Print = New System.Windows.Forms.Panel()
        Me.Panel_clicked_Edit = New System.Windows.Forms.Panel()
        Me.Panel_clicked_Add = New System.Windows.Forms.Panel()
        Me.Button_hideMenu = New System.Windows.Forms.Button()
        Me.Button_logout = New System.Windows.Forms.Button()
        Me.Button_print = New System.Windows.Forms.Button()
        Me.Button_edit = New System.Windows.Forms.Button()
        Me.Button_input = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Panel_homeContainer = New System.Windows.Forms.Panel()
        Me.Timer_monitoring = New System.Windows.Forms.Timer(Me.components)
        Me.Panel_performance = New System.Windows.Forms.Panel()
        Me.Label_cpuUse = New System.Windows.Forms.Label()
        Me.Label_ramUse = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label_cpuHid = New System.Windows.Forms.Label()
        Me.Label_ramHid = New System.Windows.Forms.Label()
        Me.Panel_menu.SuspendLayout()
        Me.Panel_performance.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel_menu
        '
        Me.Panel_menu.BackColor = System.Drawing.Color.FromArgb(CType(CType(32, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(32, Byte), Integer))
        Me.Panel_menu.Controls.Add(Me.Panel_performance)
        Me.Panel_menu.Controls.Add(Me.Panel_clicked_Main)
        Me.Panel_menu.Controls.Add(Me.Button_home)
        Me.Panel_menu.Controls.Add(Me.Panel_clicked_Print)
        Me.Panel_menu.Controls.Add(Me.Panel_clicked_Edit)
        Me.Panel_menu.Controls.Add(Me.Panel_clicked_Add)
        Me.Panel_menu.Controls.Add(Me.Button_hideMenu)
        Me.Panel_menu.Controls.Add(Me.Button_logout)
        Me.Panel_menu.Controls.Add(Me.Button_print)
        Me.Panel_menu.Controls.Add(Me.Button_edit)
        Me.Panel_menu.Controls.Add(Me.Button_input)
        Me.Panel_menu.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel_menu.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.Panel_menu.Location = New System.Drawing.Point(0, 0)
        Me.Panel_menu.Name = "Panel_menu"
        Me.Panel_menu.Size = New System.Drawing.Size(350, 600)
        Me.Panel_menu.TabIndex = 0
        '
        'Panel_clicked_Main
        '
        Me.Panel_clicked_Main.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(174, Byte), Integer), CType(CType(219, Byte), Integer))
        Me.Panel_clicked_Main.Enabled = False
        Me.Panel_clicked_Main.Location = New System.Drawing.Point(0, 94)
        Me.Panel_clicked_Main.Name = "Panel_clicked_Main"
        Me.Panel_clicked_Main.Size = New System.Drawing.Size(3, 55)
        Me.Panel_clicked_Main.TabIndex = 17
        Me.Panel_clicked_Main.TabStop = True
        Me.Panel_clicked_Main.Visible = False
        '
        'Button_home
        '
        Me.Button_home.FlatAppearance.BorderSize = 0
        Me.Button_home.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(75, Byte), Integer), CType(CType(75, Byte), Integer), CType(CType(75, Byte), Integer))
        Me.Button_home.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(52, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.Button_home.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_home.Font = New System.Drawing.Font("Segoe UI", 13.0!)
        Me.Button_home.ForeColor = System.Drawing.Color.White
        Me.Button_home.Image = Global.Hexa_Report.My.Resources.Resources.home_button
        Me.Button_home.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button_home.Location = New System.Drawing.Point(0, 94)
        Me.Button_home.Name = "Button_home"
        Me.Button_home.Size = New System.Drawing.Size(350, 55)
        Me.Button_home.TabIndex = 3
        Me.Button_home.Text = "MENU UTAMA"
        Me.Button_home.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.ToolTip1.SetToolTip(Me.Button_home, "Menu Utama")
        Me.Button_home.UseVisualStyleBackColor = True
        '
        'Panel_clicked_Print
        '
        Me.Panel_clicked_Print.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(174, Byte), Integer), CType(CType(219, Byte), Integer))
        Me.Panel_clicked_Print.Enabled = False
        Me.Panel_clicked_Print.Location = New System.Drawing.Point(0, 268)
        Me.Panel_clicked_Print.Name = "Panel_clicked_Print"
        Me.Panel_clicked_Print.Size = New System.Drawing.Size(3, 55)
        Me.Panel_clicked_Print.TabIndex = 15
        Me.Panel_clicked_Print.TabStop = True
        Me.Panel_clicked_Print.Visible = False
        '
        'Panel_clicked_Edit
        '
        Me.Panel_clicked_Edit.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(174, Byte), Integer), CType(CType(219, Byte), Integer))
        Me.Panel_clicked_Edit.Enabled = False
        Me.Panel_clicked_Edit.Location = New System.Drawing.Point(0, 210)
        Me.Panel_clicked_Edit.Name = "Panel_clicked_Edit"
        Me.Panel_clicked_Edit.Size = New System.Drawing.Size(3, 55)
        Me.Panel_clicked_Edit.TabIndex = 14
        Me.Panel_clicked_Edit.TabStop = True
        Me.Panel_clicked_Edit.Visible = False
        '
        'Panel_clicked_Add
        '
        Me.Panel_clicked_Add.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(174, Byte), Integer), CType(CType(219, Byte), Integer))
        Me.Panel_clicked_Add.Enabled = False
        Me.Panel_clicked_Add.Location = New System.Drawing.Point(0, 152)
        Me.Panel_clicked_Add.Name = "Panel_clicked_Add"
        Me.Panel_clicked_Add.Size = New System.Drawing.Size(3, 55)
        Me.Panel_clicked_Add.TabIndex = 13
        Me.Panel_clicked_Add.TabStop = True
        Me.Panel_clicked_Add.Visible = False
        '
        'Button_hideMenu
        '
        Me.Button_hideMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Button_hideMenu.FlatAppearance.BorderSize = 0
        Me.Button_hideMenu.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(75, Byte), Integer), CType(CType(75, Byte), Integer), CType(CType(75, Byte), Integer))
        Me.Button_hideMenu.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(52, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.Button_hideMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_hideMenu.Image = Global.Hexa_Report.My.Resources.Resources.menu_button
        Me.Button_hideMenu.Location = New System.Drawing.Point(0, 34)
        Me.Button_hideMenu.Name = "Button_hideMenu"
        Me.Button_hideMenu.Size = New System.Drawing.Size(55, 55)
        Me.Button_hideMenu.TabIndex = 0
        Me.ToolTip1.SetToolTip(Me.Button_hideMenu, "Sembunyikan Menu")
        Me.Button_hideMenu.UseVisualStyleBackColor = True
        '
        'Button_logout
        '
        Me.Button_logout.FlatAppearance.BorderSize = 0
        Me.Button_logout.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(75, Byte), Integer), CType(CType(75, Byte), Integer), CType(CType(75, Byte), Integer))
        Me.Button_logout.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(52, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.Button_logout.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_logout.Font = New System.Drawing.Font("Segoe UI", 13.0!)
        Me.Button_logout.ForeColor = System.Drawing.Color.White
        Me.Button_logout.Image = Global.Hexa_Report.My.Resources.Resources.logout_button
        Me.Button_logout.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button_logout.Location = New System.Drawing.Point(0, 327)
        Me.Button_logout.Name = "Button_logout"
        Me.Button_logout.Size = New System.Drawing.Size(350, 55)
        Me.Button_logout.TabIndex = 7
        Me.Button_logout.Text = "LOGOUT"
        Me.Button_logout.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.ToolTip1.SetToolTip(Me.Button_logout, "Logout")
        Me.Button_logout.UseVisualStyleBackColor = True
        '
        'Button_print
        '
        Me.Button_print.FlatAppearance.BorderSize = 0
        Me.Button_print.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(75, Byte), Integer), CType(CType(75, Byte), Integer), CType(CType(75, Byte), Integer))
        Me.Button_print.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(52, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.Button_print.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_print.Font = New System.Drawing.Font("Segoe UI", 13.0!)
        Me.Button_print.ForeColor = System.Drawing.Color.White
        Me.Button_print.Image = Global.Hexa_Report.My.Resources.Resources.print_leger_button
        Me.Button_print.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button_print.Location = New System.Drawing.Point(0, 268)
        Me.Button_print.Name = "Button_print"
        Me.Button_print.Size = New System.Drawing.Size(350, 55)
        Me.Button_print.TabIndex = 6
        Me.Button_print.Text = "CETAK NILAI"
        Me.Button_print.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.ToolTip1.SetToolTip(Me.Button_print, "Cetak Nilai")
        Me.Button_print.UseVisualStyleBackColor = True
        '
        'Button_edit
        '
        Me.Button_edit.FlatAppearance.BorderSize = 0
        Me.Button_edit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(75, Byte), Integer), CType(CType(75, Byte), Integer), CType(CType(75, Byte), Integer))
        Me.Button_edit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(52, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.Button_edit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_edit.Font = New System.Drawing.Font("Segoe UI", 13.0!)
        Me.Button_edit.ForeColor = System.Drawing.Color.White
        Me.Button_edit.Image = Global.Hexa_Report.My.Resources.Resources.edit_leger_button
        Me.Button_edit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button_edit.Location = New System.Drawing.Point(0, 210)
        Me.Button_edit.Name = "Button_edit"
        Me.Button_edit.Size = New System.Drawing.Size(350, 55)
        Me.Button_edit.TabIndex = 5
        Me.Button_edit.Text = "EDIT NILAI"
        Me.Button_edit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.ToolTip1.SetToolTip(Me.Button_edit, "Edit Nilai")
        Me.Button_edit.UseVisualStyleBackColor = True
        '
        'Button_input
        '
        Me.Button_input.FlatAppearance.BorderSize = 0
        Me.Button_input.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(75, Byte), Integer), CType(CType(75, Byte), Integer), CType(CType(75, Byte), Integer))
        Me.Button_input.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(52, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.Button_input.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_input.Font = New System.Drawing.Font("Segoe UI", 13.0!)
        Me.Button_input.ForeColor = System.Drawing.Color.White
        Me.Button_input.Image = Global.Hexa_Report.My.Resources.Resources.add_leger_button
        Me.Button_input.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button_input.Location = New System.Drawing.Point(0, 152)
        Me.Button_input.Name = "Button_input"
        Me.Button_input.Size = New System.Drawing.Size(350, 55)
        Me.Button_input.TabIndex = 4
        Me.Button_input.Text = "INPUT NILAI"
        Me.Button_input.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.ToolTip1.SetToolTip(Me.Button_input, "Input Nilai")
        Me.Button_input.UseVisualStyleBackColor = True
        '
        'Panel_homeContainer
        '
        Me.Panel_homeContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel_homeContainer.Location = New System.Drawing.Point(350, 0)
        Me.Panel_homeContainer.Name = "Panel_homeContainer"
        Me.Panel_homeContainer.Size = New System.Drawing.Size(700, 600)
        Me.Panel_homeContainer.TabIndex = 1
        '
        'Timer_monitoring
        '
        Me.Timer_monitoring.Enabled = True
        Me.Timer_monitoring.Interval = 1000
        '
        'Panel_performance
        '
        Me.Panel_performance.Controls.Add(Me.Label_ramHid)
        Me.Panel_performance.Controls.Add(Me.Label_cpuHid)
        Me.Panel_performance.Controls.Add(Me.Label3)
        Me.Panel_performance.Controls.Add(Me.Label_ramUse)
        Me.Panel_performance.Controls.Add(Me.Label_cpuUse)
        Me.Panel_performance.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel_performance.Location = New System.Drawing.Point(0, 515)
        Me.Panel_performance.Name = "Panel_performance"
        Me.Panel_performance.Size = New System.Drawing.Size(350, 85)
        Me.Panel_performance.TabIndex = 18
        '
        'Label_cpuUse
        '
        Me.Label_cpuUse.AutoSize = True
        Me.Label_cpuUse.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.Label_cpuUse.ForeColor = System.Drawing.Color.White
        Me.Label_cpuUse.Location = New System.Drawing.Point(16, 49)
        Me.Label_cpuUse.Name = "Label_cpuUse"
        Me.Label_cpuUse.Size = New System.Drawing.Size(59, 19)
        Me.Label_cpuUse.TabIndex = 0
        Me.Label_cpuUse.Text = "CPU 0%"
        Me.ToolTip1.SetToolTip(Me.Label_cpuUse, "Aktivitas CPU")
        '
        'Label_ramUse
        '
        Me.Label_ramUse.AutoSize = True
        Me.Label_ramUse.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.Label_ramUse.ForeColor = System.Drawing.Color.White
        Me.Label_ramUse.Location = New System.Drawing.Point(107, 49)
        Me.Label_ramUse.Name = "Label_ramUse"
        Me.Label_ramUse.Size = New System.Drawing.Size(73, 19)
        Me.Label_ramUse.TabIndex = 1
        Me.Label_ramUse.Text = "RAM 0 GB"
        Me.ToolTip1.SetToolTip(Me.Label_ramUse, "RAM yang terpakai")
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(16, 14)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(147, 21)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Performa Komputer"
        '
        'Label_cpuHid
        '
        Me.Label_cpuHid.AutoSize = True
        Me.Label_cpuHid.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label_cpuHid.ForeColor = System.Drawing.Color.White
        Me.Label_cpuHid.Location = New System.Drawing.Point(7, 3)
        Me.Label_cpuHid.Name = "Label_cpuHid"
        Me.Label_cpuHid.Size = New System.Drawing.Size(30, 30)
        Me.Label_cpuHid.TabIndex = 3
        Me.Label_cpuHid.Text = "CPU" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "0%"
        Me.Label_cpuHid.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ToolTip1.SetToolTip(Me.Label_cpuHid, "Aktivitas CPU")
        Me.Label_cpuHid.Visible = False
        '
        'Label_ramHid
        '
        Me.Label_ramHid.AutoSize = True
        Me.Label_ramHid.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label_ramHid.ForeColor = System.Drawing.Color.White
        Me.Label_ramHid.Location = New System.Drawing.Point(6, 46)
        Me.Label_ramHid.Name = "Label_ramHid"
        Me.Label_ramHid.Size = New System.Drawing.Size(33, 30)
        Me.Label_ramHid.TabIndex = 4
        Me.Label_ramHid.Text = "RAM" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "0%"
        Me.Label_ramHid.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ToolTip1.SetToolTip(Me.Label_ramHid, "RAM yang terpakai")
        Me.Label_ramHid.Visible = False
        '
        'mainMenu_form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        Me.Controls.Add(Me.Panel_homeContainer)
        Me.Controls.Add(Me.Panel_menu)
        Me.DoubleBuffered = True
        Me.Name = "mainMenu_form"
        Me.Size = New System.Drawing.Size(1050, 600)
        Me.Panel_menu.ResumeLayout(False)
        Me.Panel_performance.ResumeLayout(False)
        Me.Panel_performance.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel_menu As Panel
    Friend WithEvents Button_hideMenu As Button
    Friend WithEvents Button_edit As Button
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents Button_input As Button
    Friend WithEvents Button_print As Button
    Friend WithEvents Button_logout As Button
    Friend WithEvents Panel_clicked_Print As Panel
    Friend WithEvents Panel_clicked_Edit As Panel
    Friend WithEvents Panel_clicked_Add As Panel
    Friend WithEvents Panel_clicked_Main As Panel
    Friend WithEvents Panel_homeContainer As Panel
    Public WithEvents Button_home As Button
    Friend WithEvents Timer_monitoring As Timer
    Friend WithEvents Panel_performance As Panel
    Friend WithEvents Label3 As Label
    Friend WithEvents Label_ramUse As Label
    Friend WithEvents Label_cpuUse As Label
    Friend WithEvents Label_ramHid As Label
    Friend WithEvents Label_cpuHid As Label
End Class
