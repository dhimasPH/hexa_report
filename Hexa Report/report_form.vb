﻿Imports MySql.Data.MySqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared

Public Class report_form
    Dim search_nis As String
    Dim todayDate As String = Format(Now, "dd MMMM yyyy")
    Dim date_time As String = Format(Now.Year).ToString() & "/" & Format(Now.Year + 1).ToString()
    Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    Dim crParameterFieldDefinition As ParameterFieldDefinition
    Public nis, nisn, nama_siswa, kelas, rom, jurusan, jurusan_sngkt, lokal, nip, nama_wali, id_semester, ket, ket_sakit, ket_izin, ket_alpha, ekskul, komen As String

    Private Sub Button_firstItem_Click(sender As Object, e As EventArgs) Handles Button_firstItem.Click
        query = "SELECT raport.nis, master_siswa.nama_siswa, kelas.kelas, jurusan.jurusan, lokal.lokal 
FROM raport 
INNER JOIN master_siswa ON master_siswa.nis = raport.nis 
INNER JOIN kelas ON kelas.id_kelas = master_siswa.id_kelas 
INNER JOIN jurusan ON jurusan.id_jurusan = master_siswa.id_jurusan 
INNER JOIN lokal ON lokal.id_lokal = master_siswa.id_lokal 
INNER JOIN master_guru ON master_siswa.nip = master_guru.nip 
INNER JOIN wali_kelas ON master_siswa.nip = wali_kelas.nip 
WHERE wali_kelas.nip = '" & wal & "' 
ORDER BY master_siswa.nis ASC 
LIMIT 1"
        con.Open()
        cmd = New MySqlCommand(query, con)
        dr = cmd.ExecuteReader()

        If dr.HasRows() Then
            Button_firstItem.Enabled = False
            Button_lastItem.Enabled = False
            Button_backItem.Enabled = False
            Button_nextItem.Enabled = False
            Do While dr.Read()
                a1 = (dr.Item("nis").ToString())
                b1 = (dr.Item("nama_siswa").ToString())
                c1 = (dr.Item("kelas").ToString())
                d1 = (dr.Item("jurusan").ToString())
                f1 = (dr.Item("lokal").ToString())
            Loop
            Label_nis.Text = a1
            TextBox_searchNis.Text = a1
            dr.Close()
            cmd.Dispose()
            con.Close()

            'prntRepData(studentID:=Label_nis.Text)
            retStudentSpec()
            crSetParams(data_report1, "nama", nama_siswa)
            crSetParams(data_report1, "nis", nis)
            crSetParams(data_report1, "nisn", nisn)
            crSetParams(data_report1, "kelas", kelas)
            crSetParams(data_report1, "jurusan", jurusan_sngkt)
            crSetParams(data_report1, "lokal", lokal)
            crSetParams(data_report1, "semester", id_semester)
            crSetParams(data_report1, "semester_hur", ket)
            crSetParams(data_report1, "nip", nip)
            crSetParams(data_report1, "wali", nama_wali)
            crSetParams(data_report1, "date_time", date_time)
            crSetParams(data_report1, "todayDate", todayDate)

            prntRepData(Label_nis.Text, 1)
            changeObjTxt(data_report1, "Text116", k1)
            changeObjTxt(data_report1, "Text136", l1)
            changeObjTxt(data_report1, "Text155", m1)
            changeObjTxt(data_report1, "Text174", n1)
            prntRepData(Label_nis.Text, 2)
            changeObjTxt(data_report1, "Text117", k1)
            changeObjTxt(data_report1, "Text137", l1)
            changeObjTxt(data_report1, "Text156", m1)
            changeObjTxt(data_report1, "Text175", n1)
            prntRepData(Label_nis.Text, 3)
            changeObjTxt(data_report1, "Text118", k1)
            changeObjTxt(data_report1, "Text138", l1)
            changeObjTxt(data_report1, "Text157", m1)
            changeObjTxt(data_report1, "Text176", n1)
            prntRepData(Label_nis.Text, 4)
            changeObjTxt(data_report1, "Text119", k1)
            changeObjTxt(data_report1, "Text139", l1)
            changeObjTxt(data_report1, "Text158", m1)
            changeObjTxt(data_report1, "Text177", n1)
            prntRepData(Label_nis.Text, 5)
            changeObjTxt(data_report1, "Text120", k1)
            changeObjTxt(data_report1, "Text140", l1)
            changeObjTxt(data_report1, "Text159", m1)
            changeObjTxt(data_report1, "Text178", n1)
            prntRepData(Label_nis.Text, 6)
            changeObjTxt(data_report1, "Text121", k1)
            changeObjTxt(data_report1, "Text141", l1)
            changeObjTxt(data_report1, "Text160", m1)
            changeObjTxt(data_report1, "Text179", n1)
            prntRepData(Label_nis.Text, 7)
            changeObjTxt(data_report1, "Text122", k1)
            changeObjTxt(data_report1, "Text142", l1)
            changeObjTxt(data_report1, "Text161", m1)
            changeObjTxt(data_report1, "Text180", n1)
            prntRepData(Label_nis.Text, 8)
            changeObjTxt(data_report1, "Text123", k1)
            changeObjTxt(data_report1, "Text143", l1)
            changeObjTxt(data_report1, "Text162", m1)
            changeObjTxt(data_report1, "Text181", n1)
            prntRepData(Label_nis.Text, 9)
            changeObjTxt(data_report1, "Text124", k1)
            changeObjTxt(data_report1, "Text144", l1)
            changeObjTxt(data_report1, "Text163", m1)
            changeObjTxt(data_report1, "Text182", n1)
            prntRepData(Label_nis.Text, 10)
            changeObjTxt(data_report1, "Text125", k1)
            changeObjTxt(data_report1, "Text145", l1)
            changeObjTxt(data_report1, "Text164", m1)
            changeObjTxt(data_report1, "Text183", n1)

            changeObjTxt(data_report1, "Text126", "N/A")
            changeObjTxt(data_report1, "Text146", "N/A")
            changeObjTxt(data_report1, "Text165", "N/A")
            changeObjTxt(data_report1, "Text184", "N/A")
            changeObjTxt(data_report1, "Text127", "N/A")
            changeObjTxt(data_report1, "Text147", "N/A")
            changeObjTxt(data_report1, "Text166", "N/A")
            changeObjTxt(data_report1, "Text185", "N/A")
            changeObjTxt(data_report1, "Text128", "N/A")
            changeObjTxt(data_report1, "Text148", "N/A")
            changeObjTxt(data_report1, "Text167", "N/A")
            changeObjTxt(data_report1, "Text186", "N/A")

            prntRepData(Label_nis.Text, 24)
            changeObjTxt(data_report1, "Text129", k1)
            changeObjTxt(data_report1, "Text149", l1)
            changeObjTxt(data_report1, "Text168", m1)
            changeObjTxt(data_report1, "Text187", n1)
            prntRepData(Label_nis.Text, 25)
            changeObjTxt(data_report1, "Text130", k1)
            changeObjTxt(data_report1, "Text150", l1)
            changeObjTxt(data_report1, "Text169", m1)
            changeObjTxt(data_report1, "Text188", n1)
            prntRepData(Label_nis.Text, 26)
            changeObjTxt(data_report1, "Text131", k1)
            changeObjTxt(data_report1, "Text151", l1)
            changeObjTxt(data_report1, "Text170", m1)
            changeObjTxt(data_report1, "Text189", n1)
            prntRepData(Label_nis.Text, 27)
            changeObjTxt(data_report1, "Text132", k1)
            changeObjTxt(data_report1, "Text152", l1)
            changeObjTxt(data_report1, "Text171", m1)
            changeObjTxt(data_report1, "Text190", n1)
            prntRepData(Label_nis.Text, 28)
            changeObjTxt(data_report1, "Text134", k1)
            changeObjTxt(data_report1, "Text153", l1)
            changeObjTxt(data_report1, "Text172", m1)
            changeObjTxt(data_report1, "Text191", n1)
            prntRepData(Label_nis.Text, 29)
            changeObjTxt(data_report1, "Text135", k1)
            changeObjTxt(data_report1, "Text154", l1)
            changeObjTxt(data_report1, "Text173", m1)
            changeObjTxt(data_report1, "Text192", n1)
            changeObjTxt(data_report1, "Text83", r1)
            changeObjTxt(data_report1, "Text89", o1)
            changeObjTxt(data_report1, "Text90", p1)
            changeObjTxt(data_report1, "Text91", q1)
            Button_firstItem.Enabled = True
            Button_lastItem.Enabled = True
            Button_backItem.Enabled = True
            Button_nextItem.Enabled = True
        Else
            Button_firstItem.Enabled = True
            Button_lastItem.Enabled = True
            Button_backItem.Enabled = True
            Button_nextItem.Enabled = True
            Label_nis.Text = a1
            TextBox_searchNis.Text = a1
            dr.Close()
            cmd.Dispose()
            con.Close()
            Exit Sub
        End If
    End Sub

    Public Sub retStudentSpec()
        query = "SELECT
(SELECT group_concat(DISTINCT master_siswa.nis)) AS nis,
(SELECT group_concat(DISTINCT master_siswa.nisn)) AS nisn,
(SELECT UPPER(group_concat(DISTINCT master_siswa.nama_siswa))) AS nama_siswa,
(SELECT group_concat(DISTINCT kelas.kelas)) AS kelas,
(SELECT group_concat(DISTINCT kelas.rom)) AS rom,
(SELECT group_concat(DISTINCT jurusan.jurusan)) AS jurusan,
(SELECT group_concat(DISTINCT jurusan.jurusan_sngkt)) AS jurusan_sngkt,
(SELECT group_concat(DISTINCT lokal.lokal)) AS lokal,
(SELECT group_concat(DISTINCT master_siswa.nip)) AS nip,
(SELECT UPPER(group_concat(DISTINCT master_guru.nama))) AS nama_wali,
(SELECT group_concat(DISTINCT raport.id_semester)) AS id_semester, semester.ket,
supporting_grade.ket_sakit, supporting_grade.ket_izin, 
supporting_grade.ket_alpha, supporting_grade.ekskul, supporting_grade.komen
FROM master_siswa
INNER JOIN kelas ON kelas.id_kelas = master_siswa.id_kelas
INNER JOIN jurusan ON jurusan.id_jurusan = master_siswa.id_jurusan
INNER JOIN lokal ON lokal.id_lokal = master_siswa.id_lokal
INNER JOIN master_guru ON master_siswa.nip = master_guru.nip
INNER JOIN wali_kelas ON master_siswa.nip = wali_kelas.nip
INNER JOIN raport ON raport.nis = master_siswa.nis
INNER JOIN semester ON raport.id_semester = semester.id_semester
INNER JOIN supporting_grade ON supporting_grade.nis = master_siswa.nis
WHERE wali_kelas.nip = '" & wal & "' AND master_siswa.nis = '" & Label_nis.Text & "'
ORDER BY master_siswa.nis ASC"
        con.Open()
        cmd = New MySqlCommand(query, con)
        dr = cmd.ExecuteReader()

        Do While dr.Read()
            nis = (dr.Item("nis").ToString())
            nisn = (dr.Item("nisn").ToString())
            nama_siswa = (dr.Item("nama_siswa").ToString())
            kelas = (dr.Item("kelas").ToString())
            rom = (dr.Item("rom").ToString())
            jurusan = (dr.Item("jurusan").ToString())
            jurusan_sngkt = (dr.Item("jurusan_sngkt").ToString())
            lokal = (dr.Item("lokal").ToString())
            nip = (dr.Item("nip").ToString())
            nama_wali = (dr.Item("nama_wali").ToString())
            id_semester = (dr.Item("id_semester").ToString())
            ket = (dr.Item("ket").ToString())
            ket_sakit = (dr.Item("ket_sakit").ToString())
            ket_izin = (dr.Item("ket_izin").ToString())
            ket_alpha = (dr.Item("ket_alpha").ToString())
            ekskul = (dr.Item("ekskul").ToString())
            komen = (dr.Item("komen").ToString())
        Loop
        dr.Close()
        cmd.Dispose()
        con.Close()
    End Sub
    Public Sub retStudentData()
        query = "SELECT raport.nis, master_siswa.nisn, master_siswa.nama_siswa, kelas.kelas, jurusan.jurusan, lokal.lokal, master_siswa.nip, master_guru.nama
FROM raport
INNER JOIN master_siswa ON raport.nis = master_siswa.nis
INNER JOIN kelas ON kelas.id_kelas = master_siswa.id_kelas
INNER JOIN jurusan ON jurusan.id_jurusan = master_siswa.id_jurusan
INNER JOIN lokal ON lokal.id_lokal = master_siswa.id_lokal
INNER JOIN master_guru ON master_siswa.nip = master_guru.nip
INNER JOIN wali_kelas ON master_siswa.nip = wali_kelas.nip
WHERE wali_kelas.nip = '" & wal & "'
ORDER BY master_siswa.nis ASC
LIMIT 1"
        con.Open()
        cmd = New MySqlCommand(query, con)
        dr = cmd.ExecuteReader()

        Do While dr.Read()
            a1 = (dr.Item("nis").ToString())
            g1 = (dr.Item("nisn").ToString())
            b1 = (dr.Item("nama_siswa").ToString())
            c1 = (dr.Item("kelas").ToString())
            d1 = (dr.Item("jurusan").ToString())
            f1 = (dr.Item("lokal").ToString())
        Loop
        Label_nis.Text = a1
        TextBox_searchNis.Text = a1
        dr.Close()
        cmd.Dispose()
        con.Close()
    End Sub

    Private Sub report_form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Dock = DockStyle.Fill

        retStudentData()
        'crSetParams(data_report1, "id_wali", wal)
        retStudentSpec()
        crSetParams(data_report1, "nama", nama_siswa)
        crSetParams(data_report1, "nis", nis)
        crSetParams(data_report1, "nisn", nisn)
        crSetParams(data_report1, "kelas", kelas)
        crSetParams(data_report1, "jurusan", jurusan_sngkt)
        crSetParams(data_report1, "lokal", lokal)
        crSetParams(data_report1, "semester", id_semester)
        crSetParams(data_report1, "semester_hur", ket)
        crSetParams(data_report1, "nip", nip)
        crSetParams(data_report1, "wali", nama_wali)

        crSetParams(data_report1, "date_time", date_time)
        crSetParams(data_report1, "todayDate", todayDate)

        prntRepData(Label_nis.Text, 1)
        changeObjTxt(data_report1, "Text116", k1)
        changeObjTxt(data_report1, "Text136", l1)
        changeObjTxt(data_report1, "Text155", m1)
        changeObjTxt(data_report1, "Text174", n1)
        prntRepData(Label_nis.Text, 2)
        changeObjTxt(data_report1, "Text117", k1)
        changeObjTxt(data_report1, "Text137", l1)
        changeObjTxt(data_report1, "Text156", m1)
        changeObjTxt(data_report1, "Text175", n1)
        prntRepData(Label_nis.Text, 3)
        changeObjTxt(data_report1, "Text118", k1)
        changeObjTxt(data_report1, "Text138", l1)
        changeObjTxt(data_report1, "Text157", m1)
        changeObjTxt(data_report1, "Text176", n1)
        prntRepData(Label_nis.Text, 4)
        changeObjTxt(data_report1, "Text119", k1)
        changeObjTxt(data_report1, "Text139", l1)
        changeObjTxt(data_report1, "Text158", m1)
        changeObjTxt(data_report1, "Text177", n1)
        prntRepData(Label_nis.Text, 5)
        changeObjTxt(data_report1, "Text120", k1)
        changeObjTxt(data_report1, "Text140", l1)
        changeObjTxt(data_report1, "Text159", m1)
        changeObjTxt(data_report1, "Text178", n1)
        prntRepData(Label_nis.Text, 6)
        changeObjTxt(data_report1, "Text121", k1)
        changeObjTxt(data_report1, "Text141", l1)
        changeObjTxt(data_report1, "Text160", m1)
        changeObjTxt(data_report1, "Text179", n1)
        prntRepData(Label_nis.Text, 7)
        changeObjTxt(data_report1, "Text122", k1)
        changeObjTxt(data_report1, "Text142", l1)
        changeObjTxt(data_report1, "Text161", m1)
        changeObjTxt(data_report1, "Text180", n1)
        prntRepData(Label_nis.Text, 8)
        changeObjTxt(data_report1, "Text123", k1)
        changeObjTxt(data_report1, "Text143", l1)
        changeObjTxt(data_report1, "Text162", m1)
        changeObjTxt(data_report1, "Text181", n1)
        prntRepData(Label_nis.Text, 9)
        changeObjTxt(data_report1, "Text124", k1)
        changeObjTxt(data_report1, "Text144", l1)
        changeObjTxt(data_report1, "Text163", m1)
        changeObjTxt(data_report1, "Text182", n1)
        prntRepData(Label_nis.Text, 10)
        changeObjTxt(data_report1, "Text125", k1)
        changeObjTxt(data_report1, "Text145", l1)
        changeObjTxt(data_report1, "Text164", m1)
        changeObjTxt(data_report1, "Text183", n1)

        changeObjTxt(data_report1, "Text126", "N/A")
        changeObjTxt(data_report1, "Text146", "N/A")
        changeObjTxt(data_report1, "Text165", "N/A")
        changeObjTxt(data_report1, "Text184", "N/A")
        changeObjTxt(data_report1, "Text127", "N/A")
        changeObjTxt(data_report1, "Text147", "N/A")
        changeObjTxt(data_report1, "Text166", "N/A")
        changeObjTxt(data_report1, "Text185", "N/A")
        changeObjTxt(data_report1, "Text128", "N/A")
        changeObjTxt(data_report1, "Text148", "N/A")
        changeObjTxt(data_report1, "Text167", "N/A")
        changeObjTxt(data_report1, "Text186", "N/A")

        prntRepData(Label_nis.Text, 24)
        changeObjTxt(data_report1, "Text129", k1)
        changeObjTxt(data_report1, "Text149", l1)
        changeObjTxt(data_report1, "Text168", m1)
        changeObjTxt(data_report1, "Text187", n1)
        prntRepData(Label_nis.Text, 25)
        changeObjTxt(data_report1, "Text130", k1)
        changeObjTxt(data_report1, "Text150", l1)
        changeObjTxt(data_report1, "Text169", m1)
        changeObjTxt(data_report1, "Text188", n1)
        prntRepData(Label_nis.Text, 26)
        changeObjTxt(data_report1, "Text131", k1)
        changeObjTxt(data_report1, "Text151", l1)
        changeObjTxt(data_report1, "Text170", m1)
        changeObjTxt(data_report1, "Text189", n1)
        prntRepData(Label_nis.Text, 27)
        changeObjTxt(data_report1, "Text132", k1)
        changeObjTxt(data_report1, "Text152", l1)
        changeObjTxt(data_report1, "Text171", m1)
        changeObjTxt(data_report1, "Text190", n1)
        prntRepData(Label_nis.Text, 28)
        changeObjTxt(data_report1, "Text134", k1)
        changeObjTxt(data_report1, "Text153", l1)
        changeObjTxt(data_report1, "Text172", m1)
        changeObjTxt(data_report1, "Text191", n1)
        prntRepData(Label_nis.Text, 29)
        changeObjTxt(data_report1, "Text135", k1)
        changeObjTxt(data_report1, "Text154", l1)
        changeObjTxt(data_report1, "Text173", m1)
        changeObjTxt(data_report1, "Text192", n1)
        changeObjTxt(data_report1, "Text83", r1)
        changeObjTxt(data_report1, "Text89", o1)
        changeObjTxt(data_report1, "Text90", p1)
        changeObjTxt(data_report1, "Text91", q1)


        'Save this for later, how to change the name of TextObject in Crystal Reports
        'Dim txtReport As CrystalDecisions.CrystalReports.Engine.TextObject
        'txtReport = data_report1.ReportDefinition.ReportObjects("Text1")
        'txtReport.Text = "Test"

        'changeObjTxt(data_report1, "Text67", "Pemodelan Perangkat Lunak")

        form_container.Label_title.Text = "Cetak Nilai - HEXA Report"
        form_container.Text = "Cetak Nilai - HEXA Report"
    End Sub

    Private Sub TextBox_searchNis_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox_searchNis.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            Button_firstItem.Enabled = False
            Button_lastItem.Enabled = False
            Button_backItem.Enabled = False
            Button_nextItem.Enabled = False
            e.Handled = True
            query = "SELECT raport.nis, master_siswa.nisn, master_siswa.nama_siswa, kelas.kelas, jurusan.jurusan, lokal.lokal, master_siswa.nip, master_guru.nama
FROM raport
INNER JOIN master_siswa ON raport.nis = master_siswa.nis
INNER JOIN kelas ON kelas.id_kelas = master_siswa.id_kelas
INNER JOIN jurusan ON jurusan.id_jurusan = master_siswa.id_jurusan
INNER JOIN lokal ON lokal.id_lokal = master_siswa.id_lokal
INNER JOIN master_guru ON master_siswa.nip = master_guru.nip
INNER JOIN wali_kelas ON master_siswa.nip = wali_kelas.nip
WHERE wali_kelas.nip = '" & wal & "' AND raport.nis = '" & MySqlHelper.EscapeString(TextBox_searchNis.Text) & "'
ORDER BY master_siswa.nis ASC
LIMIT 1"
            con.Open()
            cmd = New MySqlCommand(query, con)
            dr = cmd.ExecuteReader()

            If dr.HasRows() Then
                Do While dr.Read()
                    a1 = (dr.Item("nis").ToString())
                    g1 = (dr.Item("nisn").ToString())
                    b1 = (dr.Item("nama_siswa").ToString())
                    c1 = (dr.Item("kelas").ToString())
                    d1 = (dr.Item("jurusan").ToString())
                    f1 = (dr.Item("lokal").ToString())
                Loop
                Label_nis.Text = a1
                TextBox_searchNis.Text = a1
                dr.Close()
                cmd.Dispose()
                con.Close()

                retStudentSpec()
                crSetParams(data_report1, "nama", nama_siswa)
                crSetParams(data_report1, "nis", nis)
                crSetParams(data_report1, "nisn", nisn)
                crSetParams(data_report1, "kelas", kelas)
                crSetParams(data_report1, "jurusan", jurusan_sngkt)
                crSetParams(data_report1, "lokal", lokal)
                crSetParams(data_report1, "semester", id_semester)
                crSetParams(data_report1, "semester_hur", ket)
                crSetParams(data_report1, "nip", nip)
                crSetParams(data_report1, "wali", nama_wali)
                crSetParams(data_report1, "date_time", date_time)
                crSetParams(data_report1, "todayDate", todayDate)

                prntRepData(Label_nis.Text, 1)
                changeObjTxt(data_report1, "Text116", k1)
                changeObjTxt(data_report1, "Text136", l1)
                changeObjTxt(data_report1, "Text155", m1)
                changeObjTxt(data_report1, "Text174", n1)
                prntRepData(Label_nis.Text, 2)
                changeObjTxt(data_report1, "Text117", k1)
                changeObjTxt(data_report1, "Text137", l1)
                changeObjTxt(data_report1, "Text156", m1)
                changeObjTxt(data_report1, "Text175", n1)
                prntRepData(Label_nis.Text, 3)
                changeObjTxt(data_report1, "Text118", k1)
                changeObjTxt(data_report1, "Text138", l1)
                changeObjTxt(data_report1, "Text157", m1)
                changeObjTxt(data_report1, "Text176", n1)
                prntRepData(Label_nis.Text, 4)
                changeObjTxt(data_report1, "Text119", k1)
                changeObjTxt(data_report1, "Text139", l1)
                changeObjTxt(data_report1, "Text158", m1)
                changeObjTxt(data_report1, "Text177", n1)
                prntRepData(Label_nis.Text, 5)
                changeObjTxt(data_report1, "Text120", k1)
                changeObjTxt(data_report1, "Text140", l1)
                changeObjTxt(data_report1, "Text159", m1)
                changeObjTxt(data_report1, "Text178", n1)
                prntRepData(Label_nis.Text, 6)
                changeObjTxt(data_report1, "Text121", k1)
                changeObjTxt(data_report1, "Text141", l1)
                changeObjTxt(data_report1, "Text160", m1)
                changeObjTxt(data_report1, "Text179", n1)
                prntRepData(Label_nis.Text, 7)
                changeObjTxt(data_report1, "Text122", k1)
                changeObjTxt(data_report1, "Text142", l1)
                changeObjTxt(data_report1, "Text161", m1)
                changeObjTxt(data_report1, "Text180", n1)
                prntRepData(Label_nis.Text, 8)
                changeObjTxt(data_report1, "Text123", k1)
                changeObjTxt(data_report1, "Text143", l1)
                changeObjTxt(data_report1, "Text162", m1)
                changeObjTxt(data_report1, "Text181", n1)
                prntRepData(Label_nis.Text, 9)
                changeObjTxt(data_report1, "Text124", k1)
                changeObjTxt(data_report1, "Text144", l1)
                changeObjTxt(data_report1, "Text163", m1)
                changeObjTxt(data_report1, "Text182", n1)
                prntRepData(Label_nis.Text, 10)
                changeObjTxt(data_report1, "Text125", k1)
                changeObjTxt(data_report1, "Text145", l1)
                changeObjTxt(data_report1, "Text164", m1)
                changeObjTxt(data_report1, "Text183", n1)

                changeObjTxt(data_report1, "Text126", "N/A")
                changeObjTxt(data_report1, "Text146", "N/A")
                changeObjTxt(data_report1, "Text165", "N/A")
                changeObjTxt(data_report1, "Text184", "N/A")
                changeObjTxt(data_report1, "Text127", "N/A")
                changeObjTxt(data_report1, "Text147", "N/A")
                changeObjTxt(data_report1, "Text166", "N/A")
                changeObjTxt(data_report1, "Text185", "N/A")
                changeObjTxt(data_report1, "Text128", "N/A")
                changeObjTxt(data_report1, "Text148", "N/A")
                changeObjTxt(data_report1, "Text167", "N/A")
                changeObjTxt(data_report1, "Text186", "N/A")

                prntRepData(Label_nis.Text, 24)
                changeObjTxt(data_report1, "Text129", k1)
                changeObjTxt(data_report1, "Text149", l1)
                changeObjTxt(data_report1, "Text168", m1)
                changeObjTxt(data_report1, "Text187", n1)
                prntRepData(Label_nis.Text, 25)
                changeObjTxt(data_report1, "Text130", k1)
                changeObjTxt(data_report1, "Text150", l1)
                changeObjTxt(data_report1, "Text169", m1)
                changeObjTxt(data_report1, "Text188", n1)
                prntRepData(Label_nis.Text, 26)
                changeObjTxt(data_report1, "Text131", k1)
                changeObjTxt(data_report1, "Text151", l1)
                changeObjTxt(data_report1, "Text170", m1)
                changeObjTxt(data_report1, "Text189", n1)
                prntRepData(Label_nis.Text, 27)
                changeObjTxt(data_report1, "Text132", k1)
                changeObjTxt(data_report1, "Text152", l1)
                changeObjTxt(data_report1, "Text171", m1)
                changeObjTxt(data_report1, "Text190", n1)
                prntRepData(Label_nis.Text, 28)
                changeObjTxt(data_report1, "Text134", k1)
                changeObjTxt(data_report1, "Text153", l1)
                changeObjTxt(data_report1, "Text172", m1)
                changeObjTxt(data_report1, "Text191", n1)
                prntRepData(Label_nis.Text, 29)
                changeObjTxt(data_report1, "Text135", k1)
                changeObjTxt(data_report1, "Text154", l1)
                changeObjTxt(data_report1, "Text173", m1)
                changeObjTxt(data_report1, "Text192", n1)
                changeObjTxt(data_report1, "Text83", r1)
                changeObjTxt(data_report1, "Text89", o1)
                changeObjTxt(data_report1, "Text90", p1)
                changeObjTxt(data_report1, "Text91", q1)

                Button_firstItem.Enabled = True
                Button_lastItem.Enabled = True
                Button_backItem.Enabled = True
                Button_nextItem.Enabled = True
            Else
                Button_firstItem.Enabled = True
                Button_lastItem.Enabled = True
                Button_backItem.Enabled = True
                Button_nextItem.Enabled = True
                MsgBox("Siswa dengan nomor NIS : " & TextBox_searchNis.Text & " belum memiliki nilai.", MsgBoxStyle.Information, "HEXA Report")
                Label_nis.Text = a1
                TextBox_searchNis.Text = a1
                dr.Close()
                cmd.Dispose()
                con.Close()
                Exit Sub
            End If
        End If
    End Sub

    Private Sub changeObjTxt(rptName As CrystalDecisions.CrystalReports.Engine.ReportDocument, objName As String,
                             chngVal As String)
        Dim txtObj As CrystalDecisions.CrystalReports.Engine.TextObject
        txtObj = rptName.ReportDefinition.ReportObjects(objName)
        txtObj.Text = chngVal
    End Sub

    Private Sub crSetParams(crRpt As CrystalDecisions.CrystalReports.Engine.ReportDocument, crParamName As String, crParamValue As String)
        For Each pfField As CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinition In crRpt.DataDefinition.ParameterFields
            If pfField.Name.ToString().ToLower() = crParamName.ToLower() Then
                Try
                    Dim crParameterValues As New CrystalDecisions.Shared.ParameterValues()
                    Dim crParameterDiscreteValue As New CrystalDecisions.Shared.ParameterDiscreteValue()
                    crParameterDiscreteValue.Value = crParamValue
                    crParameterValues = pfField.CurrentValues
                    crParameterValues.Clear()
                    crParameterValues.Add(crParameterDiscreteValue)
                    pfField.ApplyCurrentValues(crParameterValues)
                    CrystalReportViewer1.ReportSource = crRpt
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
            End If
        Next
    End Sub

    Private Sub Label_nis_Click(sender As Object, e As EventArgs) Handles Label_nis.Click
        Label_nis.Visible = False
        TextBox_searchNis.Visible = True
    End Sub

    Private Sub TextBox_searchNis_MouseLeave(sender As Object, e As EventArgs) Handles TextBox_searchNis.MouseLeave
        TextBox_searchNis.Visible = False
        Label_nis.Visible = True
    End Sub

    Private Sub Button_nextItem_Click(sender As Object, e As EventArgs) Handles Button_nextItem.Click
        Label_nis.Text = Label_nis.Text + 1
        query = "SELECT * FROM raport WHERE nis = '" & Label_nis.Text & "'"

        con.Open()
        cmd = New MySqlCommand(query, con)
        dr = cmd.ExecuteReader()

        If dr.HasRows Then
            Button_firstItem.Enabled = False
            Button_lastItem.Enabled = False
            Button_backItem.Enabled = False
            Button_nextItem.Enabled = False
            'Do While dr.Read()
            '    search_nis = (dr.Item("nis").ToString())
            'Loop
            dr.Close()
            cmd.Dispose()
            con.Close()

            'prntRepData(studentID:=Label_nis.Text)
            TextBox_searchNis.Text = Label_nis.Text

            retStudentSpec()
            crSetParams(data_report1, "nama", nama_siswa)
            crSetParams(data_report1, "nis", nis)
            crSetParams(data_report1, "nisn", nisn)
            crSetParams(data_report1, "kelas", kelas)
            crSetParams(data_report1, "jurusan", jurusan_sngkt)
            crSetParams(data_report1, "lokal", lokal)
            crSetParams(data_report1, "semester", id_semester)
            crSetParams(data_report1, "semester_hur", ket)
            crSetParams(data_report1, "nip", nip)
            crSetParams(data_report1, "wali", nama_wali)

            crSetParams(data_report1, "date_time", date_time)
            crSetParams(data_report1, "todayDate", todayDate)

            prntRepData(Label_nis.Text, 1)
            changeObjTxt(data_report1, "Text116", k1)
            changeObjTxt(data_report1, "Text136", l1)
            changeObjTxt(data_report1, "Text155", m1)
            changeObjTxt(data_report1, "Text174", n1)
            prntRepData(Label_nis.Text, 2)
            changeObjTxt(data_report1, "Text117", k1)
            changeObjTxt(data_report1, "Text137", l1)
            changeObjTxt(data_report1, "Text156", m1)
            changeObjTxt(data_report1, "Text175", n1)
            prntRepData(Label_nis.Text, 3)
            changeObjTxt(data_report1, "Text118", k1)
            changeObjTxt(data_report1, "Text138", l1)
            changeObjTxt(data_report1, "Text157", m1)
            changeObjTxt(data_report1, "Text176", n1)
            prntRepData(Label_nis.Text, 4)
            changeObjTxt(data_report1, "Text119", k1)
            changeObjTxt(data_report1, "Text139", l1)
            changeObjTxt(data_report1, "Text158", m1)
            changeObjTxt(data_report1, "Text177", n1)
            prntRepData(Label_nis.Text, 5)
            changeObjTxt(data_report1, "Text120", k1)
            changeObjTxt(data_report1, "Text140", l1)
            changeObjTxt(data_report1, "Text159", m1)
            changeObjTxt(data_report1, "Text178", n1)
            prntRepData(Label_nis.Text, 6)
            changeObjTxt(data_report1, "Text121", k1)
            changeObjTxt(data_report1, "Text141", l1)
            changeObjTxt(data_report1, "Text160", m1)
            changeObjTxt(data_report1, "Text179", n1)
            prntRepData(Label_nis.Text, 7)
            changeObjTxt(data_report1, "Text122", k1)
            changeObjTxt(data_report1, "Text142", l1)
            changeObjTxt(data_report1, "Text161", m1)
            changeObjTxt(data_report1, "Text180", n1)
            prntRepData(Label_nis.Text, 8)
            changeObjTxt(data_report1, "Text123", k1)
            changeObjTxt(data_report1, "Text143", l1)
            changeObjTxt(data_report1, "Text162", m1)
            changeObjTxt(data_report1, "Text181", n1)
            prntRepData(Label_nis.Text, 9)
            changeObjTxt(data_report1, "Text124", k1)
            changeObjTxt(data_report1, "Text144", l1)
            changeObjTxt(data_report1, "Text163", m1)
            changeObjTxt(data_report1, "Text182", n1)
            prntRepData(Label_nis.Text, 10)
            changeObjTxt(data_report1, "Text125", k1)
            changeObjTxt(data_report1, "Text145", l1)
            changeObjTxt(data_report1, "Text164", m1)
            changeObjTxt(data_report1, "Text183", n1)

            changeObjTxt(data_report1, "Text126", "N/A")
            changeObjTxt(data_report1, "Text146", "N/A")
            changeObjTxt(data_report1, "Text165", "N/A")
            changeObjTxt(data_report1, "Text184", "N/A")
            changeObjTxt(data_report1, "Text127", "N/A")
            changeObjTxt(data_report1, "Text147", "N/A")
            changeObjTxt(data_report1, "Text166", "N/A")
            changeObjTxt(data_report1, "Text185", "N/A")
            changeObjTxt(data_report1, "Text128", "N/A")
            changeObjTxt(data_report1, "Text148", "N/A")
            changeObjTxt(data_report1, "Text167", "N/A")
            changeObjTxt(data_report1, "Text186", "N/A")

            prntRepData(Label_nis.Text, 24)
            changeObjTxt(data_report1, "Text129", k1)
            changeObjTxt(data_report1, "Text149", l1)
            changeObjTxt(data_report1, "Text168", m1)
            changeObjTxt(data_report1, "Text187", n1)
            prntRepData(Label_nis.Text, 25)
            changeObjTxt(data_report1, "Text130", k1)
            changeObjTxt(data_report1, "Text150", l1)
            changeObjTxt(data_report1, "Text169", m1)
            changeObjTxt(data_report1, "Text188", n1)
            prntRepData(Label_nis.Text, 26)
            changeObjTxt(data_report1, "Text131", k1)
            changeObjTxt(data_report1, "Text151", l1)
            changeObjTxt(data_report1, "Text170", m1)
            changeObjTxt(data_report1, "Text189", n1)
            prntRepData(Label_nis.Text, 27)
            changeObjTxt(data_report1, "Text132", k1)
            changeObjTxt(data_report1, "Text152", l1)
            changeObjTxt(data_report1, "Text171", m1)
            changeObjTxt(data_report1, "Text190", n1)
            prntRepData(Label_nis.Text, 28)
            changeObjTxt(data_report1, "Text134", k1)
            changeObjTxt(data_report1, "Text153", l1)
            changeObjTxt(data_report1, "Text172", m1)
            changeObjTxt(data_report1, "Text191", n1)
            prntRepData(Label_nis.Text, 29)
            changeObjTxt(data_report1, "Text135", k1)
            changeObjTxt(data_report1, "Text154", l1)
            changeObjTxt(data_report1, "Text173", m1)
            changeObjTxt(data_report1, "Text192", n1)
            changeObjTxt(data_report1, "Text83", r1)
            changeObjTxt(data_report1, "Text89", o1)
            changeObjTxt(data_report1, "Text90", p1)
            changeObjTxt(data_report1, "Text91", q1)

            Button_firstItem.Enabled = True
            Button_lastItem.Enabled = True
            Button_backItem.Enabled = True
            Button_nextItem.Enabled = True
        Else
            Button_firstItem.Enabled = True
            Button_lastItem.Enabled = True
            Button_backItem.Enabled = True
            Button_nextItem.Enabled = True
            Label_nis.Text = Label_nis.Text - 1
            TextBox_searchNis.Text = Label_nis.Text
            dr.Close()
            cmd.Dispose()
            con.Close()
            Exit Sub
        End If
    End Sub

    Private Sub Button_backItem_Click(sender As Object, e As EventArgs) Handles Button_backItem.Click
        Label_nis.Text = Label_nis.Text - 1
        query = "SELECT * FROM raport WHERE nis = '" & Label_nis.Text & "'"

        con.Open()
        cmd = New MySqlCommand(query, con)
        dr = cmd.ExecuteReader()

        If dr.HasRows Then
            Button_firstItem.Enabled = False
            Button_lastItem.Enabled = False
            Button_backItem.Enabled = False
            Button_nextItem.Enabled = False
            dr.Close()
            cmd.Dispose()
            con.Close()

            'prntRepData(studentID:=Label_nis.Text)
            TextBox_searchNis.Text = Label_nis.Text

            retStudentSpec()
            crSetParams(data_report1, "nama", nama_siswa)
            crSetParams(data_report1, "nis", nis)
            crSetParams(data_report1, "nisn", nisn)
            crSetParams(data_report1, "kelas", kelas)
            crSetParams(data_report1, "jurusan", jurusan_sngkt)
            crSetParams(data_report1, "lokal", lokal)
            crSetParams(data_report1, "semester", id_semester)
            crSetParams(data_report1, "semester_hur", ket)
            crSetParams(data_report1, "nip", nip)
            crSetParams(data_report1, "wali", nama_wali)

            crSetParams(data_report1, "date_time", date_time)
            crSetParams(data_report1, "todayDate", todayDate)

            prntRepData(Label_nis.Text, 1)
            changeObjTxt(data_report1, "Text116", k1)
            changeObjTxt(data_report1, "Text136", l1)
            changeObjTxt(data_report1, "Text155", m1)
            changeObjTxt(data_report1, "Text174", n1)
            prntRepData(Label_nis.Text, 2)
            changeObjTxt(data_report1, "Text117", k1)
            changeObjTxt(data_report1, "Text137", l1)
            changeObjTxt(data_report1, "Text156", m1)
            changeObjTxt(data_report1, "Text175", n1)
            prntRepData(Label_nis.Text, 3)
            changeObjTxt(data_report1, "Text118", k1)
            changeObjTxt(data_report1, "Text138", l1)
            changeObjTxt(data_report1, "Text157", m1)
            changeObjTxt(data_report1, "Text176", n1)
            prntRepData(Label_nis.Text, 4)
            changeObjTxt(data_report1, "Text119", k1)
            changeObjTxt(data_report1, "Text139", l1)
            changeObjTxt(data_report1, "Text158", m1)
            changeObjTxt(data_report1, "Text177", n1)
            prntRepData(Label_nis.Text, 5)
            changeObjTxt(data_report1, "Text120", k1)
            changeObjTxt(data_report1, "Text140", l1)
            changeObjTxt(data_report1, "Text159", m1)
            changeObjTxt(data_report1, "Text178", n1)
            prntRepData(Label_nis.Text, 6)
            changeObjTxt(data_report1, "Text121", k1)
            changeObjTxt(data_report1, "Text141", l1)
            changeObjTxt(data_report1, "Text160", m1)
            changeObjTxt(data_report1, "Text179", n1)
            prntRepData(Label_nis.Text, 7)
            changeObjTxt(data_report1, "Text122", k1)
            changeObjTxt(data_report1, "Text142", l1)
            changeObjTxt(data_report1, "Text161", m1)
            changeObjTxt(data_report1, "Text180", n1)
            prntRepData(Label_nis.Text, 8)
            changeObjTxt(data_report1, "Text123", k1)
            changeObjTxt(data_report1, "Text143", l1)
            changeObjTxt(data_report1, "Text162", m1)
            changeObjTxt(data_report1, "Text181", n1)
            prntRepData(Label_nis.Text, 9)
            changeObjTxt(data_report1, "Text124", k1)
            changeObjTxt(data_report1, "Text144", l1)
            changeObjTxt(data_report1, "Text163", m1)
            changeObjTxt(data_report1, "Text182", n1)
            prntRepData(Label_nis.Text, 10)
            changeObjTxt(data_report1, "Text125", k1)
            changeObjTxt(data_report1, "Text145", l1)
            changeObjTxt(data_report1, "Text164", m1)
            changeObjTxt(data_report1, "Text183", n1)

            changeObjTxt(data_report1, "Text126", "N/A")
            changeObjTxt(data_report1, "Text146", "N/A")
            changeObjTxt(data_report1, "Text165", "N/A")
            changeObjTxt(data_report1, "Text184", "N/A")
            changeObjTxt(data_report1, "Text127", "N/A")
            changeObjTxt(data_report1, "Text147", "N/A")
            changeObjTxt(data_report1, "Text166", "N/A")
            changeObjTxt(data_report1, "Text185", "N/A")
            changeObjTxt(data_report1, "Text128", "N/A")
            changeObjTxt(data_report1, "Text148", "N/A")
            changeObjTxt(data_report1, "Text167", "N/A")
            changeObjTxt(data_report1, "Text186", "N/A")

            prntRepData(Label_nis.Text, 24)
            changeObjTxt(data_report1, "Text129", k1)
            changeObjTxt(data_report1, "Text149", l1)
            changeObjTxt(data_report1, "Text168", m1)
            changeObjTxt(data_report1, "Text187", n1)
            prntRepData(Label_nis.Text, 25)
            changeObjTxt(data_report1, "Text130", k1)
            changeObjTxt(data_report1, "Text150", l1)
            changeObjTxt(data_report1, "Text169", m1)
            changeObjTxt(data_report1, "Text188", n1)
            prntRepData(Label_nis.Text, 26)
            changeObjTxt(data_report1, "Text131", k1)
            changeObjTxt(data_report1, "Text151", l1)
            changeObjTxt(data_report1, "Text170", m1)
            changeObjTxt(data_report1, "Text189", n1)
            prntRepData(Label_nis.Text, 27)
            changeObjTxt(data_report1, "Text132", k1)
            changeObjTxt(data_report1, "Text152", l1)
            changeObjTxt(data_report1, "Text171", m1)
            changeObjTxt(data_report1, "Text190", n1)
            prntRepData(Label_nis.Text, 28)
            changeObjTxt(data_report1, "Text134", k1)
            changeObjTxt(data_report1, "Text153", l1)
            changeObjTxt(data_report1, "Text172", m1)
            changeObjTxt(data_report1, "Text191", n1)
            prntRepData(Label_nis.Text, 29)
            changeObjTxt(data_report1, "Text135", k1)
            changeObjTxt(data_report1, "Text154", l1)
            changeObjTxt(data_report1, "Text173", m1)
            changeObjTxt(data_report1, "Text192", n1)
            changeObjTxt(data_report1, "Text83", r1)
            changeObjTxt(data_report1, "Text89", o1)
            changeObjTxt(data_report1, "Text90", p1)
            changeObjTxt(data_report1, "Text91", q1)

            Button_firstItem.Enabled = True
            Button_lastItem.Enabled = True
            Button_backItem.Enabled = True
            Button_nextItem.Enabled = True
        Else
            Button_firstItem.Enabled = True
            Button_lastItem.Enabled = True
            Button_backItem.Enabled = True
            Button_nextItem.Enabled = True
            Label_nis.Text = Label_nis.Text + 1
            TextBox_searchNis.Text = Label_nis.Text
            dr.Close()
            cmd.Dispose()
            con.Close()
            Exit Sub
        End If
    End Sub

    Private Sub Button_lastItem_Click(sender As Object, e As EventArgs) Handles Button_lastItem.Click
        query = "SELECT raport.nis, master_siswa.nama_siswa, kelas.kelas, jurusan.jurusan, lokal.lokal 
FROM raport 
INNER JOIN master_siswa ON master_siswa.nis = raport.nis 
INNER JOIN kelas ON kelas.id_kelas = master_siswa.id_kelas 
INNER JOIN jurusan ON jurusan.id_jurusan = master_siswa.id_jurusan 
INNER JOIN lokal ON lokal.id_lokal = master_siswa.id_lokal 
INNER JOIN master_guru ON master_siswa.nip = master_guru.nip 
INNER JOIN wali_kelas ON master_siswa.nip = wali_kelas.nip 
WHERE wali_kelas.nip = '" & wal & "' 
ORDER BY master_siswa.nis DESC
LIMIT 1"
        con.Open()
        cmd = New MySqlCommand(query, con)
        dr = cmd.ExecuteReader()

        If dr.HasRows() Then
            Button_firstItem.Enabled = False
            Button_lastItem.Enabled = False
            Button_backItem.Enabled = False
            Button_nextItem.Enabled = False
            Do While dr.Read()
                a1 = (dr.Item("nis").ToString())
                b1 = (dr.Item("nama_siswa").ToString())
                c1 = (dr.Item("kelas").ToString())
                d1 = (dr.Item("jurusan").ToString())
                f1 = (dr.Item("lokal").ToString())
            Loop
            Label_nis.Text = a1
            TextBox_searchNis.Text = a1
            dr.Close()
            cmd.Dispose()
            con.Close()

            'prntRepData(studentID:=Label_nis.Text)
            retStudentSpec()
            crSetParams(data_report1, "nama", nama_siswa)
            crSetParams(data_report1, "nis", nis)
            crSetParams(data_report1, "nisn", nisn)
            crSetParams(data_report1, "kelas", kelas)
            crSetParams(data_report1, "jurusan", jurusan_sngkt)
            crSetParams(data_report1, "lokal", lokal)
            crSetParams(data_report1, "semester", id_semester)
            crSetParams(data_report1, "semester_hur", ket)
            crSetParams(data_report1, "nip", nip)
            crSetParams(data_report1, "wali", nama_wali)

            crSetParams(data_report1, "date_time", date_time)
            crSetParams(data_report1, "todayDate", todayDate)

            prntRepData(Label_nis.Text, 1)
            changeObjTxt(data_report1, "Text116", k1)
            changeObjTxt(data_report1, "Text136", l1)
            changeObjTxt(data_report1, "Text155", m1)
            changeObjTxt(data_report1, "Text174", n1)
            prntRepData(Label_nis.Text, 2)
            changeObjTxt(data_report1, "Text117", k1)
            changeObjTxt(data_report1, "Text137", l1)
            changeObjTxt(data_report1, "Text156", m1)
            changeObjTxt(data_report1, "Text175", n1)
            prntRepData(Label_nis.Text, 3)
            changeObjTxt(data_report1, "Text118", k1)
            changeObjTxt(data_report1, "Text138", l1)
            changeObjTxt(data_report1, "Text157", m1)
            changeObjTxt(data_report1, "Text176", n1)
            prntRepData(Label_nis.Text, 4)
            changeObjTxt(data_report1, "Text119", k1)
            changeObjTxt(data_report1, "Text139", l1)
            changeObjTxt(data_report1, "Text158", m1)
            changeObjTxt(data_report1, "Text177", n1)
            prntRepData(Label_nis.Text, 5)
            changeObjTxt(data_report1, "Text120", k1)
            changeObjTxt(data_report1, "Text140", l1)
            changeObjTxt(data_report1, "Text159", m1)
            changeObjTxt(data_report1, "Text178", n1)
            prntRepData(Label_nis.Text, 6)
            changeObjTxt(data_report1, "Text121", k1)
            changeObjTxt(data_report1, "Text141", l1)
            changeObjTxt(data_report1, "Text160", m1)
            changeObjTxt(data_report1, "Text179", n1)
            prntRepData(Label_nis.Text, 7)
            changeObjTxt(data_report1, "Text122", k1)
            changeObjTxt(data_report1, "Text142", l1)
            changeObjTxt(data_report1, "Text161", m1)
            changeObjTxt(data_report1, "Text180", n1)
            prntRepData(Label_nis.Text, 8)
            changeObjTxt(data_report1, "Text123", k1)
            changeObjTxt(data_report1, "Text143", l1)
            changeObjTxt(data_report1, "Text162", m1)
            changeObjTxt(data_report1, "Text181", n1)
            prntRepData(Label_nis.Text, 9)
            changeObjTxt(data_report1, "Text124", k1)
            changeObjTxt(data_report1, "Text144", l1)
            changeObjTxt(data_report1, "Text163", m1)
            changeObjTxt(data_report1, "Text182", n1)
            prntRepData(Label_nis.Text, 10)
            changeObjTxt(data_report1, "Text125", k1)
            changeObjTxt(data_report1, "Text145", l1)
            changeObjTxt(data_report1, "Text164", m1)
            changeObjTxt(data_report1, "Text183", n1)

            changeObjTxt(data_report1, "Text126", "N/A")
            changeObjTxt(data_report1, "Text146", "N/A")
            changeObjTxt(data_report1, "Text165", "N/A")
            changeObjTxt(data_report1, "Text184", "N/A")
            changeObjTxt(data_report1, "Text127", "N/A")
            changeObjTxt(data_report1, "Text147", "N/A")
            changeObjTxt(data_report1, "Text166", "N/A")
            changeObjTxt(data_report1, "Text185", "N/A")
            changeObjTxt(data_report1, "Text128", "N/A")
            changeObjTxt(data_report1, "Text148", "N/A")
            changeObjTxt(data_report1, "Text167", "N/A")
            changeObjTxt(data_report1, "Text186", "N/A")

            prntRepData(Label_nis.Text, 24)
            changeObjTxt(data_report1, "Text129", k1)
            changeObjTxt(data_report1, "Text149", l1)
            changeObjTxt(data_report1, "Text168", m1)
            changeObjTxt(data_report1, "Text187", n1)
            prntRepData(Label_nis.Text, 25)
            changeObjTxt(data_report1, "Text130", k1)
            changeObjTxt(data_report1, "Text150", l1)
            changeObjTxt(data_report1, "Text169", m1)
            changeObjTxt(data_report1, "Text188", n1)
            prntRepData(Label_nis.Text, 26)
            changeObjTxt(data_report1, "Text131", k1)
            changeObjTxt(data_report1, "Text151", l1)
            changeObjTxt(data_report1, "Text170", m1)
            changeObjTxt(data_report1, "Text189", n1)
            prntRepData(Label_nis.Text, 27)
            changeObjTxt(data_report1, "Text132", k1)
            changeObjTxt(data_report1, "Text152", l1)
            changeObjTxt(data_report1, "Text171", m1)
            changeObjTxt(data_report1, "Text190", n1)
            prntRepData(Label_nis.Text, 28)
            changeObjTxt(data_report1, "Text134", k1)
            changeObjTxt(data_report1, "Text153", l1)
            changeObjTxt(data_report1, "Text172", m1)
            changeObjTxt(data_report1, "Text191", n1)
            prntRepData(Label_nis.Text, 29)
            changeObjTxt(data_report1, "Text135", k1)
            changeObjTxt(data_report1, "Text154", l1)
            changeObjTxt(data_report1, "Text173", m1)
            changeObjTxt(data_report1, "Text192", n1)
            changeObjTxt(data_report1, "Text83", r1)
            changeObjTxt(data_report1, "Text89", o1)
            changeObjTxt(data_report1, "Text90", p1)
            changeObjTxt(data_report1, "Text91", q1)

            Button_firstItem.Enabled = True
            Button_lastItem.Enabled = True
            Button_backItem.Enabled = True
            Button_nextItem.Enabled = True
        Else
            Button_firstItem.Enabled = True
            Button_lastItem.Enabled = True
            Button_backItem.Enabled = True
            Button_nextItem.Enabled = True
            Label_nis.Text = a1
            TextBox_searchNis.Text = a1
            dr.Close()
            cmd.Dispose()
            con.Close()
            Exit Sub
        End If
    End Sub
End Class
