﻿Imports MySql.Data.MySqlClient
Public Class grade_form
    Public filledBoxes As Control

    Private Sub Label_nis_Click(sender As Object, e As EventArgs) Handles Label_nis.Click
        Label_nis.Visible = False
        TextBox_searchNis.Visible = True
    End Sub

    Private Sub TextBox_searchNis_MouseLeave(sender As Object, e As EventArgs) Handles TextBox_searchNis.MouseLeave
        TextBox_searchNis.Visible = False
        Label_nis.Visible = True
    End Sub

    Private Sub Button_save_Click(sender As Object, e As EventArgs) Handles Button_save.Click
        Dim emptyTextBoxes =
            From TextBox In Me.MetroTabControl1.TabPages(0).Controls.OfType(Of TextBox)()
            Where TextBox.Text.Trim.Length = 0 And TextBox.Visible = True
            Select TextBox.Tag
        If emptyTextBoxes.Any Then
            MsgBox(String.Format("Harap isi terlebih dahulu nilai-nilai yang masih kosong"),
                   MsgBoxStyle.Information, "HEXA Report")
        ElseIf Label_smes.Text = 0 Then
            MsgBox("Semester tidak boleh bernilai 0, harap isi semester", MsgBoxStyle.Information, "HEXA Report")
        Else

            If Me.Tag = "Add" Then
                query = "INSERT INTO raport(nis, id_mapel, id_semester, nilai_peng, nilai_hurPeng, nilai_ket, nilai_hurKet)" _
            & "VALUES('" & Label_nisSis.Text & "', '" & id(0) & "', 5, '" & TextBox_pengPai.Text & "', '" & Label_hurPengPai.Text & "', '" & TextBox_ketPai.Text & "', '" & Label_hurKetPai.Text & "')," _
            & "('" & Label_nisSis.Text & "', '" & id(1) & "', '" & Label_smes.Text & "', '" & TextBox_pengPkn.Text & "', '" & Label_hurPengPkn.Text & "', '" & TextBox_ketPkn.Text & "', '" & Label_hurKetPkn.Text & "')," _
            & "('" & Label_nisSis.Text & "', '" & id(2) & "', '" & Label_smes.Text & "', '" & TextBox_pengIndo.Text & "', '" & Label_hurPengIndo.Text & "', '" & TextBox_ketIndo.Text & "', '" & Label_hurKetIndo.Text & "')," _
            & "('" & Label_nisSis.Text & "', '" & id(3) & "', '" & Label_smes.Text & "', '" & TextBox_pengMtk.Text & "', '" & Label_hurPengMtk.Text & "', '" & TextBox_ketMtk.Text & "', '" & Label_hurKetMtk.Text & "')," _
            & "('" & Label_nisSis.Text & "', '" & id(4) & "', '" & Label_smes.Text & "', '" & TextBox_pengSej.Text & "', '" & Label_hurPengSej.Text & "', '" & TextBox_ketSej.Text & "', '" & Label_hurKetSej.Text & "')," _
            & "('" & Label_nisSis.Text & "', '" & id(5) & "', '" & Label_smes.Text & "', '" & TextBox_pengIng.Text & "', '" & Label_hurPengIng.Text & "', '" & TextBox_ketIng.Text & "', '" & Label_hurKetIng.Text & "')," _
            & "('" & Label_nisSis.Text & "', '" & id(6) & "', '" & Label_smes.Text & "', '" & TextBox_pengSbk.Text & "', '" & Label_hurPengSbk.Text & "', '" & TextBox_ketSbk.Text & "', '" & Label_hurKetSbk.Text & "')," _
            & "('" & Label_nisSis.Text & "', '" & id(7) & "', '" & Label_smes.Text & "', '" & TextBox_pengPen.Text & "', '" & Label_hurPengPen.Text & "', '" & TextBox_ketPen.Text & "', '" & Label_hurKetPen.Text & "')," _
            & "('" & Label_nisSis.Text & "', '" & id(8) & "', '" & Label_smes.Text & "', '" & TextBox_pengKwh.Text & "', '" & Label_hurPengKwh.Text & "', '" & TextBox_ketKwh.Text & "', '" & Label_hurKetKwh.Text & "')," _
            & "('" & Label_nisSis.Text & "', '" & id(9) & "', '" & Label_smes.Text & "', '" & TextBox_pengMul.Text & "', '" & Label_hurPengMul.Text & "', '" & TextBox_ketMul.Text & "', '" & Label_hurKetMul.Text & "')," _
            & "('" & Label_nisSis.Text & "', '" & id(10) & "', '" & Label_smes.Text & "', '" & TextBox_pengProd1.Text & "', '" & Label_hurPengProd1.Text & "', '" & TextBox_ketProd1.Text & "', '" & Label_hurKetProd1.Text & "')," _
            & "('" & Label_nisSis.Text & "', '" & id(11) & "', '" & Label_smes.Text & "', '" & TextBox_pengProd2.Text & "', '" & Label_hurPengProd2.Text & "', '" & TextBox_ketProd2.Text & "', '" & Label_hurKetProd2.Text & "')," _
            & "('" & Label_nisSis.Text & "', '" & id(12) & "', '" & Label_smes.Text & "', '" & TextBox_pengProd3.Text & "', '" & Label_hurPengProd3.Text & "', '" & TextBox_ketProd3.Text & "', '" & Label_hurKetProd3.Text & "')," _
            & "('" & Label_nisSis.Text & "', '" & id(13) & "', '" & Label_smes.Text & "', '" & TextBox_pengProd4.Text & "', '" & Label_hurPengProd4.Text & "', '" & TextBox_ketProd4.Text & "', '" & Label_hurKetProd4.Text & "')," _
            & "('" & Label_nisSis.Text & "', '" & id(14) & "', '" & Label_smes.Text & "', '" & TextBox_pengProd5.Text & "', '" & Label_hurPengProd5.Text & "', '" & TextBox_ketProd5.Text & "', '" & Label_hurKetProd5.Text & "')," _
            & "('" & Label_nisSis.Text & "', '" & id(15) & "', '" & Label_smes.Text & "', '" & TextBox_pengProd6.Text & "', '" & Label_hurPengProd6.Text & "', '" & TextBox_ketProd6.Text & "', '" & Label_hurKetProd6.Text & "');" _
            & "INSERT INTO supporting_grade(nis, id_semester, ket_sakit, ket_izin, ket_alpha, ekskul, komen)" _
            & "VALUES('" & Label_nisSis.Text & "', '" & Label_smes.Text & "', '" & TextBox_sakit.Text & "', '" & TextBox_izin.Text & "', '" & TextBox_alpha.Text & "', '" & MetroComboBox_ekskul.Text & "', '" & TextBox_komen.Text & "');"

                con.Open()
                openDB()
                cmd.Connection = con
                cmd.CommandText = query
                cmd.ExecuteNonQuery()
                MsgBox("Input nilai berhasil.", MsgBoxStyle.Information, "HEXA Report")
                cmd.Dispose()
                con.Close()
            ElseIf Me.Tag = "Edit" Then
                query = "UPDATE raport SET nilai_peng = '" & TextBox_pengPai.Text & "', nilai_hurPeng = '" & Label_hurPengPai.Text & "', nilai_ket = '" & TextBox_ketPai.Text & "', nilai_hurKet = '" & Label_hurKetPai.Text & "' WHERE nis = '" _
                & Label_nisSis.Text & "' AND id_mapel = '" & id(1) & "' AND id_semester = '" & Label_smes.Text & "';" _
                & "UPDATE raport SET nilai_peng = '" & TextBox_pengPkn.Text & "', nilai_hurPeng = '" & Label_hurPengPkn.Text & "', nilai_ket = '" & TextBox_ketPkn.Text & "', nilai_hurKet = '" & Label_hurKetPkn.Text & "' WHERE nis = '" _
                & Label_nisSis.Text & "' AND id_mapel = '" & id(2) & "' AND id_semester = '" & Label_smes.Text & "';" _
                & "UPDATE raport SET nilai_peng = '" & TextBox_pengIndo.Text & "', nilai_hurPeng = '" & Label_hurPengIndo.Text & "', nilai_ket = '" & TextBox_ketIndo.Text & "', nilai_hurKet = '" & Label_hurKetIndo.Text & "' WHERE nis = '" _
                & Label_nisSis.Text & "' AND id_mapel = '" & id(3) & "' AND id_semester = '" & Label_smes.Text & "';" _
                & "UPDATE raport SET nilai_peng = '" & TextBox_pengMtk.Text & "', nilai_hurPeng = '" & Label_hurPengMtk.Text & "', nilai_ket = '" & TextBox_ketMtk.Text & "', nilai_hurKet = '" & Label_hurKetMtk.Text & "' WHERE nis = '" _
                & Label_nisSis.Text & "' AND id_mapel = '" & id(4) & "' AND id_semester = '" & Label_smes.Text & "';" _
                & "UPDATE raport SET nilai_peng = '" & TextBox_pengSej.Text & "', nilai_hurPeng = '" & Label_hurPengSej.Text & "', nilai_ket = '" & TextBox_ketSej.Text & "', nilai_hurKet = '" & Label_hurKetSej.Text & "' WHERE nis = '" _
                & Label_nisSis.Text & "' AND id_mapel = '" & id(5) & "' AND id_semester = '" & Label_smes.Text & "';" _
                & "UPDATE raport SET nilai_peng = '" & TextBox_pengIng.Text & "', nilai_hurPeng = '" & Label_hurPengIng.Text & "', nilai_ket = '" & TextBox_ketIng.Text & "', nilai_hurKet = '" & Label_hurKetIng.Text & "' WHERE nis = '" _
                & Label_nisSis.Text & "' AND id_mapel = '" & id(6) & "' AND id_semester = '" & Label_smes.Text & "';" _
                & "UPDATE raport SET nilai_peng = '" & TextBox_pengSbk.Text & "', nilai_hurPeng = '" & Label_hurPengSbk.Text & "', nilai_ket = '" & TextBox_ketSbk.Text & "', nilai_hurKet = '" & Label_hurKetSbk.Text & "' WHERE nis = '" _
                & Label_nisSis.Text & "' AND id_mapel = '" & id(7) & "' AND id_semester = '" & Label_smes.Text & "';" _
                & "UPDATE raport SET nilai_peng = '" & TextBox_pengPen.Text & "', nilai_hurPeng = '" & Label_hurPengPen.Text & "', nilai_ket = '" & TextBox_ketPen.Text & "', nilai_hurKet = '" & Label_hurKetPen.Text & "' WHERE nis = '" _
                & Label_nisSis.Text & "' AND id_mapel = '" & id(8) & "' AND id_semester = '" & Label_smes.Text & "';" _
                & "UPDATE raport SET nilai_peng = '" & TextBox_pengKwh.Text & "', nilai_hurPeng = '" & Label_hurPengKwh.Text & "', nilai_ket = '" & TextBox_ketKwh.Text & "', nilai_hurKet = '" & Label_hurKetKwh.Text & "' WHERE nis = '" _
                & Label_nisSis.Text & "' AND id_mapel = '" & id(9) & "' AND id_semester = '" & Label_smes.Text & "';" _
                & "UPDATE raport SET nilai_peng = '" & TextBox_pengMul.Text & "', nilai_hurPeng = '" & Label_hurPengMul.Text & "', nilai_ket = '" & TextBox_ketMul.Text & "', nilai_hurKet = '" & Label_hurKetMul.Text & "' WHERE nis = '" _
                & Label_nisSis.Text & "' AND id_mapel = '" & id(10) & "' AND id_semester = '" & Label_smes.Text & "';" _
                & "UPDATE raport SET nilai_peng = '" & TextBox_pengProd1.Text & "', nilai_hurPeng = '" & Label_hurPengProd1.Text & "', nilai_ket = '" & TextBox_ketProd1.Text & "', nilai_hurKet = '" & Label_hurKetProd1.Text & "' WHERE nis = '" _
                & Label_nisSis.Text & "' AND id_mapel = '" & id(11) & "' AND id_semester = '" & Label_smes.Text & "';" _
                & "UPDATE raport SET nilai_peng = '" & TextBox_pengProd2.Text & "', nilai_hurPeng = '" & Label_hurPengProd2.Text & "', nilai_ket = '" & TextBox_ketProd2.Text & "', nilai_hurKet = '" & Label_hurKetProd2.Text & "' WHERE nis = '" _
                & Label_nisSis.Text & "' AND id_mapel = '" & id(12) & "' AND id_semester = '" & Label_smes.Text & "';" _
                & "UPDATE raport SET nilai_peng = '" & TextBox_pengProd3.Text & "', nilai_hurPeng = '" & Label_hurPengProd3.Text & "', nilai_ket = '" & TextBox_ketProd3.Text & "', nilai_hurKet = '" & Label_hurKetProd3.Text & "' WHERE nis = '" _
                & Label_nisSis.Text & "' AND id_mapel = '" & id(13) & "' AND id_semester = '" & Label_smes.Text & "';" _
                & "UPDATE raport SET nilai_peng = '" & TextBox_pengProd4.Text & "', nilai_hurPeng = '" & Label_hurPengProd4.Text & "', nilai_ket = '" & TextBox_ketProd4.Text & "', nilai_hurKet = '" & Label_hurKetProd4.Text & "' WHERE nis = '" _
                & Label_nisSis.Text & "' AND id_mapel = '" & id(14) & "' AND id_semester = '" & Label_smes.Text & "';" _
                & "UPDATE raport SET nilai_peng = '" & TextBox_pengProd5.Text & "', nilai_hurPeng = '" & Label_hurPengProd5.Text & "', nilai_ket = '" & TextBox_ketProd5.Text & "', nilai_hurKet = '" & Label_hurKetProd5.Text & "' WHERE nis = '" _
                & Label_nisSis.Text & "' AND id_mapel = '" & id(15) & "' AND id_semester = '" & Label_smes.Text & "';" _
                & "UPDATE raport SET nilai_peng = '" & TextBox_pengProd6.Text & "', nilai_hurPeng = '" & Label_hurPengProd6.Text & "', nilai_ket = '" & TextBox_ketProd6.Text & "', nilai_hurKet = '" & Label_hurKetProd6.Text & "' WHERE nis = '" _
                & Label_nisSis.Text & "' AND id_mapel = '" & id(16) & "' AND id_semester = '" & Label_smes.Text & "';" _
                & "UPDATE supporting_grade SET ket_sakit = '" & TextBox_sakit.Text & "', ket_izin = '" & TextBox_izin.Text & "', ket_alpha = '" & TextBox_alpha.Text & "', ekskul = '" & MetroComboBox_ekskul.Text & "', komen = '" & TextBox_komen.Text & "' WHERE nis = '" _
                & Label_nisSis.Text & "' AND id_semester = '" & Label_smes.Text & "';"

                con.Open()
                openDB()
                cmd.Connection = con
                cmd.CommandText = query
                cmd.ExecuteNonQuery()

                MsgBox("Edit nilai berhasil.", MsgBoxStyle.Information, "HEXA Report")
                cmd.Dispose()
                con.Close()
            End If
        End If
    End Sub

    Private Sub TextBox_searchNis_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox_searchNis.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) = True Then
            Button_firstItem.Enabled = False
            Button_nextItem.Enabled = False
            Button_lastItem.Enabled = False
            Button_backItem.Enabled = False
            e.Handled = True
            If Me.Tag = "Add" Then
                query = "SELECT master_siswa.nis, master_siswa.nisn, master_siswa.nama_siswa, kelas.kelas, kelas.rom, jurusan.jurusan, jurusan.jurusan_sngkt, lokal.lokal, master_siswa.nip, master_guru.nama
FROM master_siswa
INNER JOIN kelas ON kelas.id_kelas = master_siswa.id_kelas
INNER JOIN jurusan ON jurusan.id_jurusan = master_siswa.id_jurusan
INNER JOIN lokal ON lokal.id_lokal = master_siswa.id_lokal
INNER JOIN master_guru ON master_siswa.nip = master_guru.nip
INNER JOIN wali_kelas ON master_siswa.nip = wali_kelas.nip
WHERE wali_kelas.nip = '" & wal & "' AND master_siswa.nis = '" & MySqlHelper.EscapeString(TextBox_searchNis.Text) & "'
ORDER BY master_siswa.nis ASC
LIMIT 1"

                con.Open()
                cmd = New MySqlCommand(query, con)
                dr = cmd.ExecuteReader()

                Do While dr.Read()
                    a1 = (dr.Item("nis").ToString())
                    g1 = (dr.Item("nisn").ToString())
                    b1 = (dr.Item("nama_siswa").ToString())
                    c1 = (dr.Item("kelas").ToString())
                    c2 = (dr.Item("rom").ToString())
                    d1 = (dr.Item("jurusan").ToString())
                    d2 = (dr.Item("jurusan_sngkt").ToString())
                    f1 = (dr.Item("lokal").ToString())
                Loop
                Label_nisSis.Text = a1
                Label_namaSiswa.Text = UCase(b1)
                Label_kelas.Text = UCase(c2 & " " & d2 & " " & f1)
                Label_nis.Text = a1
                Label_jur.Text = UCase(d1)
                TextBox_searchNis.Text = a1
                dr.Close()
                cmd.Dispose()
                con.Close()
                Button_firstItem.Enabled = True
                Button_nextItem.Enabled = True
                Button_lastItem.Enabled = True
                Button_backItem.Enabled = True
            ElseIf Me.Tag = "Edit" Then
                If TextBox_searchNis.Text = "" Then
                    Exit Sub
                Else
                    query = "SELECT * FROM raport WHERE nis = '" & MySqlHelper.EscapeString(TextBox_searchNis.Text) & "'"

                    con.Open()
                    cmd = New MySqlCommand(query, con)
                    dr = cmd.ExecuteReader()

                    If dr.HasRows Then
                        dr.Close()
                        cmd.Dispose()
                        con.Close()
                        retRepData(nextItem:=1, studentID:=TextBox_searchNis.Text)
                        TextBox_pengPai.Text = k1
                        TextBox_ketPai.Text = m1
                        id(1) = i1
                        retRepData(nextItem:=2, studentID:=TextBox_searchNis.Text)
                        TextBox_pengPkn.Text = k1
                        TextBox_ketPkn.Text = m1
                        id(2) = i1
                        retRepData(nextItem:=3, studentID:=TextBox_searchNis.Text)
                        TextBox_pengIndo.Text = k1
                        TextBox_ketIndo.Text = m1
                        id(3) = i1
                        retRepData(nextItem:=4, studentID:=TextBox_searchNis.Text)
                        TextBox_pengMtk.Text = k1
                        TextBox_ketMtk.Text = m1
                        id(4) = i1
                        retRepData(nextItem:=5, studentID:=TextBox_searchNis.Text)
                        TextBox_pengSej.Text = k1
                        TextBox_ketSej.Text = m1
                        id(5) = i1
                        retRepData(nextItem:=6, studentID:=TextBox_searchNis.Text)
                        TextBox_pengIng.Text = k1
                        TextBox_ketIng.Text = m1
                        id(6) = i1
                        retRepData(nextItem:=7, studentID:=TextBox_searchNis.Text)
                        TextBox_pengSbk.Text = k1
                        TextBox_ketSbk.Text = m1
                        id(7) = i1
                        retRepData(nextItem:=8, studentID:=TextBox_searchNis.Text)
                        TextBox_pengPen.Text = k1
                        TextBox_ketPen.Text = m1
                        id(8) = i1
                        retRepData(nextItem:=9, studentID:=TextBox_searchNis.Text)
                        TextBox_pengKwh.Text = k1
                        TextBox_ketKwh.Text = m1
                        id(9) = i1
                        retRepData(nextItem:=10, studentID:=TextBox_searchNis.Text)
                        TextBox_pengMul.Text = k1
                        TextBox_ketMul.Text = m1
                        id(10) = i1
                        retRepData(nextItem:=24, studentID:=TextBox_searchNis.Text)
                        TextBox_pengProd1.Text = k1
                        TextBox_ketProd1.Text = m1
                        id(11) = i1
                        retRepData(nextItem:=25, studentID:=TextBox_searchNis.Text)
                        TextBox_pengProd2.Text = k1
                        TextBox_ketProd2.Text = m1
                        id(12) = i1
                        retRepData(nextItem:=26, studentID:=TextBox_searchNis.Text)
                        TextBox_pengProd3.Text = k1
                        TextBox_ketProd3.Text = m1
                        id(13) = i1
                        retRepData(nextItem:=27, studentID:=TextBox_searchNis.Text)
                        TextBox_pengProd4.Text = k1
                        TextBox_ketProd4.Text = m1
                        id(14) = i1

                        Label_nisSis.Text = a1
                        Label_namaSiswa.Text = UCase(b1)
                        Label_kelas.Text = UCase(c2 & " " & d2 & " " & f1)
                        Label_nis.Text = a1
                        Label_jur.Text = UCase(d1)
                        'TextBox_searchNis.Text = a1

                        TextBox_sakit.Text = o1
                        TextBox_izin.Text = p1
                        TextBox_alpha.Text = q1
                        MetroComboBox_ekskul.Text = r1
                        TextBox_komen.Text = s1
                        Button_firstItem.Enabled = True
                        Button_nextItem.Enabled = True
                        Button_lastItem.Enabled = True
                        Button_backItem.Enabled = True
                    Else
                        Button_firstItem.Enabled = True
                        Button_nextItem.Enabled = True
                        Button_lastItem.Enabled = True
                        Button_backItem.Enabled = True
                        dr.Close()
                        cmd.Dispose()
                        con.Close()
                        MsgBox("Siswa dengan nomor NIS : " & TextBox_searchNis.Text & " belum memiliki nilai.", MsgBoxStyle.Information, "HEXA Report")
                        Label_nis.Text = a1
                        TextBox_searchNis.Text = a1
                        Exit Sub
                    End If
                End If
            End If
        Else
            Exit Sub
        End If
    End Sub

    Private Sub Button_backItem_Click(sender As Object, e As EventArgs) Handles Button_backItem.Click
        Label_nis.Text = a1 - 1
        If Me.Tag = "Add" Then
            Button_firstItem.Enabled = False
            Button_nextItem.Enabled = False
            Button_lastItem.Enabled = False
            Button_backItem.Enabled = False
            query = "SELECT master_siswa.nis, master_siswa.nisn, master_siswa.nama_siswa, kelas.kelas, kelas.rom, jurusan.jurusan, jurusan.jurusan_sngkt, lokal.lokal, master_siswa.nip, master_guru.nama
FROM master_siswa
INNER JOIN kelas ON kelas.id_kelas = master_siswa.id_kelas
INNER JOIN jurusan ON jurusan.id_jurusan = master_siswa.id_jurusan
INNER JOIN lokal ON lokal.id_lokal = master_siswa.id_lokal
INNER JOIN master_guru ON master_siswa.nip = master_guru.nip
INNER JOIN wali_kelas ON master_siswa.nip = wali_kelas.nip
WHERE wali_kelas.nip = '" & wal & "' AND master_siswa.nis = '" & Label_nis.Text & "'
ORDER BY master_siswa.nis ASC
LIMIT 1"

            con.Open()
            cmd = New MySqlCommand(query, con)
            dr = cmd.ExecuteReader()
            If dr.HasRows() Then
                Do While dr.Read()
                    a1 = (dr.Item("nis").ToString())
                    g1 = (dr.Item("nisn").ToString())
                    b1 = (dr.Item("nama_siswa").ToString())
                    c1 = (dr.Item("kelas").ToString())
                    c2 = (dr.Item("rom").ToString())
                    d1 = (dr.Item("jurusan").ToString())
                    d2 = (dr.Item("jurusan_sngkt").ToString())
                    f1 = (dr.Item("lokal").ToString())
                Loop
                Label_nisSis.Text = a1
                Label_namaSiswa.Text = UCase(b1)
                Label_kelas.Text = UCase(c2 & " " & d2 & " " & f1)
                Label_nis.Text = a1
                Label_jur.Text = UCase(d1)
                TextBox_searchNis.Text = a1
                dr.Close()
                cmd.Dispose()
                con.Close()

                Button_firstItem.Enabled = True
                Button_nextItem.Enabled = True
                Button_lastItem.Enabled = True
                Button_backItem.Enabled = True
            Else
                Button_firstItem.Enabled = True
                Button_nextItem.Enabled = True
                Button_lastItem.Enabled = True
                Button_backItem.Enabled = True
                Label_nis.Text = a1
                dr.Close()
                cmd.Dispose()
                con.Close()
                Exit Sub
            End If
        ElseIf Me.Tag = "Edit" Then
            Button_firstItem.Enabled = False
            Button_nextItem.Enabled = False
            Button_lastItem.Enabled = False
            Button_backItem.Enabled = False
            query = "SELECT * FROM raport WHERE nis = '" & Label_nis.Text & "'"

            con.Open()
            cmd = New MySqlCommand(query, con)
            dr = cmd.ExecuteReader()

            If dr.HasRows Then
                dr.Close()
                cmd.Dispose()
                con.Close()

                retRepData(nextItem:=1, studentID:=Label_nis.Text)
                TextBox_pengPai.Text = k1
                TextBox_ketPai.Text = m1
                id(1) = i1
                retRepData(nextItem:=2, studentID:=Label_nis.Text)
                TextBox_pengPkn.Text = k1
                TextBox_ketPkn.Text = m1
                id(2) = i1
                retRepData(nextItem:=3, studentID:=Label_nis.Text)
                TextBox_pengIndo.Text = k1
                TextBox_ketIndo.Text = m1
                id(3) = i1
                retRepData(nextItem:=4, studentID:=Label_nis.Text)
                TextBox_pengMtk.Text = k1
                TextBox_ketMtk.Text = m1
                id(4) = i1
                retRepData(nextItem:=5, studentID:=Label_nis.Text)
                TextBox_pengSej.Text = k1
                TextBox_ketSej.Text = m1
                id(5) = i1
                retRepData(nextItem:=6, studentID:=Label_nis.Text)
                TextBox_pengIng.Text = k1
                TextBox_ketIng.Text = m1
                id(6) = i1
                retRepData(nextItem:=7, studentID:=Label_nis.Text)
                TextBox_pengSbk.Text = k1
                TextBox_ketSbk.Text = m1
                id(7) = i1
                retRepData(nextItem:=8, studentID:=Label_nis.Text)
                TextBox_pengPen.Text = k1
                TextBox_ketPen.Text = m1
                id(8) = i1
                retRepData(nextItem:=9, studentID:=Label_nis.Text)
                TextBox_pengKwh.Text = k1
                TextBox_ketKwh.Text = m1
                id(9) = i1
                retRepData(nextItem:=10, studentID:=Label_nis.Text)
                TextBox_pengMul.Text = k1
                TextBox_ketMul.Text = m1
                id(10) = i1
                retRepData(nextItem:=24, studentID:=Label_nis.Text)
                TextBox_pengProd1.Text = k1
                TextBox_ketProd1.Text = m1
                id(11) = i1
                retRepData(nextItem:=25, studentID:=Label_nis.Text)
                TextBox_pengProd2.Text = k1
                TextBox_ketProd2.Text = m1
                id(12) = i1
                retRepData(nextItem:=26, studentID:=Label_nis.Text)
                TextBox_pengProd3.Text = k1
                TextBox_ketProd3.Text = m1
                id(13) = i1
                retRepData(nextItem:=27, studentID:=Label_nis.Text)
                TextBox_pengProd4.Text = k1
                TextBox_ketProd4.Text = m1
                id(14) = i1

                Label_nisSis.Text = a1
                Label_namaSiswa.Text = UCase(b1)
                Label_kelas.Text = UCase(c2 & " " & d2 & " " & f1)
                Label_nis.Text = a1
                Label_jur.Text = UCase(d1)
                TextBox_searchNis.Text = a1

                TextBox_sakit.Text = o1
                TextBox_izin.Text = p1
                TextBox_alpha.Text = q1
                MetroComboBox_ekskul.Text = r1
                TextBox_komen.Text = s1

                Button_firstItem.Enabled = True
                Button_nextItem.Enabled = True
                Button_lastItem.Enabled = True
                Button_backItem.Enabled = True
            Else
                Button_firstItem.Enabled = True
                Button_nextItem.Enabled = True
                Button_lastItem.Enabled = True
                Button_backItem.Enabled = True
                Label_nis.Text = a1
                dr.Close()
                cmd.Dispose()
                con.Close()
                Exit Sub
            End If
        End If
    End Sub

    Private Sub Button_nextItem_Click(sender As Object, e As EventArgs) Handles Button_nextItem.Click
        Label_nis.Text = a1 + 1
        If Me.Tag = "Add" Then
            Button_firstItem.Enabled = False
            Button_nextItem.Enabled = False
            Button_lastItem.Enabled = False
            Button_backItem.Enabled = False
            query = "SELECT master_siswa.nis, master_siswa.nisn, master_siswa.nama_siswa, kelas.kelas, kelas.rom, jurusan.jurusan, jurusan.jurusan_sngkt, lokal.lokal, master_siswa.nip, master_guru.nama
FROM master_siswa
INNER JOIN kelas ON kelas.id_kelas = master_siswa.id_kelas
INNER JOIN jurusan ON jurusan.id_jurusan = master_siswa.id_jurusan
INNER JOIN lokal ON lokal.id_lokal = master_siswa.id_lokal
INNER JOIN master_guru ON master_siswa.nip = master_guru.nip
INNER JOIN wali_kelas ON master_siswa.nip = wali_kelas.nip
WHERE wali_kelas.nip = '" & wal & "' AND master_siswa.nis = '" & Label_nis.Text & "'
ORDER BY master_siswa.nis ASC
LIMIT 1"

            con.Open()
            cmd = New MySqlCommand(query, con)
            dr = cmd.ExecuteReader()

            If dr.HasRows() Then
                Do While dr.Read()
                    a1 = (dr.Item("nis").ToString())
                    g1 = (dr.Item("nisn").ToString())
                    b1 = (dr.Item("nama_siswa").ToString())
                    c1 = (dr.Item("kelas").ToString())
                    c2 = (dr.Item("rom").ToString())
                    d1 = (dr.Item("jurusan").ToString())
                    d2 = (dr.Item("jurusan_sngkt").ToString())
                    f1 = (dr.Item("lokal").ToString())
                Loop
                Label_nisSis.Text = a1
                Label_namaSiswa.Text = UCase(b1)
                Label_kelas.Text = UCase(c2 & " " & d2 & " " & f1)
                Label_nis.Text = a1
                Label_jur.Text = UCase(d1)
                TextBox_searchNis.Text = a1
                dr.Close()
                cmd.Dispose()
                con.Close()
                Button_firstItem.Enabled = True
                Button_nextItem.Enabled = True
                Button_lastItem.Enabled = True
                Button_backItem.Enabled = True
            Else
                Button_firstItem.Enabled = True
                Button_nextItem.Enabled = True
                Button_lastItem.Enabled = True
                Button_backItem.Enabled = True
                Label_nis.Text = a1
                dr.Close()
                cmd.Dispose()
                con.Close()
                Exit Sub
            End If
        ElseIf Me.Tag = "Edit" Then
            Button_firstItem.Enabled = False
            Button_nextItem.Enabled = False
            Button_lastItem.Enabled = False
            Button_backItem.Enabled = False
            query = "SELECT * FROM raport WHERE nis = '" & Label_nis.Text & "'"

            con.Open()
            cmd = New MySqlCommand(query, con)
            dr = cmd.ExecuteReader()

            If dr.HasRows Then
                dr.Close()
                cmd.Dispose()
                con.Close()

                retRepData(nextItem:=1, studentID:=Label_nis.Text)
                TextBox_pengPai.Text = k1
                TextBox_ketPai.Text = m1
                id(1) = i1
                retRepData(nextItem:=2, studentID:=Label_nis.Text)
                TextBox_pengPkn.Text = k1
                TextBox_ketPkn.Text = m1
                id(2) = i1
                retRepData(nextItem:=3, studentID:=Label_nis.Text)
                TextBox_pengIndo.Text = k1
                TextBox_ketIndo.Text = m1
                id(3) = i1
                retRepData(nextItem:=4, studentID:=Label_nis.Text)
                TextBox_pengMtk.Text = k1
                TextBox_ketMtk.Text = m1
                id(4) = i1
                retRepData(nextItem:=5, studentID:=Label_nis.Text)
                TextBox_pengSej.Text = k1
                TextBox_ketSej.Text = m1
                id(5) = i1
                retRepData(nextItem:=6, studentID:=Label_nis.Text)
                TextBox_pengIng.Text = k1
                TextBox_ketIng.Text = m1
                id(6) = i1
                retRepData(nextItem:=7, studentID:=Label_nis.Text)
                TextBox_pengSbk.Text = k1
                TextBox_ketSbk.Text = m1
                id(7) = i1
                retRepData(nextItem:=8, studentID:=Label_nis.Text)
                TextBox_pengPen.Text = k1
                TextBox_ketPen.Text = m1
                id(8) = i1
                retRepData(nextItem:=9, studentID:=Label_nis.Text)
                TextBox_pengKwh.Text = k1
                TextBox_ketKwh.Text = m1
                id(9) = i1
                retRepData(nextItem:=10, studentID:=Label_nis.Text)
                TextBox_pengMul.Text = k1
                TextBox_ketMul.Text = m1
                id(10) = i1
                retRepData(nextItem:=24, studentID:=Label_nis.Text)
                TextBox_pengProd1.Text = k1
                TextBox_ketProd1.Text = m1
                id(11) = i1
                retRepData(nextItem:=25, studentID:=Label_nis.Text)
                TextBox_pengProd2.Text = k1
                TextBox_ketProd2.Text = m1
                id(12) = i1
                retRepData(nextItem:=26, studentID:=Label_nis.Text)
                TextBox_pengProd3.Text = k1
                TextBox_ketProd3.Text = m1
                id(13) = i1
                retRepData(nextItem:=27, studentID:=Label_nis.Text)
                TextBox_pengProd4.Text = k1
                TextBox_ketProd4.Text = m1
                id(14) = i1

                Label_nisSis.Text = a1
                Label_namaSiswa.Text = UCase(b1)
                Label_kelas.Text = UCase(c2 & " " & d2 & " " & f1)
                Label_nis.Text = a1
                Label_jur.Text = UCase(d1)
                TextBox_searchNis.Text = a1

                TextBox_sakit.Text = o1
                TextBox_izin.Text = p1
                TextBox_alpha.Text = q1
                MetroComboBox_ekskul.Text = r1
                TextBox_komen.Text = s1

                Button_firstItem.Enabled = True
                Button_nextItem.Enabled = True
                Button_lastItem.Enabled = True
                Button_backItem.Enabled = True
            Else
                Button_firstItem.Enabled = True
                Button_nextItem.Enabled = True
                Button_lastItem.Enabled = True
                Button_backItem.Enabled = True
                Label_nis.Text = a1
                dr.Close()
                cmd.Dispose()
                con.Close()
                Exit Sub
            End If
        End If
    End Sub

    Public Sub retData()
        query = "SELECT master_siswa.nis, master_siswa.nisn, master_siswa.nama_siswa, kelas.kelas, kelas.rom, jurusan.jurusan, jurusan.jurusan_sngkt, lokal.lokal, master_siswa.nip, master_guru.nama
FROM master_siswa
INNER JOIN kelas ON kelas.id_kelas = master_siswa.id_kelas
INNER JOIN jurusan ON jurusan.id_jurusan = master_siswa.id_jurusan
INNER JOIN lokal ON lokal.id_lokal = master_siswa.id_lokal
INNER JOIN master_guru ON master_siswa.nip = master_guru.nip
INNER JOIN wali_kelas ON master_siswa.nip = wali_kelas.nip
WHERE wali_kelas.nip = '" & wal & "'
ORDER BY master_siswa.nis ASC
LIMIT 1"
        con.Open()
        cmd = New MySqlCommand(query, con)
        dr = cmd.ExecuteReader()

        Do While dr.Read()
            a1 = (dr.Item("nis").ToString())
            g1 = (dr.Item("nisn").ToString())
            b1 = (dr.Item("nama_siswa").ToString())
            c1 = (dr.Item("kelas").ToString())
            c2 = (dr.Item("rom").ToString())
            d1 = (dr.Item("jurusan").ToString())
            d2 = (dr.Item("jurusan_sngkt").ToString())
            f1 = (dr.Item("lokal").ToString())
        Loop
        Label_nisSis.Text = a1
        Label_namaSiswa.Text = UCase(b1)
        Label_kelas.Text = UCase(c2 & " " & d2 & " " & f1)
        Label_nis.Text = a1
        Label_jur.Text = UCase(d1)
        TextBox_searchNis.Text = a1
        dr.Close()
        cmd.Dispose()
        con.Close()
    End Sub

    Private Sub addGrade_form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Label_smes.Text = 0
        retData()
        Label_kar.Text = TextBox_komen.MaxLength

        If Me.Tag = "Add" Then
            getId_mapel(nextRow:=0)
            id(0) = id_mapel
            getId_mapel(nextRow:=1)
            id(1) = id_mapel
            getId_mapel(nextRow:=2)
            id(2) = id_mapel
            getId_mapel(nextRow:=3)
            id(3) = id_mapel
            getId_mapel(nextRow:=4)
            id(4) = id_mapel
            getId_mapel(nextRow:=5)
            id(5) = id_mapel
            getId_mapel(nextRow:=6)
            id(6) = id_mapel
            getId_mapel(nextRow:=7)
            id(7) = id_mapel
            getId_mapel(nextRow:=8)
            id(8) = id_mapel
            getId_mapel(nextRow:=9)
            id(9) = id_mapel
            getId_mapel(nextRow:=10)
            id(10) = id_mapel
            getId_mapel(nextRow:=24)
            id(11) = id_mapel
            getId_mapel(nextRow:=25)
            id(12) = id_mapel
            getId_mapel(nextRow:=26)
            id(13) = id_mapel
            getId_mapel(nextRow:=27)
            id(14) = id_mapel
            getId_mapel(nextRow:=28)
            id(15) = id_mapel
            form_container.Label_title.Text = "Input Nilai - HEXA Report"
            form_container.Text = "Input Nilai - HEXA Report"
        ElseIf Me.Tag = "Edit" Then
            retRepData(nextItem:=1, studentID:=a1)
            TextBox_pengPai.Text = k1
            TextBox_ketPai.Text = m1
            id(1) = i1
            retRepData(nextItem:=2, studentID:=a1)
            TextBox_pengPkn.Text = k1
            TextBox_ketPkn.Text = m1
            id(2) = i1
            retRepData(nextItem:=3, studentID:=a1)
            TextBox_pengIndo.Text = k1
            TextBox_ketIndo.Text = m1
            id(3) = i1
            retRepData(nextItem:=4, studentID:=a1)
            TextBox_pengMtk.Text = k1
            TextBox_ketMtk.Text = m1
            id(4) = i1
            retRepData(nextItem:=5, studentID:=a1)
            TextBox_pengSej.Text = k1
            TextBox_ketSej.Text = m1
            id(5) = i1
            retRepData(nextItem:=6, studentID:=a1)
            TextBox_pengIng.Text = k1
            TextBox_ketIng.Text = m1
            id(6) = i1
            retRepData(nextItem:=7, studentID:=a1)
            TextBox_pengSbk.Text = k1
            TextBox_ketSbk.Text = m1
            id(7) = i1
            retRepData(nextItem:=8, studentID:=a1)
            TextBox_pengPen.Text = k1
            TextBox_ketPen.Text = m1
            id(8) = i1
            retRepData(nextItem:=9, studentID:=a1)
            TextBox_pengKwh.Text = k1
            TextBox_ketKwh.Text = m1
            id(9) = i1
            retRepData(nextItem:=10, studentID:=a1)
            TextBox_pengMul.Text = k1
            TextBox_ketMul.Text = m1
            id(10) = i1
            retRepData(nextItem:=24, studentID:=a1)
            TextBox_pengProd1.Text = k1
            TextBox_ketProd1.Text = m1
            id(11) = i1
            retRepData(nextItem:=25, studentID:=a1)
            TextBox_pengProd2.Text = k1
            TextBox_ketProd2.Text = m1
            id(12) = i1
            retRepData(nextItem:=26, studentID:=a1)
            TextBox_pengProd3.Text = k1
            TextBox_ketProd3.Text = m1
            id(13) = i1
            retRepData(nextItem:=27, studentID:=a1)
            TextBox_pengProd4.Text = k1
            TextBox_ketProd4.Text = m1
            id(14) = i1
            retRepData(nextItem:=28, studentID:=a1)
            TextBox_pengProd5.Text = k1
            TextBox_ketProd5.Text = m1
            id(15) = i1
            retRepData(nextItem:=29, studentID:=a1)
            TextBox_pengProd6.Text = k1
            TextBox_ketProd6.Text = m1
            id(16) = i1

            Label_smes.Text = j1
            MetroComboBox_smes.Text = j1
            TextBox_sakit.Text = o1
            TextBox_izin.Text = p1
            TextBox_alpha.Text = q1
            MetroComboBox_ekskul.Text = r1
            TextBox_komen.Text = s1
            form_container.Label_title.Text = "Edit Nilai - HEXA Report"
            form_container.Text = "Edit Nilai - HEXA Report"
        End If

        Me.Dock = DockStyle.Fill
        Label_thnPel.Text = Format(Now.Year).ToString() & "/" & Format(Now.Year + 1).ToString()
    End Sub

    Private Sub TextBox_pengPai_TextChanged(sender As Object, e As EventArgs) Handles TextBox_pengPai.TextChanged
        If TextBox_pengPai.Text.Trim.Length = 0 Then
            Label_hurPengPai.Text = ""
        Else
            Select Case Val(TextBox_pengPai.Text)
                Case 0 To 49
                    Label_hurPengPai.Text = "E"
                Case 50 To 59
                    Label_hurPengPai.Text = "D"
                Case 60 To 74
                    Label_hurPengPai.Text = "C"
                Case 75 To 84
                    Label_hurPengPai.Text = "B"
                Case 85 To 100
                    Label_hurPengPai.Text = "A"
            End Select
        End If
    End Sub

    Private Sub TextBox_ketPai_TextChanged(sender As Object, e As EventArgs) Handles TextBox_ketPai.TextChanged
        If TextBox_ketPai.Text.Trim.Length = 0 Then
            Label_hurKetPai.Text = ""
        Else
            Select Case Val(TextBox_ketPai.Text)
                Case 0 To 49
                    Label_hurKetPai.Text = "E"
                Case 50 To 59
                    Label_hurKetPai.Text = "D"
                Case 60 To 74
                    Label_hurKetPai.Text = "C"
                Case 75 To 84
                    Label_hurKetPai.Text = "B"
                Case 85 To 100
                    Label_hurKetPai.Text = "A"
            End Select
        End If
    End Sub

    Private Sub TextBox_pengPkn_TextChanged(sender As Object, e As EventArgs) Handles TextBox_pengPkn.TextChanged
        If TextBox_pengPkn.Text.Trim.Length = 0 Then
            Label_hurPengPkn.Text = ""
        Else
            Select Case Val(TextBox_pengPkn.Text)
                Case 0 To 49
                    Label_hurPengPkn.Text = "E"
                Case 50 To 59
                    Label_hurPengPkn.Text = "D"
                Case 60 To 74
                    Label_hurPengPkn.Text = "C"
                Case 75 To 84
                    Label_hurPengPkn.Text = "B"
                Case 85 To 100
                    Label_hurPengPkn.Text = "A"
            End Select
        End If
    End Sub

    Private Sub TextBox_ketPkn_TextChanged(sender As Object, e As EventArgs) Handles TextBox_ketPkn.TextChanged
        If TextBox_ketPkn.Text.Trim.Length = 0 Then
            Label_hurKetPkn.Text = ""
        Else
            Select Case Val(TextBox_ketPkn.Text)
                Case 0 To 49
                    Label_hurKetPkn.Text = "E"
                Case 50 To 59
                    Label_hurKetPkn.Text = "D"
                Case 60 To 74
                    Label_hurKetPkn.Text = "C"
                Case 75 To 84
                    Label_hurKetPkn.Text = "B"
                Case 85 To 100
                    Label_hurKetPkn.Text = "A"
            End Select
        End If
    End Sub

    Private Sub TextBox_pengIndo_TextChanged(sender As Object, e As EventArgs) Handles TextBox_pengIndo.TextChanged
        If TextBox_pengIndo.Text.Trim.Length = 0 Then
            Label_hurPengIndo.Text = ""
        Else
            Select Case Val(TextBox_pengIndo.Text)
                Case 0 To 49
                    Label_hurPengIndo.Text = "E"
                Case 50 To 59
                    Label_hurPengIndo.Text = "D"
                Case 60 To 74
                    Label_hurPengIndo.Text = "C"
                Case 75 To 84
                    Label_hurPengIndo.Text = "B"
                Case 85 To 100
                    Label_hurPengIndo.Text = "A"
            End Select
        End If
    End Sub

    Private Sub TextBox_ketIndo_TextChanged(sender As Object, e As EventArgs) Handles TextBox_ketIndo.TextChanged
        If TextBox_ketIndo.Text.Trim.Length = 0 Then
            Label_hurKetIndo.Text = ""
        Else
            Select Case Val(TextBox_ketIndo.Text)
                Case 0 To 49
                    Label_hurKetIndo.Text = "E"
                Case 50 To 59
                    Label_hurKetIndo.Text = "D"
                Case 60 To 74
                    Label_hurKetIndo.Text = "C"
                Case 75 To 84
                    Label_hurKetIndo.Text = "B"
                Case 85 To 100
                    Label_hurKetIndo.Text = "A"
            End Select
        End If
    End Sub

    Private Sub TextBox_pengMtk_TextChanged(sender As Object, e As EventArgs) Handles TextBox_pengMtk.TextChanged
        If TextBox_pengMtk.Text.Trim.Length = 0 Then
            Label_hurPengMtk.Text = ""
        Else
            Select Case Val(TextBox_pengMtk.Text)
                Case 0 To 49
                    Label_hurPengMtk.Text = "E"
                Case 50 To 59
                    Label_hurPengMtk.Text = "D"
                Case 60 To 74
                    Label_hurPengMtk.Text = "C"
                Case 75 To 84
                    Label_hurPengMtk.Text = "B"
                Case 85 To 100
                    Label_hurPengMtk.Text = "A"
            End Select
        End If
    End Sub

    Private Sub TextBox_ketMtk_TextChanged(sender As Object, e As EventArgs) Handles TextBox_ketMtk.TextChanged
        If TextBox_ketMtk.Text.Trim.Length = 0 Then
            Label_hurKetMtk.Text = ""
        Else
            Select Case Val(TextBox_ketMtk.Text)
                Case 0 To 49
                    Label_hurKetMtk.Text = "E"
                Case 50 To 59
                    Label_hurKetMtk.Text = "D"
                Case 60 To 74
                    Label_hurKetMtk.Text = "C"
                Case 75 To 84
                    Label_hurKetMtk.Text = "B"
                Case 85 To 100
                    Label_hurKetMtk.Text = "A"
            End Select
        End If
    End Sub

    Private Sub TextBox_pengSej_TextChanged(sender As Object, e As EventArgs) Handles TextBox_pengSej.TextChanged
        If TextBox_pengSej.Text.Trim.Length = 0 Then
            Label_hurPengSej.Text = ""
        Else
            Select Case Val(TextBox_pengSej.Text)
                Case 0 To 49
                    Label_hurPengSej.Text = "E"
                Case 50 To 59
                    Label_hurPengSej.Text = "D"
                Case 60 To 74
                    Label_hurPengSej.Text = "C"
                Case 75 To 84
                    Label_hurPengSej.Text = "B"
                Case 85 To 100
                    Label_hurPengSej.Text = "A"
            End Select
        End If
    End Sub

    Private Sub TextBox_ketSej_TextChanged(sender As Object, e As EventArgs) Handles TextBox_ketSej.TextChanged
        If TextBox_ketSej.Text.Trim.Length = 0 Then
            Label_hurKetSej.Text = ""
        Else
            Select Case Val(TextBox_ketSej.Text)
                Case 0 To 49
                    Label_hurKetSej.Text = "E"
                Case 50 To 59
                    Label_hurKetSej.Text = "D"
                Case 60 To 74
                    Label_hurKetSej.Text = "C"
                Case 75 To 84
                    Label_hurKetSej.Text = "B"
                Case 85 To 100
                    Label_hurKetSej.Text = "A"
            End Select
        End If
    End Sub

    Private Sub TextBox_pengIng_TextChanged(sender As Object, e As EventArgs) Handles TextBox_pengIng.TextChanged
        If TextBox_pengIng.Text.Trim.Length = 0 Then
            Label_hurPengIng.Text = ""
        Else
            Select Case Val(TextBox_pengIng.Text)
                Case 0 To 49
                    Label_hurPengIng.Text = "E"
                Case 50 To 59
                    Label_hurPengIng.Text = "D"
                Case 60 To 74
                    Label_hurPengIng.Text = "C"
                Case 75 To 84
                    Label_hurPengIng.Text = "B"
                Case 85 To 100
                    Label_hurPengIng.Text = "A"
            End Select
        End If
    End Sub

    Private Sub TextBox_ketIng_TextChanged(sender As Object, e As EventArgs) Handles TextBox_ketIng.TextChanged
        If TextBox_ketIng.Text.Trim.Length = 0 Then
            Label_hurKetIng.Text = ""
        Else
            Select Case Val(TextBox_ketIng.Text)
                Case 0 To 49
                    Label_hurKetIng.Text = "E"
                Case 50 To 59
                    Label_hurKetIng.Text = "D"
                Case 60 To 74
                    Label_hurKetIng.Text = "C"
                Case 75 To 84
                    Label_hurKetIng.Text = "B"
                Case 85 To 100
                    Label_hurKetIng.Text = "A"
            End Select
        End If
    End Sub

    Private Sub TextBox_pengSbk_TextChanged(sender As Object, e As EventArgs) Handles TextBox_pengSbk.TextChanged
        If TextBox_pengSbk.Text.Trim.Length = 0 Then
            Label_hurPengSbk.Text = ""
        Else
            Select Case Val(TextBox_pengSbk.Text)
                Case 0 To 49
                    Label_hurPengSbk.Text = "E"
                Case 50 To 59
                    Label_hurPengSbk.Text = "D"
                Case 60 To 74
                    Label_hurPengSbk.Text = "C"
                Case 75 To 84
                    Label_hurPengSbk.Text = "B"
                Case 85 To 100
                    Label_hurPengSbk.Text = "A"
            End Select
        End If
    End Sub

    Private Sub TextBox_ketSbk_TextChanged(sender As Object, e As EventArgs) Handles TextBox_ketSbk.TextChanged
        If TextBox_ketSbk.Text.Trim.Length = 0 Then
            Label_hurKetSbk.Text = ""
        Else
            Select Case Val(TextBox_ketSbk.Text)
                Case 0 To 49
                    Label_hurKetSbk.Text = "E"
                Case 50 To 59
                    Label_hurKetSbk.Text = "D"
                Case 60 To 74
                    Label_hurKetSbk.Text = "C"
                Case 75 To 84
                    Label_hurKetSbk.Text = "B"
                Case 85 To 100
                    Label_hurKetSbk.Text = "A"
            End Select
        End If
    End Sub

    Private Sub TextBox_pengPen_TextChanged(sender As Object, e As EventArgs) Handles TextBox_pengPen.TextChanged
        If TextBox_pengPen.Text.Trim.Length = 0 Then
            Label_hurPengPen.Text = ""
        Else
            Select Case Val(TextBox_pengPen.Text)
                Case 0 To 49
                    Label_hurPengPen.Text = "E"
                Case 50 To 59
                    Label_hurPengPen.Text = "D"
                Case 60 To 74
                    Label_hurPengPen.Text = "C"
                Case 75 To 84
                    Label_hurPengPen.Text = "B"
                Case 85 To 100
                    Label_hurPengPen.Text = "A"
            End Select
        End If
    End Sub

    Private Sub TextBox_ketPen_TextChanged(sender As Object, e As EventArgs) Handles TextBox_ketPen.TextChanged
        If TextBox_ketPen.Text.Trim.Length = 0 Then
            Label_hurKetPen.Text = ""
        Else
            Select Case Val(TextBox_ketPen.Text)
                Case 0 To 49
                    Label_hurKetPen.Text = "E"
                Case 50 To 59
                    Label_hurKetPen.Text = "D"
                Case 60 To 74
                    Label_hurKetPen.Text = "C"
                Case 75 To 84
                    Label_hurKetPen.Text = "B"
                Case 85 To 100
                    Label_hurKetPen.Text = "A"
            End Select
        End If
    End Sub

    Private Sub TextBox_pengKwh_TextChanged(sender As Object, e As EventArgs) Handles TextBox_pengKwh.TextChanged
        If TextBox_pengKwh.Text.Trim.Length = 0 Then
            Label_hurPengKwh.Text = ""
        Else
            Select Case Val(TextBox_pengKwh.Text)
                Case 0 To 49
                    Label_hurPengKwh.Text = "E"
                Case 50 To 59
                    Label_hurPengKwh.Text = "D"
                Case 60 To 74
                    Label_hurPengKwh.Text = "C"
                Case 75 To 84
                    Label_hurPengKwh.Text = "B"
                Case 85 To 100
                    Label_hurPengKwh.Text = "A"
            End Select
        End If
    End Sub

    Private Sub TextBox_ketKwh_TextChanged(sender As Object, e As EventArgs) Handles TextBox_ketKwh.TextChanged
        If TextBox_ketKwh.Text.Trim.Length = 0 Then
            Label_hurKetKwh.Text = ""
        Else
            Select Case Val(TextBox_ketKwh.Text)
                Case 0 To 49
                    Label_hurKetKwh.Text = "E"
                Case 50 To 59
                    Label_hurKetKwh.Text = "D"
                Case 60 To 74
                    Label_hurKetKwh.Text = "C"
                Case 75 To 84
                    Label_hurKetKwh.Text = "B"
                Case 85 To 100
                    Label_hurKetKwh.Text = "A"
            End Select
        End If
    End Sub

    Private Sub TextBox_pengMul_TextChanged(sender As Object, e As EventArgs) Handles TextBox_pengMul.TextChanged
        If TextBox_pengMul.Text.Trim.Length = 0 Then
            Label_hurPengMul.Text = ""
        Else
            Select Case Val(TextBox_pengMul.Text)
                Case 0 To 49
                    Label_hurPengMul.Text = "E"
                Case 50 To 59
                    Label_hurPengMul.Text = "D"
                Case 60 To 74
                    Label_hurPengMul.Text = "C"
                Case 75 To 84
                    Label_hurPengMul.Text = "B"
                Case 85 To 100
                    Label_hurPengMul.Text = "A"
            End Select
        End If
    End Sub

    Private Sub TextBox_ketMul_TextChanged(sender As Object, e As EventArgs) Handles TextBox_ketMul.TextChanged
        If TextBox_ketMul.Text.Trim.Length = 0 Then
            Label_hurKetMul.Text = ""
        Else
            Select Case Val(TextBox_ketMul.Text)
                Case 0 To 49
                    Label_hurKetMul.Text = "E"
                Case 50 To 59
                    Label_hurKetMul.Text = "D"
                Case 60 To 74
                    Label_hurKetMul.Text = "C"
                Case 75 To 84
                    Label_hurKetMul.Text = "B"
                Case 85 To 100
                    Label_hurKetMul.Text = "A"
            End Select
        End If
    End Sub

    Private Sub Button_lastItem_Click(sender As Object, e As EventArgs) Handles Button_lastItem.Click
        If Me.Tag = "Add" Then
            Button_firstItem.Enabled = False
            Button_nextItem.Enabled = False
            Button_lastItem.Enabled = False
            Button_backItem.Enabled = False
            query = "SELECT master_siswa.nis, master_siswa.nisn, master_siswa.nama_siswa, kelas.kelas, kelas.rom, jurusan.jurusan, jurusan.jurusan_sngkt, lokal.lokal, master_siswa.nip, master_guru.nama
FROM master_siswa
INNER JOIN kelas ON kelas.id_kelas = master_siswa.id_kelas
INNER JOIN jurusan ON jurusan.id_jurusan = master_siswa.id_jurusan
INNER JOIN lokal ON lokal.id_lokal = master_siswa.id_lokal
INNER JOIN master_guru ON master_siswa.nip = master_guru.nip
INNER JOIN wali_kelas ON master_siswa.nip = wali_kelas.nip
WHERE wali_kelas.nip = '" & wal & "'
ORDER BY master_siswa.nis DESC
LIMIT 1"
            con.Open()
            cmd = New MySqlCommand(query, con)
            dr = cmd.ExecuteReader()

            If dr.HasRows() Then
                Do While dr.Read()
                    a1 = (dr.Item("nis").ToString())
                    g1 = (dr.Item("nisn").ToString())
                    b1 = (dr.Item("nama_siswa").ToString())
                    c1 = (dr.Item("kelas").ToString())
                    c2 = (dr.Item("rom").ToString())
                    d1 = (dr.Item("jurusan").ToString())
                    d2 = (dr.Item("jurusan_sngkt").ToString())
                    f1 = (dr.Item("lokal").ToString())
                Loop
                Label_nisSis.Text = a1
                Label_namaSiswa.Text = UCase(b1)
                Label_kelas.Text = UCase(c2 & " " & d2 & " " & f1)
                Label_nis.Text = a1
                Label_jur.Text = UCase(d1)
                TextBox_searchNis.Text = a1
                dr.Close()
                cmd.Dispose()
                con.Close()

                Button_firstItem.Enabled = True
                Button_nextItem.Enabled = True
                Button_lastItem.Enabled = True
                Button_backItem.Enabled = True
            Else
                Button_firstItem.Enabled = True
                Button_nextItem.Enabled = True
                Button_lastItem.Enabled = True
                Button_backItem.Enabled = True
                Label_nis.Text = a1
                dr.Close()
                cmd.Dispose()
                con.Close()
                Exit Sub
            End If
        ElseIf Me.Tag = "Edit" Then
            Button_firstItem.Enabled = False
            Button_nextItem.Enabled = False
            Button_lastItem.Enabled = False
            Button_backItem.Enabled = False
            query = "SELECT raport.nis, master_siswa.nisn, master_siswa.nama_siswa, kelas.kelas, kelas.rom, jurusan.jurusan, jurusan.jurusan_sngkt, lokal.lokal, master_siswa.nip, master_guru.nama
FROM raport
INNER JOIN master_siswa ON master_siswa.nis = raport.nis
INNER JOIN kelas ON kelas.id_kelas = master_siswa.id_kelas
INNER JOIN jurusan ON jurusan.id_jurusan = master_siswa.id_jurusan
INNER JOIN lokal ON lokal.id_lokal = master_siswa.id_lokal
INNER JOIN master_guru ON master_siswa.nip = master_guru.nip
INNER JOIN wali_kelas ON master_siswa.nip = wali_kelas.nip
WHERE wali_kelas.nip = '" & wal & "'
ORDER BY master_siswa.nis DESC
LIMIT 1"
            con.Open()
            cmd = New MySqlCommand(query, con)
            dr = cmd.ExecuteReader()

            If dr.HasRows() Then
                Do While dr.Read()
                    a1 = (dr.Item("nis").ToString())
                    g1 = (dr.Item("nisn").ToString())
                    b1 = (dr.Item("nama_siswa").ToString())
                    c1 = (dr.Item("kelas").ToString())
                    c2 = (dr.Item("rom").ToString())
                    d1 = (dr.Item("jurusan").ToString())
                    d2 = (dr.Item("jurusan_sngkt").ToString)
                    f1 = (dr.Item("lokal").ToString())
                Loop
                Label_nisSis.Text = a1
                Label_namaSiswa.Text = UCase(b1)
                Label_kelas.Text = UCase(c2 & " " & d2 & " " & f1)
                Label_nis.Text = a1
                Label_jur.Text = UCase(d1)
                TextBox_searchNis.Text = a1
                dr.Close()
                cmd.Dispose()
                con.Close()

                retRepData(nextItem:=1, studentID:=a1)
                TextBox_pengPai.Text = k1
                TextBox_ketPai.Text = m1
                id(1) = i1
                retRepData(nextItem:=2, studentID:=a1)
                TextBox_pengPkn.Text = k1
                TextBox_ketPkn.Text = m1
                id(2) = i1
                retRepData(nextItem:=3, studentID:=a1)
                TextBox_pengIndo.Text = k1
                TextBox_ketIndo.Text = m1
                id(3) = i1
                retRepData(nextItem:=4, studentID:=a1)
                TextBox_pengMtk.Text = k1
                TextBox_ketMtk.Text = m1
                id(4) = i1
                retRepData(nextItem:=5, studentID:=a1)
                TextBox_pengSej.Text = k1
                TextBox_ketSej.Text = m1
                id(5) = i1
                retRepData(nextItem:=6, studentID:=a1)
                TextBox_pengIng.Text = k1
                TextBox_ketIng.Text = m1
                id(6) = i1
                retRepData(nextItem:=7, studentID:=a1)
                TextBox_pengSbk.Text = k1
                TextBox_ketSbk.Text = m1
                id(7) = i1
                retRepData(nextItem:=8, studentID:=a1)
                TextBox_pengPen.Text = k1
                TextBox_ketPen.Text = m1
                id(8) = i1
                retRepData(nextItem:=9, studentID:=a1)
                TextBox_pengKwh.Text = k1
                TextBox_ketKwh.Text = m1
                id(9) = i1
                retRepData(nextItem:=10, studentID:=a1)
                TextBox_pengMul.Text = k1
                TextBox_ketMul.Text = m1
                id(10) = i1
                retRepData(nextItem:=24, studentID:=a1)
                TextBox_pengProd1.Text = k1
                TextBox_ketProd1.Text = m1
                id(11) = i1
                retRepData(nextItem:=25, studentID:=a1)
                TextBox_pengProd2.Text = k1
                TextBox_ketProd2.Text = m1
                id(12) = i1
                retRepData(nextItem:=26, studentID:=a1)
                TextBox_pengProd3.Text = k1
                TextBox_ketProd3.Text = m1
                id(13) = i1
                retRepData(nextItem:=27, studentID:=a1)
                TextBox_pengProd4.Text = k1
                TextBox_ketProd4.Text = m1
                id(14) = i1

                TextBox_sakit.Text = o1
                TextBox_izin.Text = p1
                TextBox_alpha.Text = q1
                MetroComboBox_ekskul.Text = r1
                TextBox_komen.Text = s1

                Button_firstItem.Enabled = True
                Button_nextItem.Enabled = True
                Button_lastItem.Enabled = True
                Button_backItem.Enabled = True
            Else
                Button_firstItem.Enabled = True
                Button_nextItem.Enabled = True
                Button_lastItem.Enabled = True
                Button_backItem.Enabled = True
                Label_nis.Text = a1
                dr.Close()
                cmd.Dispose()
                con.Close()
                Exit Sub
            End If
        End If
    End Sub

    Private Sub TextBox_pengProd1_TextChanged(sender As Object, e As EventArgs) Handles TextBox_pengProd1.TextChanged
        If TextBox_pengProd1.Text.Trim.Length = 0 Then
            Label_hurPengProd1.Text = ""
        Else
            Select Case Val(TextBox_pengProd1.Text)
                Case 0 To 49
                    Label_hurPengProd1.Text = "E"
                Case 50 To 59
                    Label_hurPengProd1.Text = "D"
                Case 60 To 74
                    Label_hurPengProd1.Text = "C"
                Case 75 To 84
                    Label_hurPengProd1.Text = "B"
                Case 85 To 100
                    Label_hurPengProd1.Text = "A"
            End Select
        End If
    End Sub

    Private Sub TextBox_ketProd1_TextChanged(sender As Object, e As EventArgs) Handles TextBox_ketProd1.TextChanged
        If TextBox_ketProd1.Text.Trim.Length = 0 Then
            Label_hurKetProd1.Text = ""
        Else
            Select Case Val(TextBox_ketProd1.Text)
                Case 0 To 49
                    Label_hurKetProd1.Text = "E"
                Case 50 To 59
                    Label_hurKetProd1.Text = "D"
                Case 60 To 74
                    Label_hurKetProd1.Text = "C"
                Case 75 To 84
                    Label_hurKetProd1.Text = "B"
                Case 85 To 100
                    Label_hurKetProd1.Text = "A"
            End Select
        End If
    End Sub

    Private Sub TextBox_pengProd2_TextChanged(sender As Object, e As EventArgs) Handles TextBox_pengProd2.TextChanged
        If TextBox_pengProd2.Text.Trim.Length = 0 Then
            Label_hurPengProd2.Text = ""
        Else
            Select Case Val(TextBox_pengProd2.Text)
                Case 0 To 49
                    Label_hurPengProd2.Text = "E"
                Case 50 To 59
                    Label_hurPengProd2.Text = "D"
                Case 60 To 74
                    Label_hurPengProd2.Text = "C"
                Case 75 To 84
                    Label_hurPengProd2.Text = "B"
                Case 85 To 100
                    Label_hurPengProd2.Text = "A"
            End Select
        End If
    End Sub

    Private Sub TextBox_ketProd2_TextChanged(sender As Object, e As EventArgs) Handles TextBox_ketProd2.TextChanged
        If TextBox_ketProd2.Text.Trim.Length = 0 Then
            Label_hurKetProd2.Text = ""
        Else
            Select Case Val(TextBox_ketProd2.Text)
                Case 0 To 49
                    Label_hurKetProd2.Text = "E"
                Case 50 To 59
                    Label_hurKetProd2.Text = "D"
                Case 60 To 74
                    Label_hurKetProd2.Text = "C"
                Case 75 To 84
                    Label_hurKetProd2.Text = "B"
                Case 85 To 100
                    Label_hurKetProd2.Text = "A"
            End Select
        End If
    End Sub

    Private Sub TextBox_pengProd3_TextChanged(sender As Object, e As EventArgs) Handles TextBox_pengProd3.TextChanged
        If TextBox_pengProd3.Text.Trim.Length = 0 Then
            Label_hurPengProd3.Text = ""
        Else
            Select Case Val(TextBox_pengProd3.Text)
                Case 0 To 49
                    Label_hurPengProd3.Text = "E"
                Case 50 To 59
                    Label_hurPengProd3.Text = "D"
                Case 60 To 74
                    Label_hurPengProd3.Text = "C"
                Case 75 To 84
                    Label_hurPengProd3.Text = "B"
                Case 85 To 100
                    Label_hurPengProd3.Text = "A"
            End Select
        End If
    End Sub

    Private Sub TextBox_ketProd3_TextChanged(sender As Object, e As EventArgs) Handles TextBox_ketProd3.TextChanged
        If TextBox_ketProd3.Text.Trim.Length = 0 Then
            Label_hurKetProd3.Text = ""
        Else
            Select Case Val(TextBox_ketProd3.Text)
                Case 0 To 49
                    Label_hurKetProd3.Text = "E"
                Case 50 To 59
                    Label_hurKetProd3.Text = "D"
                Case 60 To 74
                    Label_hurKetProd3.Text = "C"
                Case 75 To 84
                    Label_hurKetProd3.Text = "B"
                Case 85 To 100
                    Label_hurKetProd3.Text = "A"
            End Select
        End If
    End Sub

    Private Sub TextBox_pengProd4_TextChanged(sender As Object, e As EventArgs) Handles TextBox_pengProd4.TextChanged
        If TextBox_pengProd4.Text.Trim.Length = 0 Then
            Label_hurPengProd4.Text = ""
        Else
            Select Case Val(TextBox_pengProd4.Text)
                Case 0 To 49
                    Label_hurPengProd4.Text = "E"
                Case 50 To 59
                    Label_hurPengProd4.Text = "D"
                Case 60 To 74
                    Label_hurPengProd4.Text = "C"
                Case 75 To 84
                    Label_hurPengProd4.Text = "B"
                Case 85 To 100
                    Label_hurPengProd4.Text = "A"
            End Select
        End If
    End Sub

    Private Sub TextBox_ketProd4_TextChanged(sender As Object, e As EventArgs) Handles TextBox_ketProd4.TextChanged
        If TextBox_ketProd4.Text.Trim.Length = 0 Then
            Label_hurKetProd4.Text = ""
        Else
            Select Case Val(TextBox_ketProd4.Text)
                Case 0 To 49
                    Label_hurKetProd4.Text = "E"
                Case 50 To 59
                    Label_hurKetProd4.Text = "D"
                Case 60 To 74
                    Label_hurKetProd4.Text = "C"
                Case 75 To 84
                    Label_hurKetProd4.Text = "B"
                Case 85 To 100
                    Label_hurKetProd4.Text = "A"
            End Select
        End If
    End Sub

    Private Sub TextBox_pengProd5_TextChanged(sender As Object, e As EventArgs) Handles TextBox_pengProd5.TextChanged
        If TextBox_pengProd5.Text.Trim.Length = 0 Then
            Label_hurPengProd5.Text = ""
        Else
            Select Case Val(TextBox_pengProd5.Text)
                Case 0 To 49
                    Label_hurPengProd5.Text = "E"
                Case 50 To 59
                    Label_hurPengProd5.Text = "D"
                Case 60 To 74
                    Label_hurPengProd5.Text = "C"
                Case 75 To 84
                    Label_hurPengProd5.Text = "B"
                Case 85 To 100
                    Label_hurPengProd5.Text = "A"
            End Select
        End If
    End Sub

    Private Sub TextBox_ketProd5_TextChanged(sender As Object, e As EventArgs) Handles TextBox_ketProd5.TextChanged
        If TextBox_ketProd5.Text.Trim.Length = 0 Then
            Label_hurKetProd5.Text = ""
        Else
            Select Case Val(TextBox_ketProd5.Text)
                Case 0 To 49
                    Label_hurKetProd5.Text = "E"
                Case 50 To 59
                    Label_hurKetProd5.Text = "D"
                Case 60 To 74
                    Label_hurKetProd5.Text = "C"
                Case 75 To 84
                    Label_hurKetProd5.Text = "B"
                Case 85 To 100
                    Label_hurKetProd5.Text = "A"
            End Select
        End If
    End Sub

    Private Sub TextBox_pengProd6_TextChanged(sender As Object, e As EventArgs) Handles TextBox_pengProd6.TextChanged
        If TextBox_pengProd6.Text.Trim.Length = 0 Then
            Label_hurPengProd6.Text = ""
        Else
            Select Case Val(TextBox_pengProd6.Text)
                Case 0 To 49
                    Label_hurPengProd6.Text = "E"
                Case 50 To 59
                    Label_hurPengProd6.Text = "D"
                Case 60 To 74
                    Label_hurPengProd6.Text = "C"
                Case 75 To 84
                    Label_hurPengProd6.Text = "B"
                Case 85 To 100
                    Label_hurPengProd6.Text = "A"
            End Select
        End If
    End Sub

    Private Sub TextBox_ketProd6_TextChanged(sender As Object, e As EventArgs) Handles TextBox_ketProd6.TextChanged
        If TextBox_ketProd6.Text.Trim.Length = 0 Then
            Label_hurKetProd6.Text = ""
        Else
            Select Case Val(TextBox_ketProd6.Text)
                Case 0 To 49
                    Label_hurKetProd6.Text = "E"
                Case 50 To 59
                    Label_hurKetProd6.Text = "D"
                Case 60 To 74
                    Label_hurKetProd6.Text = "C"
                Case 75 To 84
                    Label_hurKetProd6.Text = "B"
                Case 85 To 100
                    Label_hurKetProd6.Text = "A"
            End Select
        End If
    End Sub

    Private Sub TextBox_pengFis_TextChanged(sender As Object, e As EventArgs) Handles TextBox_pengFis.TextChanged
        If TextBox_pengFis.Text.Trim.Length = 0 Then
            Label_hurPengFis.Text = ""
        Else
            Select Case Val(TextBox_pengFis.Text)
                Case 0 To 49
                    Label_hurPengFis.Text = "E"
                Case 50 To 59
                    Label_hurPengFis.Text = "D"
                Case 60 To 74
                    Label_hurPengFis.Text = "C"
                Case 75 To 84
                    Label_hurPengFis.Text = "B"
                Case 85 To 100
                    Label_hurPengFis.Text = "A"
            End Select
        End If
    End Sub

    Private Sub TextBox_ketFis_TextChanged(sender As Object, e As EventArgs) Handles TextBox_ketFis.TextChanged
        If TextBox_ketFis.Text.Trim.Length = 0 Then
            Label_hurKetFis.Text = ""
        Else
            Select Case Val(TextBox_ketFis.Text)
                Case 0 To 49
                    Label_hurKetFis.Text = "E"
                Case 50 To 59
                    Label_hurKetFis.Text = "D"
                Case 60 To 74
                    Label_hurKetFis.Text = "C"
                Case 75 To 84
                    Label_hurKetFis.Text = "B"
                Case 85 To 100
                    Label_hurKetFis.Text = "A"
            End Select
        End If
    End Sub

    Private Sub TextBox_pengKim_TextChanged(sender As Object, e As EventArgs) Handles TextBox_pengKim.TextChanged
        If TextBox_pengKim.Text.Trim.Length = 0 Then
            Label_hurPengKim.Text = ""
        Else
            Select Case Val(TextBox_pengKim.Text)
                Case 0 To 49
                    Label_hurPengKim.Text = "E"
                Case 50 To 59
                    Label_hurPengKim.Text = "D"
                Case 60 To 74
                    Label_hurPengKim.Text = "C"
                Case 75 To 84
                    Label_hurPengKim.Text = "B"
                Case 85 To 100
                    Label_hurPengKim.Text = "A"
            End Select
        End If
    End Sub

    Private Sub TextBox_ketKim_TextChanged(sender As Object, e As EventArgs) Handles TextBox_ketKim.TextChanged
        If TextBox_ketKim.Text.Trim.Length = 0 Then
            Label_hurKetKim.Text = ""
        Else
            Select Case Val(TextBox_ketKim.Text)
                Case 0 To 49
                    Label_hurKetKim.Text = "E"
                Case 50 To 59
                    Label_hurKetKim.Text = "D"
                Case 60 To 74
                    Label_hurKetKim.Text = "C"
                Case 75 To 84
                    Label_hurKetKim.Text = "B"
                Case 85 To 100
                    Label_hurKetKim.Text = "A"
            End Select
        End If
    End Sub

    Private Sub TextBox_komen_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox_komen.KeyPress
        If e.KeyChar = ControlChars.Back Then
            If Label_kar.Text = 150 Then
                Exit Sub
            Else
                Label_kar.Text = TextBox_komen.MaxLength - TextBox_komen.Text.Trim.Length + 1
            End If
        Else
            If Label_kar.Text <> 0 Then
                Label_kar.Text = Label_kar.Text - 1
            Else
                Exit Sub
            End If
        End If
    End Sub

    Private Sub TextBox_komen_TextChanged(sender As Object, e As EventArgs) Handles TextBox_komen.TextChanged
        If TextBox_komen.Text.Trim.Length = 0 Or TextBox_komen.Text.Length = 0 Then
            Label_kar.Text = TextBox_komen.MaxLength
        End If
    End Sub

    Private Sub Button_reset_Click(sender As Object, e As EventArgs) Handles Button_reset.Click
        For Each filledBoxes In Me.MetroTabControl1.TabPages(0).Controls.OfType(Of TextBox)()
            filledBoxes.Text = ""
        Next
        For Each filledBoxes In Me.MetroTabControl1.TabPages(1).Controls.OfType(Of TextBox)()
            filledBoxes.Text = ""
        Next
        For Each filledBoxes In Me.MetroTabControl1.TabPages(2).Controls.OfType(Of TextBox)()
            filledBoxes.Text = ""
        Next
    End Sub

    Private Sub Label_smes_Click(sender As Object, e As EventArgs) Handles Label_smes.Click
        MetroComboBox_smes.Visible = True
        Label_smes.Visible = False
    End Sub

    Private Sub ComboBox_smes_MouseLeave(sender As Object, e As EventArgs)
        Label_smes.Visible = True
        MetroComboBox_smes.Visible = False
    End Sub

    Private Sub Button_firstItem_Click(sender As Object, e As EventArgs) Handles Button_firstItem.Click
        If Me.Tag = "Add" Then
            Button_firstItem.Enabled = False
            Button_nextItem.Enabled = False
            Button_lastItem.Enabled = False
            Button_backItem.Enabled = False

            query = "SELECT master_siswa.nis, master_siswa.nisn, master_siswa.nama_siswa, kelas.kelas, kelas.rom, jurusan.jurusan, jurusan.jurusan_sngkt, lokal.lokal, master_siswa.nip, master_guru.nama
FROM master_siswa
INNER JOIN kelas ON kelas.id_kelas = master_siswa.id_kelas
INNER JOIN jurusan ON jurusan.id_jurusan = master_siswa.id_jurusan
INNER JOIN lokal ON lokal.id_lokal = master_siswa.id_lokal
INNER JOIN master_guru ON master_siswa.nip = master_guru.nip
INNER JOIN wali_kelas ON master_siswa.nip = wali_kelas.nip
WHERE wali_kelas.nip = '" & wal & "'
ORDER BY master_siswa.nis ASC
LIMIT 1"
            con.Open()
            cmd = New MySqlCommand(query, con)
            dr = cmd.ExecuteReader()

            If dr.HasRows() Then
                Do While dr.Read()
                    a1 = (dr.Item("nis").ToString())
                    g1 = (dr.Item("nisn").ToString())
                    b1 = (dr.Item("nama_siswa").ToString())
                    c1 = (dr.Item("kelas").ToString())
                    c2 = (dr.Item("rom").ToString())
                    d1 = (dr.Item("jurusan").ToString())
                    d2 = (dr.Item("jurusan_sngkt").ToString())
                    f1 = (dr.Item("lokal").ToString())
                Loop
                Label_nisSis.Text = a1
                Label_namaSiswa.Text = UCase(b1)
                Label_kelas.Text = UCase(c2 & " " & d2 & " " & f1)
                Label_nis.Text = a1
                Label_jur.Text = UCase(d1)
                TextBox_searchNis.Text = a1
                dr.Close()
                cmd.Dispose()
                con.Close()
                Button_firstItem.Enabled = True
                Button_nextItem.Enabled = True
                Button_lastItem.Enabled = True
                Button_backItem.Enabled = True
            Else
                Button_firstItem.Enabled = True
                Button_nextItem.Enabled = True
                Button_lastItem.Enabled = True
                Button_backItem.Enabled = True
                Label_nis.Text = a1
                dr.Close()
                cmd.Dispose()
                con.Close()
                Exit Sub
            End If
        ElseIf Me.Tag = "Edit" Then
            Button_firstItem.Enabled = False
            Button_nextItem.Enabled = False
            Button_lastItem.Enabled = False
            Button_backItem.Enabled = False
            query = "SELECT raport.nis, master_siswa.nisn, master_siswa.nama_siswa, kelas.kelas, kelas.rom, jurusan.jurusan, jurusan.jurusan_sngkt, lokal.lokal, master_siswa.nip, master_guru.nama
FROM raport
INNER JOIN master_siswa ON master_siswa.nis = raport.nis
INNER JOIN kelas ON kelas.id_kelas = master_siswa.id_kelas
INNER JOIN jurusan ON jurusan.id_jurusan = master_siswa.id_jurusan
INNER JOIN lokal ON lokal.id_lokal = master_siswa.id_lokal
INNER JOIN master_guru ON master_siswa.nip = master_guru.nip
INNER JOIN wali_kelas ON master_siswa.nip = wali_kelas.nip
WHERE wali_kelas.nip = '" & wal & "'
ORDER BY master_siswa.nis ASC
LIMIT 1"
            con.Open()
            cmd = New MySqlCommand(query, con)
            dr = cmd.ExecuteReader()

            If dr.HasRows() Then
                Do While dr.Read()
                    a1 = (dr.Item("nis").ToString())
                    g1 = (dr.Item("nisn").ToString())
                    b1 = (dr.Item("nama_siswa").ToString())
                    c1 = (dr.Item("kelas").ToString())
                    c2 = (dr.Item("rom").ToString())
                    d1 = (dr.Item("jurusan").ToString())
                    d2 = (dr.Item("jurusan_sngkt").ToString())
                    f1 = (dr.Item("lokal").ToString())
                Loop
                Label_nisSis.Text = a1
                Label_namaSiswa.Text = UCase(b1)
                Label_kelas.Text = UCase(c2 & " " & d2 & " " & f1)
                Label_nis.Text = a1
                Label_jur.Text = UCase(d1)
                TextBox_searchNis.Text = a1
                dr.Close()
                cmd.Dispose()
                con.Close()

                retRepData(nextItem:=1, studentID:=a1)
                TextBox_pengPai.Text = k1
                TextBox_ketPai.Text = m1
                id(1) = i1
                retRepData(nextItem:=2, studentID:=a1)
                TextBox_pengPkn.Text = k1
                TextBox_ketPkn.Text = m1
                id(2) = i1
                retRepData(nextItem:=3, studentID:=a1)
                TextBox_pengIndo.Text = k1
                TextBox_ketIndo.Text = m1
                id(3) = i1
                retRepData(nextItem:=4, studentID:=a1)
                TextBox_pengMtk.Text = k1
                TextBox_ketMtk.Text = m1
                id(4) = i1
                retRepData(nextItem:=5, studentID:=a1)
                TextBox_pengSej.Text = k1
                TextBox_ketSej.Text = m1
                id(5) = i1
                retRepData(nextItem:=6, studentID:=a1)
                TextBox_pengIng.Text = k1
                TextBox_ketIng.Text = m1
                id(6) = i1
                retRepData(nextItem:=7, studentID:=a1)
                TextBox_pengSbk.Text = k1
                TextBox_ketSbk.Text = m1
                id(7) = i1
                retRepData(nextItem:=8, studentID:=a1)
                TextBox_pengPen.Text = k1
                TextBox_ketPen.Text = m1
                id(8) = i1
                retRepData(nextItem:=9, studentID:=a1)
                TextBox_pengKwh.Text = k1
                TextBox_ketKwh.Text = m1
                id(9) = i1
                retRepData(nextItem:=10, studentID:=a1)
                TextBox_pengMul.Text = k1
                TextBox_ketMul.Text = m1
                id(10) = i1
                retRepData(nextItem:=24, studentID:=a1)
                TextBox_pengProd1.Text = k1
                TextBox_ketProd1.Text = m1
                id(11) = i1
                retRepData(nextItem:=25, studentID:=a1)
                TextBox_pengProd2.Text = k1
                TextBox_ketProd2.Text = m1
                id(12) = i1
                retRepData(nextItem:=26, studentID:=a1)
                TextBox_pengProd3.Text = k1
                TextBox_ketProd3.Text = m1
                id(13) = i1
                retRepData(nextItem:=27, studentID:=a1)
                TextBox_pengProd4.Text = k1
                TextBox_ketProd4.Text = m1
                id(14) = i1

                TextBox_sakit.Text = o1
                TextBox_izin.Text = p1
                TextBox_alpha.Text = q1
                MetroComboBox_ekskul.Text = r1
                TextBox_komen.Text = s1

                Button_firstItem.Enabled = True
                Button_nextItem.Enabled = True
                Button_lastItem.Enabled = True
                Button_backItem.Enabled = True
            Else
                Button_firstItem.Enabled = True
                Button_nextItem.Enabled = True
                Button_lastItem.Enabled = True
                Button_backItem.Enabled = True
                Label_nis.Text = a1
                dr.Close()
                cmd.Dispose()
                con.Close()
                Exit Sub
            End If
        End If
    End Sub

    Private Sub MetroComboBox_smes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles MetroComboBox_smes.SelectedIndexChanged
        Label_smes.Text = MetroComboBox_smes.SelectedItem
        Label_smes.Visible = True
        MetroComboBox_smes.Visible = False
    End Sub
End Class
