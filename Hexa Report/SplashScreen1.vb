﻿Public NotInheritable Class SplashScreen1

    'TODO: This form can easily be set as the splash screen for the application by going to the "Application" tab
    '  of the Project Designer ("Properties" under the "Project" menu).
    Dim x, y As Integer
    Dim NewLocation As New System.Drawing.Point

    Private Sub SplashScreen1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ''Set up the dialog text at runtime according to the application's assembly information.  

        ''TODO: Customize the application's assembly information in the "Application" pane of the project 
        ''  properties dialog (under the "Project" menu).

        ''Application title
        'If My.Application.Info.Title <> "" Then
        '    ApplicationTitle.Text = My.Application.Info.Title
        'Else
        '    'If the application title is missing, use the application name, without the extension
        '    ApplicationTitle.Text = System.IO.Path.GetFileNameWithoutExtension(My.Application.Info.AssemblyName)
        'End If

        ''Format the version information using the text set into the Version control at design time as the
        ''  formatting string.  This allows for effective localization if desired.
        ''  Build and revision information could be included by using the following code and changing the 
        ''  Version control's designtime text to "Version {0}.{1:00}.{2}.{3}" or something similar.  See
        ''  String.Format() in Help for more information.
        ''
        ''    Version.Text = System.String.Format(Version.Text, My.Application.Info.Version.Major, My.Application.Info.Version.Minor, My.Application.Info.Version.Build, My.Application.Info.Version.Revision)

        'Version.Text = System.String.Format(Version.Text, My.Application.Info.Version.Major, My.Application.Info.Version.Minor)

        ''Copyright info
        'Copyright.Text = My.Application.Info.Copyright
        Me.BringToFront()
        Timer_startAnim.Start()
    End Sub

    Private Sub Timer_startAnim_Tick(sender As Object, e As EventArgs) Handles Timer_startAnim.Tick
        Me.Opacity = Me.Opacity + 0.1
        If Me.Opacity = 100 Then
            Timer_startAnim.Stop()
        End If
        Timer_endAnim.Start()
    End Sub

    Private Sub Timer_endAnim_Tick(sender As Object, e As EventArgs) Handles Timer_endAnim.Tick
        form_container.Show()
        Me.Close()
    End Sub

    'Private Sub SplashScreen1_MouseMove(sender As Object, e As MouseEventArgs) Handles MyBase.MouseMove
    '    If e.Button = MouseButtons.Left Then
    '        NewLocation = Control.MousePosition
    '        NewLocation.X -= (x)
    '        NewLocation.Y -= (y)
    '        Me.Location = NewLocation
    '    End If
    'End Sub

    Private Sub Button_cancelProgram_Click(sender As Object, e As EventArgs) Handles Button_cancelProgram.Click
        Timer_startAnim.Stop()
        Timer_startAnim.Dispose()
        Timer_endAnim.Stop()
        Timer_endAnim.Dispose()
        End
    End Sub

    'Private Sub SplashScreen1_MouseDown(sender As Object, e As MouseEventArgs) Handles MyBase.MouseDown
    '    x = Control.MousePosition.X - Me.Location.X
    '    y = Control.MousePosition.Y - Me.Location.Y
    'End Sub
End Class
