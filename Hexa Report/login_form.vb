﻿Imports System
Imports System.Drawing
Imports System.Windows.Forms
Imports MySql.Data.MySqlClient
Public Class login_form
    Private Sub login_form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ToolTip1.SetToolTip(Label_dateTime, Format(Now, "dddd, MMMM d, yyyy"))
        Me.Dock = DockStyle.Fill
        form_container.Label_title.Text = "Login - HEXA Report"
        form_container.Text = "Login - HEXA Report"

        'TextBox_password.Margin = New System.Windows.Forms.Padding(20, 20, 20, 20)
        'TextBox_username.Margin = New System.Windows.Forms.Padding(20, 20, 20, 20)
    End Sub

    'Private Sub CheckBox_showNip_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox_showNip.CheckedChanged
    '    If CheckBox_showNip.Checked Then
    '        TextBox_password.PasswordChar = ""
    '        CheckBox_showNip.Text = "Sembunyikan Password"
    '    Else
    '        TextBox_password.PasswordChar = "•"
    '        CheckBox_showNip.Text = "Tampilkan Password"
    '    End If
    'End Sub

    Private Sub Timer_dateTime_Tick(sender As Object, e As EventArgs) Handles Timer_dateTime.Tick
        'Show current date and time
        Label_dateTime.Text = Format(Now, "H:mm tt" & vbNewLine & "M/dd/yyyy")
        'ToolTip1.SetToolTip(Label_dateTime, Format(Now, "dddd, MMMM d, yyyy"))
    End Sub

    Private Sub TextBox_username_Enter(sender As Object, e As EventArgs) Handles TextBox_username.Enter
        If Control.IsKeyLocked(Keys.CapsLock) = True Then
            Label_capsWarn.Visible = True
        Else
            Label_capsWarn.Visible = False
        End If
        TextBox_username.BackColor = Color.White
        TextBox_username.ForeColor = Color.Black
    End Sub

    Private Sub TextBox_username_Leave(sender As Object, e As EventArgs) Handles TextBox_username.Leave
        Label_capsWarn.Visible = False
        TextBox_username.BackColor = Color.FromArgb(10, 10, 10)
        TextBox_username.ForeColor = Color.White
    End Sub

    Private Sub TextBox_password_Enter(sender As Object, e As EventArgs) Handles TextBox_password.Enter
        'Button_showPass.BackgroundImage = My.Resources.showPass_button_1
        'Button_showPass.BackColor = Color.White
        If Control.IsKeyLocked(Keys.CapsLock) = True Then
            Label_capsWarn.Visible = True
        Else
            Label_capsWarn.Visible = False
        End If
        TextBox_password.BackColor = Color.White
        TextBox_password.ForeColor = Color.Black

        If TextBox_password.Text.Trim.Length > 0 Then
            Button_showPass.Visible = True
        Else
            Button_showPass.Visible = False
        End If
    End Sub

    Private Sub TextBox_password_Leave(sender As Object, e As EventArgs) Handles TextBox_password.Leave
        Label_capsWarn.Visible = False
        TextBox_password.BackColor = Color.FromArgb(10, 10, 10)
        TextBox_password.ForeColor = Color.White
        Button_showPass.Visible = False
    End Sub

    Public Sub login()
        'Validation if the TextBoxes is empty
        Dim emptyTextBox =
            From TextBox In Me.Controls.OfType(Of TextBox)() 'To select all TextBoxes from login_form
            Where TextBox.Text.Trim.Length = 0 'The criteria where the value of TextBoxes is 0(null)
            Order By TextBox.Tag Descending, TextBox.Tag 'Sorting the string value from the top
            Select TextBox.Tag 'As the result, it will return what's in the Tag
        If emptyTextBox.Any Then
            MsgBox(String.Format("Harap isi terlebih dahulu" & vbNewLine & "{0}", String.Join(vbNewLine, emptyTextBox)),
                   MsgBoxStyle.Information, "HEXA Report")
        Else
            PictureBox1.Visible = False
            TextBox_username.Visible = False
            TextBox_password.Visible = False
            Label1.Visible = False
            Label4.Visible = False
            Label2.Visible = False
            Label3.Visible = False
            Label_dateTime.Visible = False
            'CheckBox_showNip.Visible = False
            Button_login.Visible = False
            Button_showPass.Visible = False
            Label_capsWarn.Visible = False
            Label_welcome.Visible = True
            PictureBox_loading.Visible = True

            query = "SELECT user_id, username, pass FROM user_account WHERE username = BINARY('" & MySqlHelper.EscapeString(TextBox_username.Text) & "') AND pass = BINARY('" & MySqlHelper.EscapeString(TextBox_password.Text) & "')"
            con.Open()
            cmd = New MySqlCommand(query, con)
            dr = cmd.ExecuteReader()

            If dr.HasRows() Then
                Do While dr.Read()
                    account_id = (dr.Item("user_id").ToString())
                Loop
                dr.Close()
                cmd.Dispose()
                con.Close()

                query = "SELECT wali_kelas.nip, master_guru.nama, master_guru.jk, wali_kelas.id_kelas, wali_kelas.id_jurusan, wali_kelas.id_lokal 
FROM wali_kelas INNER JOIN master_guru ON wali_kelas.nip = master_guru.nip INNER JOIN user_account ON wali_kelas.user_id = user_account.user_id
WHERE user_account.user_id = '" & account_id & "'"
                con.Open()
                cmd = New MySqlCommand(query, con)
                dr = cmd.ExecuteReader()

                If dr.HasRows() Then
                    Dim mn As New mainMenu_form()
                    Do While dr.Read()
                        nam = (dr.Item("nama").ToString())
                        kel = (dr.Item("id_kelas").ToString())
                        jur = (dr.Item("id_jurusan").ToString())
                        lok = (dr.Item("id_lokal").ToString())
                        wal = (dr.Item("nip").ToString())
                        jenkel = (dr.Item("jk").ToString())
                    Loop

                    If dr.Item("jk") = "L" Then
                        form_container.NotifyIcon1.BalloonTipText = "Selamat datang bapak " & nam & ""
                        form_container.NotifyIcon1.Visible = True
                        form_container.NotifyIcon1.ShowBalloonTip(1000)
                    ElseIf dr.Item("jk") = "P" Then
                        form_container.NotifyIcon1.BalloonTipText = "Selamat datang ibu " & nam & ""
                        form_container.NotifyIcon1.Visible = True
                        form_container.NotifyIcon1.ShowBalloonTip(1000)
                    End If

                    dr.Close()
                    cmd.Dispose()
                    con.Close()
                    form_container.Panel_container.Controls.Add(mn)
                    'form_container.Panel_header.BackColor = Color.FromArgb(1, 115, 152)
                    Me.Dispose()
                Else
                    dr.Close()
                    cmd.Dispose()
                    con.Close()
                    MsgBox("Anda tidak terdaftar sebagai wali kelas.",
                           MsgBoxStyle.Critical, "HEXA Report")
                    Exit Sub
                End If
            Else
                dr.Close()
                cmd.Dispose()
                con.Close()
                PictureBox1.Visible = True
                TextBox_username.Visible = True
                TextBox_password.Visible = True
                Label1.Visible = True
                Label4.Visible = True
                Label2.Visible = True
                Label3.Visible = True
                Label_dateTime.Visible = True
                'CheckBox_showNip.Visible = True
                Button_login.Visible = True
                Button_showPass.Visible = False
                Label_capsWarn.Visible = False
                Label_welcome.Visible = False
                PictureBox_loading.Visible = False
                TextBox_username.Clear()
                TextBox_password.Clear()
                MsgBox("Akun anda tidak ada. Harap masukan kembali" & vbNewLine &
                       "akun anda dengan benar.", MsgBoxStyle.Critical, "HEXA Report")
                Exit Sub
            End If
        End If
        'This is the end of the validation
    End Sub

    Private Sub TextBox_username_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox_username.KeyPress
        'To trigger a method when press Enter button
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) = True Then
            e.Handled = True
            'login()
            login()
        Else
            Exit Sub
        End If
    End Sub

    Private Sub TextBox_password_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox_password.KeyPress
        'To trigger a method when press Enter button
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) = True Then
            e.Handled = True
            'login()
            login()
        Else
            Exit Sub
        End If
    End Sub

    Private Sub TextBox_password_KeyDown(sender As Object, e As KeyEventArgs) Handles TextBox_password.KeyDown
        'Display a warn that the Caps Lock key is activated
        If Control.IsKeyLocked(Keys.CapsLock) = True Then
            Label_capsWarn.Visible = True
        Else
            Label_capsWarn.Visible = False
        End If
    End Sub

    Private Sub TextBox_username_KeyDown(sender As Object, e As KeyEventArgs) Handles TextBox_username.KeyDown
        'Display a warn that the Caps Lock key is activated
        If Control.IsKeyLocked(Keys.CapsLock) = True Then
            Label_capsWarn.Visible = True
        Else
            Label_capsWarn.Visible = False
        End If
    End Sub

    Private Sub TextBox_password_TextChanged(sender As Object, e As EventArgs) Handles TextBox_password.TextChanged
        If TextBox_password.Text.Trim.Length > 0 Then
            Button_showPass.Visible = True
        Else
            Button_showPass.Visible = False
        End If
    End Sub

    Private Sub Button_login_Click(sender As Object, e As EventArgs) Handles Button_login.Click
        login()
    End Sub

    Private Sub Button_showPass_MouseDown(sender As Object, e As MouseEventArgs) Handles Button_showPass.MouseDown
        'Button_showPass.BackgroundImage = My.Resources.showPass_button
        TextBox_password.PasswordChar = ""
        TextBox_password.Focus()
        TextBox_password.BackColor = Color.White
        TextBox_password.ForeColor = Color.Black
        'Button_showPass.BackColor = Color.FromArgb(11, 107, 118)
    End Sub

    Private Sub Button_showPass_MouseUp(sender As Object, e As MouseEventArgs) Handles Button_showPass.MouseUp
        TextBox_password.PasswordChar = "•"
        'Button_showPass.BackColor = Color.FromArgb(10, 151, 167)
    End Sub

    Private Sub Button_showPass_MouseLeave(sender As Object, e As EventArgs) Handles Button_showPass.MouseLeave
        TextBox_password.PasswordChar = "•"
    End Sub
End Class
