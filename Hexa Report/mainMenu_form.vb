﻿Imports MySql.Data.MySqlClient
'Imports MetroFramework.Drawing
Public Class mainMenu_form
    Public setToggle As Boolean = False
    Private PerCounter As System.Diagnostics.PerformanceCounter

    Private Sub mainMenu_form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        PerCounter = New System.Diagnostics.PerformanceCounter
        PerCounter.CategoryName = "Processor"
        PerCounter.CounterName = "% Processor Time"
        PerCounter.InstanceName = "_Total"

        Button_home.PerformClick()
        If restoreMenu Then
            ToolTip1.SetToolTip(Button_hideMenu, "Tampilkan Menu")
            Button_home.Text = ""
            Button_input.Text = ""
            Button_edit.Text = ""
            Button_print.Text = ""
            Button_logout.Text = ""
            Panel_menu.Width = 55
            Label3.Visible = False
            Label_cpuUse.Visible = False
            Label_ramUse.Visible = False
            Label_cpuHid.Visible = True
            Label_ramHid.Visible = True
        Else
            Button_home.Text = "MENU UTAMA"
            Button_input.Text = "INPUT NILAI"
            Button_edit.Text = "EDIT NILAI"
            Button_print.Text = "CETAK NILAI"
            Button_logout.Text = "LOGOUT"
            ToolTip1.SetToolTip(Button_hideMenu, "Sembunyikan Menu")
            Panel_menu.Width = 350
            Label3.Visible = True
            Label_cpuUse.Visible = True
            Label_ramUse.Visible = True
            Label_cpuHid.Visible = False
            Label_ramHid.Visible = False
        End If
        Me.Dock = DockStyle.Fill
        'getData()
        'rankingScore()
        'Timer_autoRefresh_data.Start()
        'countData()
        'classInfo()
        form_container.Label_title.Text = "Menu Utama - HEXA Report"
        form_container.Text = "Menu Utama - HEXA Report"
        'rowColoring()
    End Sub

    Private Sub Button_logout_Click(sender As Object, e As EventArgs) Handles Button_logout.Click
        If MsgBox("Anda yakin ingin logout?", vbQuestion + vbYesNo, "HEXA Report") = vbYes Then
            If jenkel = "L" Then
                form_container.NotifyIcon1.BalloonTipText = "Selamat tinggal bapak " & nam & ""
                form_container.NotifyIcon1.Visible = True
                form_container.NotifyIcon1.ShowBalloonTip(10000)
            ElseIf jenkel = "P" Then
                form_container.NotifyIcon1.BalloonTipText = "Selamat tinggal ibu " & nam & ""
                form_container.NotifyIcon1.Visible = True
                form_container.NotifyIcon1.ShowBalloonTip(10000)
            End If
            'Timer_autoRefresh_data.Stop()
            'Timer_autoRefresh_data.Dispose()
            dr.Close()
            cmd.Dispose()
            con.Close()
            Dim log As New login_form()
            form_container.Panel_container.Controls.Add(log)
            'form_container.Panel_header.BackColor = Color.Transparent
            Me.Dispose()
        Else
            Exit Sub
        End If
    End Sub

    Private Sub Button_input_Click(sender As Object, e As EventArgs) Handles Button_input.Click
        Panel_homeContainer.Controls.Clear()

        'Timer_autoRefresh_data.Stop()

        Dim loading_timeOut As Boolean = False
        Dim lod As New waiting_screen()
        Panel_homeContainer.Controls.Add(lod)
        While loading_timeOut = False
            Dim addGrade As New grade_form()
            addGrade.Button_save.Text = "Simpan Data"
            addGrade.Tag = "Add"
            Panel_homeContainer.Controls.Add(addGrade)
            lod.Dispose()
            loading_timeOut = True
        End While

        Button_home.ForeColor = Color.White
        Button_home.Image = My.Resources.home_button
        Panel_clicked_Main.Visible = False

        Button_input.ForeColor = Color.FromArgb(0, 174, 219)
        Button_input.Image = My.Resources.add_leger_button_blue
        Panel_clicked_Add.Visible = True

        Button_edit.ForeColor = Color.White
        Button_edit.Image = My.Resources.edit_leger_button
        Panel_clicked_Edit.Visible = False

        Button_print.ForeColor = Color.White
        Button_print.Image = My.Resources.print_leger_button
        Panel_clicked_Print.Visible = False
    End Sub

    Private Sub Button_edit_Click(sender As Object, e As EventArgs) Handles Button_edit.Click
        Panel_homeContainer.Controls.Clear()

        'Timer_autoRefresh_data.Stop()

        Dim loading_timeOut As Boolean = False
        Dim lod As New waiting_screen()
        Panel_homeContainer.Controls.Add(lod)
        While loading_timeOut = False
            Dim addGrade As New grade_form()
            addGrade.Button_save.Text = "Edit Data"
            addGrade.Tag = "Edit"
            Panel_homeContainer.Controls.Add(addGrade)
            lod.Dispose()
            loading_timeOut = True
        End While

        Button_home.ForeColor = Color.White
        Button_home.Image = My.Resources.home_button
        Panel_clicked_Main.Visible = False

        Button_input.ForeColor = Color.White
        Button_input.Image = My.Resources.add_leger_button
        Panel_clicked_Add.Visible = False

        Button_edit.ForeColor = Color.FromArgb(0, 174, 219)
        Button_edit.Image = My.Resources.edit_leger_button_blue
        Panel_clicked_Edit.Visible = True

        Button_print.ForeColor = Color.White
        Button_print.Image = My.Resources.print_leger_button
        Panel_clicked_Print.Visible = False
    End Sub

    Private Sub Button_print_Click(sender As Object, e As EventArgs) Handles Button_print.Click
        Panel_homeContainer.Controls.Clear()

        'Timer_autoRefresh_data.Stop()

        Dim loading_timeOut As Boolean = False
        Dim lod As New waiting_screen()
        Panel_homeContainer.Controls.Add(lod)
        While loading_timeOut = False
            Dim rf As New report_form()
            Panel_homeContainer.Controls.Add(rf)
            lod.Dispose()
            loading_timeOut = True
        End While

        Button_home.ForeColor = Color.White
        Button_home.Image = My.Resources.home_button
        Panel_clicked_Main.Visible = False

        Button_input.ForeColor = Color.White
        Button_input.Image = My.Resources.add_leger_button
        Panel_clicked_Add.Visible = False

        Button_edit.ForeColor = Color.White
        Button_edit.Image = My.Resources.edit_leger_button
        Panel_clicked_Edit.Visible = False

        Button_print.ForeColor = Color.FromArgb(0, 174, 219)
        Button_print.Image = My.Resources.print_leger_button_blue
        Panel_clicked_Print.Visible = True
    End Sub

    Private Sub Button_hideMenu_Click(sender As Object, e As EventArgs) Handles Button_hideMenu.Click
        If restoreMenu = False Then
            ToolTip1.SetToolTip(Button_hideMenu, "Tampilkan Menu")
            Button_home.Text = ""
            Button_input.Text = ""
            Button_edit.Text = ""
            Button_print.Text = ""
            Button_logout.Text = ""
            Panel_menu.Width = 55
            Label3.Visible = False
            Label_cpuUse.Visible = False
            Label_ramUse.Visible = False
            Label_cpuHid.Visible = True
            Label_ramHid.Visible = True
            restoreMenu = True
        ElseIf restoreMenu = True Then
            Button_home.Text = "MENU UTAMA"
            Button_input.Text = "INPUT NILAI"
            Button_edit.Text = "EDIT NILAI"
            Button_print.Text = "CETAK NILAI"
            Button_logout.Text = "LOGOUT"
            ToolTip1.SetToolTip(Button_hideMenu, "Sembunyikan Menu")
            Panel_menu.Width = 350
            Label3.Visible = True
            Label_cpuUse.Visible = True
            Label_ramUse.Visible = True
            Label_cpuHid.Visible = False
            Label_ramHid.Visible = False
            restoreMenu = False
        End If
    End Sub

    Private Sub Button_searchHidden_Click(sender As Object, e As EventArgs)
        If restoreMenu = True Then
            Button_home.Text = "MENU UTAMA"
            Button_input.Text = "INPUT NILAI"
            Button_edit.Text = "EDIT NILAI"
            Button_print.Text = "CETAK NILAI"
            Button_logout.Text = "LOGOUT"
            ToolTip1.SetToolTip(Button_hideMenu, "Sembunyikan Menu")
            Panel_menu.Width = 350
            'Button_searchHidden.Visible = False
            'Label1.Visible = True
            'TextBox_searchStudent.Visible = True
            'Button_search.Visible = True
            'TextBox_searchStudent.Focus()
            restoreMenu = False
        Else
            Exit Sub
        End If
    End Sub

    Private Sub Button_home_Click(sender As Object, e As EventArgs) Handles Button_home.Click
        Panel_homeContainer.Controls.Clear()
        Dim hf As New home_form()
        Panel_homeContainer.Controls.Add(hf)

        Button_home.ForeColor = Color.FromArgb(0, 174, 219)
        Button_home.Image = My.Resources.home_button_blue
        Panel_clicked_Main.Visible = True

        Button_input.ForeColor = Color.White
        Button_input.Image = My.Resources.add_leger_button
        Panel_clicked_Add.Visible = False

        Button_edit.ForeColor = Color.White
        Button_edit.Image = My.Resources.edit_leger_button
        Panel_clicked_Edit.Visible = False

        Button_print.ForeColor = Color.White
        Button_print.Image = My.Resources.print_leger_button
        Panel_clicked_Print.Visible = False
    End Sub

    Private Sub Timer_monitoring_Tick(sender As Object, e As EventArgs) Handles Timer_monitoring.Tick
        Dim cpuVal As Integer = Integer.Parse(Format(PerCounter.NextValue, "##0"))
        'MetroProgressBar_cpu.Value = cpuVal
        Label_cpuUse.Text = "CPU " & cpuVal & "%"
        Label_cpuHid.Text = "CPU" & vbNewLine & cpuVal & "%"

        Dim ramVal As Double = (My.Computer.Info.TotalPhysicalMemory - My.Computer.Info.AvailablePhysicalMemory) / 1048576 / 1024
        Label_ramUse.Text = "RAM " & ramVal.ToString("N") & " GB"
        Label_ramHid.Text = "RAM" & vbNewLine & ramVal.ToString("N") & " GB"

        Dim ramAv As Long = My.Computer.Info.AvailablePhysicalMemory * 100
        Dim ramProg As Long = (ramAv / My.Computer.Info.TotalPhysicalMemory)
        'MetroProgressBar_ram.Value = ramProg
        'Label3.Text = ramProg

        Select Case (cpuVal)
            Case 80 To 100
                Label_cpuUse.ForeColor = Color.FromArgb(255, 31, 68)
                Label_cpuHid.ForeColor = Color.FromArgb(255, 31, 68)
            Case 0 To 79
                Label_cpuUse.ForeColor = Color.White
                Label_cpuHid.ForeColor = Color.White
        End Select

        Select Case (ramProg)
            Case 0 To 35
                Label_ramUse.ForeColor = Color.FromArgb(255, 31, 68)
                Label_ramHid.ForeColor = Color.FromArgb(255, 31, 68)
            Case 36 To 100
                Label_ramUse.ForeColor = Color.White
                Label_ramHid.ForeColor = Color.White
        End Select
    End Sub
End Class
