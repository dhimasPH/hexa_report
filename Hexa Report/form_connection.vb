﻿'This for importing MySql module to create a 
'connection between the Program And MySql Server
Imports MySql.Data.MySqlClient
Module form_connection
    'Connection declaration
    '
    'conString is created to find the Server
    Public conString As String = "Server=localhost;UID=root;pwd=;Database=database_raport"

    'con is used to connect the program to Server
    Public con As New MySql.Data.MySqlClient.MySqlConnection(conString)

    'da is an interface between the Data Set and the Database
    Public da As MySql.Data.MySqlClient.MySqlDataAdapter

    'cmd is used to take the execution query command
    Public cmd As New MySql.Data.MySqlClient.MySqlCommand

    'dr is a command the read the query
    Public dr As MySql.Data.MySqlClient.MySqlDataReader

    'ds is used to set the area in which data is loaded to read/modify
    Public ds As New DataSet
    '
    'This is the end of the Connection declaration

    'This is a Sub to open the connection
    Public Sub openDB()
        If con.State = ConnectionState.Open Then
            con.Close()
        End If
        con.Open()
    End Sub

End Module
