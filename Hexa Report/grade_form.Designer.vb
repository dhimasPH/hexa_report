﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class grade_form
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.MetroComboBox_smes = New MetroFramework.Controls.MetroComboBox()
        Me.Label_jur = New System.Windows.Forms.Label()
        Me.Label91 = New System.Windows.Forms.Label()
        Me.Label92 = New System.Windows.Forms.Label()
        Me.Label_thnPel = New System.Windows.Forms.Label()
        Me.Label89 = New System.Windows.Forms.Label()
        Me.Label90 = New System.Windows.Forms.Label()
        Me.Label_smes = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.Label_kelas = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label_nisSis = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label_namaSiswa = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Button_firstItem = New System.Windows.Forms.Button()
        Me.Button_lastItem = New System.Windows.Forms.Button()
        Me.Label93 = New System.Windows.Forms.Label()
        Me.Label87 = New System.Windows.Forms.Label()
        Me.Button_reset = New System.Windows.Forms.Button()
        Me.Button_save = New System.Windows.Forms.Button()
        Me.Label_nis = New System.Windows.Forms.Label()
        Me.Button_nextItem = New System.Windows.Forms.Button()
        Me.Button_backItem = New System.Windows.Forms.Button()
        Me.TextBox_searchNis = New System.Windows.Forms.TextBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.MetroTabControl1 = New MetroFramework.Controls.MetroTabControl()
        Me.MetroTabPage_mapelNA = New MetroFramework.Controls.MetroTabPage()
        Me.Label_hurKetKim = New System.Windows.Forms.Label()
        Me.Label_hurPengKim = New System.Windows.Forms.Label()
        Me.TextBox_ketKim = New System.Windows.Forms.TextBox()
        Me.TextBox_pengKim = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.TextBox_pengPai = New System.Windows.Forms.TextBox()
        Me.Label_hurKetFis = New System.Windows.Forms.Label()
        Me.TextBox_ketPai = New System.Windows.Forms.TextBox()
        Me.Label_hurPengFis = New System.Windows.Forms.Label()
        Me.Label_hurPengPai = New System.Windows.Forms.Label()
        Me.TextBox_ketFis = New System.Windows.Forms.TextBox()
        Me.Label_hurKetPai = New System.Windows.Forms.Label()
        Me.TextBox_pengFis = New System.Windows.Forms.TextBox()
        Me.TextBox_ketPkn = New System.Windows.Forms.TextBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label_hurKetMul = New System.Windows.Forms.Label()
        Me.TextBox_pengPkn = New System.Windows.Forms.TextBox()
        Me.Label_hurPengMul = New System.Windows.Forms.Label()
        Me.Label_hurPengPkn = New System.Windows.Forms.Label()
        Me.TextBox_ketMul = New System.Windows.Forms.TextBox()
        Me.Label_hurKetPkn = New System.Windows.Forms.Label()
        Me.TextBox_pengMul = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.TextBox_pengMtk = New System.Windows.Forms.TextBox()
        Me.Label_hurKetKwh = New System.Windows.Forms.Label()
        Me.TextBox_ketMtk = New System.Windows.Forms.TextBox()
        Me.Label_hurPengKwh = New System.Windows.Forms.Label()
        Me.Label_hurPengMtk = New System.Windows.Forms.Label()
        Me.TextBox_ketKwh = New System.Windows.Forms.TextBox()
        Me.Label_hurKetMtk = New System.Windows.Forms.Label()
        Me.TextBox_pengKwh = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.TextBox_pengIndo = New System.Windows.Forms.TextBox()
        Me.Label_hurKetPen = New System.Windows.Forms.Label()
        Me.TextBox_ketIndo = New System.Windows.Forms.TextBox()
        Me.Label_hurPengPen = New System.Windows.Forms.Label()
        Me.Label_hurPengIndo = New System.Windows.Forms.Label()
        Me.TextBox_ketPen = New System.Windows.Forms.TextBox()
        Me.Label_hurKetIndo = New System.Windows.Forms.Label()
        Me.TextBox_pengPen = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.TextBox_pengSej = New System.Windows.Forms.TextBox()
        Me.Label_hurKetSbk = New System.Windows.Forms.Label()
        Me.TextBox_ketSej = New System.Windows.Forms.TextBox()
        Me.Label_hurPengSbk = New System.Windows.Forms.Label()
        Me.Label_hurPengSej = New System.Windows.Forms.Label()
        Me.TextBox_ketSbk = New System.Windows.Forms.TextBox()
        Me.Label_hurKetSej = New System.Windows.Forms.Label()
        Me.TextBox_pengSbk = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.TextBox_pengIng = New System.Windows.Forms.TextBox()
        Me.Label_hurKetIng = New System.Windows.Forms.Label()
        Me.TextBox_ketIng = New System.Windows.Forms.TextBox()
        Me.Label_hurPengIng = New System.Windows.Forms.Label()
        Me.MetroTabPage_mapelProd = New MetroFramework.Controls.MetroTabPage()
        Me.Label_hurKetProd9 = New System.Windows.Forms.Label()
        Me.Label_hurKetProd11 = New System.Windows.Forms.Label()
        Me.Label_hurPengProd9 = New System.Windows.Forms.Label()
        Me.TextBox_ketProd9 = New System.Windows.Forms.TextBox()
        Me.Label_hurPengProd11 = New System.Windows.Forms.Label()
        Me.TextBox_pengProd9 = New System.Windows.Forms.TextBox()
        Me.Label77 = New System.Windows.Forms.Label()
        Me.TextBox_ketProd11 = New System.Windows.Forms.TextBox()
        Me.Label78 = New System.Windows.Forms.Label()
        Me.Label79 = New System.Windows.Forms.Label()
        Me.TextBox_pengProd11 = New System.Windows.Forms.TextBox()
        Me.Label_hurKetProd6 = New System.Windows.Forms.Label()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Label_hurPengProd6 = New System.Windows.Forms.Label()
        Me.Label83 = New System.Windows.Forms.Label()
        Me.TextBox_ketProd6 = New System.Windows.Forms.TextBox()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.TextBox_pengProd6 = New System.Windows.Forms.TextBox()
        Me.Label84 = New System.Windows.Forms.Label()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.Label71 = New System.Windows.Forms.Label()
        Me.Label85 = New System.Windows.Forms.Label()
        Me.Label72 = New System.Windows.Forms.Label()
        Me.TextBox_pengProd1 = New System.Windows.Forms.TextBox()
        Me.Label_hurKetProd3 = New System.Windows.Forms.Label()
        Me.Label_hurKetProd10 = New System.Windows.Forms.Label()
        Me.Label_hurPengProd3 = New System.Windows.Forms.Label()
        Me.TextBox_ketProd1 = New System.Windows.Forms.TextBox()
        Me.TextBox_ketProd3 = New System.Windows.Forms.TextBox()
        Me.Label_hurPengProd10 = New System.Windows.Forms.Label()
        Me.TextBox_pengProd3 = New System.Windows.Forms.TextBox()
        Me.Label_hurPengProd1 = New System.Windows.Forms.Label()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.TextBox_ketProd10 = New System.Windows.Forms.TextBox()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.Label_hurKetProd1 = New System.Windows.Forms.Label()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.TextBox_pengProd10 = New System.Windows.Forms.TextBox()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.Label80 = New System.Windows.Forms.Label()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.Label81 = New System.Windows.Forms.Label()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.Label82 = New System.Windows.Forms.Label()
        Me.TextBox_pengProd2 = New System.Windows.Forms.TextBox()
        Me.TextBox_ketProd2 = New System.Windows.Forms.TextBox()
        Me.Label_hurPengProd2 = New System.Windows.Forms.Label()
        Me.Label_hurKetProd2 = New System.Windows.Forms.Label()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.TextBox_pengProd4 = New System.Windows.Forms.TextBox()
        Me.Label_hurKetProd8 = New System.Windows.Forms.Label()
        Me.TextBox_ketProd4 = New System.Windows.Forms.TextBox()
        Me.Label_hurPengProd8 = New System.Windows.Forms.Label()
        Me.Label_hurPengProd4 = New System.Windows.Forms.Label()
        Me.TextBox_ketProd8 = New System.Windows.Forms.TextBox()
        Me.Label_hurKetProd4 = New System.Windows.Forms.Label()
        Me.TextBox_pengProd8 = New System.Windows.Forms.TextBox()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.Label74 = New System.Windows.Forms.Label()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.Label75 = New System.Windows.Forms.Label()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.Label76 = New System.Windows.Forms.Label()
        Me.TextBox_pengProd5 = New System.Windows.Forms.TextBox()
        Me.Label_hurKetProd7 = New System.Windows.Forms.Label()
        Me.TextBox_ketProd5 = New System.Windows.Forms.TextBox()
        Me.Label_hurPengProd7 = New System.Windows.Forms.Label()
        Me.Label_hurPengProd5 = New System.Windows.Forms.Label()
        Me.TextBox_ketProd7 = New System.Windows.Forms.TextBox()
        Me.Label_hurKetProd5 = New System.Windows.Forms.Label()
        Me.TextBox_pengProd7 = New System.Windows.Forms.TextBox()
        Me.Label73 = New System.Windows.Forms.Label()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.MetroTabPage_supp = New MetroFramework.Controls.MetroTabPage()
        Me.MetroComboBox_ekskul = New MetroFramework.Controls.MetroComboBox()
        Me.Label_kar = New System.Windows.Forms.Label()
        Me.Label95 = New System.Windows.Forms.Label()
        Me.TextBox_komen = New System.Windows.Forms.TextBox()
        Me.Label94 = New System.Windows.Forms.Label()
        Me.Label88 = New System.Windows.Forms.Label()
        Me.TextBox_alpha = New System.Windows.Forms.TextBox()
        Me.Label86 = New System.Windows.Forms.Label()
        Me.TextBox_izin = New System.Windows.Forms.TextBox()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.TextBox_sakit = New System.Windows.Forms.TextBox()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.MetroTabControl1.SuspendLayout()
        Me.MetroTabPage_mapelNA.SuspendLayout()
        Me.MetroTabPage_mapelProd.SuspendLayout()
        Me.MetroTabPage_supp.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        Me.Panel1.Controls.Add(Me.MetroComboBox_smes)
        Me.Panel1.Controls.Add(Me.Label_jur)
        Me.Panel1.Controls.Add(Me.Label91)
        Me.Panel1.Controls.Add(Me.Label92)
        Me.Panel1.Controls.Add(Me.Label_thnPel)
        Me.Panel1.Controls.Add(Me.Label89)
        Me.Panel1.Controls.Add(Me.Label90)
        Me.Panel1.Controls.Add(Me.Label_smes)
        Me.Panel1.Controls.Add(Me.Label50)
        Me.Panel1.Controls.Add(Me.Label51)
        Me.Panel1.Controls.Add(Me.Label_kelas)
        Me.Panel1.Controls.Add(Me.Label48)
        Me.Panel1.Controls.Add(Me.Label49)
        Me.Panel1.Controls.Add(Me.Label_nisSis)
        Me.Panel1.Controls.Add(Me.Label46)
        Me.Panel1.Controls.Add(Me.Label47)
        Me.Panel1.Controls.Add(Me.Label42)
        Me.Panel1.Controls.Add(Me.Label_namaSiswa)
        Me.Panel1.Controls.Add(Me.Label43)
        Me.Panel1.Controls.Add(Me.Label44)
        Me.Panel1.Controls.Add(Me.Label30)
        Me.Panel1.Controls.Add(Me.Label34)
        Me.Panel1.Controls.Add(Me.Label35)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1050, 136)
        Me.Panel1.TabIndex = 0
        '
        'MetroComboBox_smes
        '
        Me.MetroComboBox_smes.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.MetroComboBox_smes.ForeColor = System.Drawing.Color.White
        Me.MetroComboBox_smes.FormattingEnabled = True
        Me.MetroComboBox_smes.ItemHeight = 23
        Me.MetroComboBox_smes.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6"})
        Me.MetroComboBox_smes.Location = New System.Drawing.Point(767, 35)
        Me.MetroComboBox_smes.Name = "MetroComboBox_smes"
        Me.MetroComboBox_smes.Size = New System.Drawing.Size(63, 29)
        Me.MetroComboBox_smes.TabIndex = 27
        Me.MetroComboBox_smes.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MetroComboBox_smes.UseSelectable = True
        Me.MetroComboBox_smes.Visible = False
        '
        'Label_jur
        '
        Me.Label_jur.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label_jur.AutoSize = True
        Me.Label_jur.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.Label_jur.ForeColor = System.Drawing.Color.White
        Me.Label_jur.Location = New System.Drawing.Point(763, 103)
        Me.Label_jur.Name = "Label_jur"
        Me.Label_jur.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label_jur.Size = New System.Drawing.Size(66, 19)
        Me.Label_jur.TabIndex = 25
        Me.Label_jur.Text = "-jurusan-"
        '
        'Label91
        '
        Me.Label91.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label91.AutoSize = True
        Me.Label91.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.Label91.ForeColor = System.Drawing.Color.White
        Me.Label91.Location = New System.Drawing.Point(745, 103)
        Me.Label91.Name = "Label91"
        Me.Label91.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label91.Size = New System.Drawing.Size(12, 19)
        Me.Label91.TabIndex = 24
        Me.Label91.Text = ":"
        '
        'Label92
        '
        Me.Label92.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label92.AutoSize = True
        Me.Label92.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.Label92.ForeColor = System.Drawing.Color.White
        Me.Label92.Location = New System.Drawing.Point(598, 103)
        Me.Label92.Name = "Label92"
        Me.Label92.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label92.Size = New System.Drawing.Size(115, 19)
        Me.Label92.TabIndex = 23
        Me.Label92.Text = "PAKET KEAHLIAN"
        '
        'Label_thnPel
        '
        Me.Label_thnPel.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label_thnPel.AutoSize = True
        Me.Label_thnPel.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.Label_thnPel.ForeColor = System.Drawing.Color.White
        Me.Label_thnPel.Location = New System.Drawing.Point(763, 70)
        Me.Label_thnPel.Name = "Label_thnPel"
        Me.Label_thnPel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label_thnPel.Size = New System.Drawing.Size(57, 19)
        Me.Label_thnPel.TabIndex = 22
        Me.Label_thnPel.Text = "-tahun-"
        '
        'Label89
        '
        Me.Label89.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label89.AutoSize = True
        Me.Label89.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.Label89.ForeColor = System.Drawing.Color.White
        Me.Label89.Location = New System.Drawing.Point(745, 70)
        Me.Label89.Name = "Label89"
        Me.Label89.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label89.Size = New System.Drawing.Size(12, 19)
        Me.Label89.TabIndex = 21
        Me.Label89.Text = ":"
        '
        'Label90
        '
        Me.Label90.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label90.AutoSize = True
        Me.Label90.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.Label90.ForeColor = System.Drawing.Color.White
        Me.Label90.Location = New System.Drawing.Point(598, 70)
        Me.Label90.Name = "Label90"
        Me.Label90.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label90.Size = New System.Drawing.Size(131, 19)
        Me.Label90.TabIndex = 20
        Me.Label90.Text = "TAHUN PELAJARAN"
        '
        'Label_smes
        '
        Me.Label_smes.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label_smes.AutoSize = True
        Me.Label_smes.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.Label_smes.ForeColor = System.Drawing.Color.White
        Me.Label_smes.Location = New System.Drawing.Point(763, 39)
        Me.Label_smes.Name = "Label_smes"
        Me.Label_smes.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label_smes.Size = New System.Drawing.Size(76, 19)
        Me.Label_smes.TabIndex = 19
        Me.Label_smes.Text = "-semester-"
        '
        'Label50
        '
        Me.Label50.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label50.AutoSize = True
        Me.Label50.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.Label50.ForeColor = System.Drawing.Color.White
        Me.Label50.Location = New System.Drawing.Point(745, 39)
        Me.Label50.Name = "Label50"
        Me.Label50.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label50.Size = New System.Drawing.Size(12, 19)
        Me.Label50.TabIndex = 18
        Me.Label50.Text = ":"
        '
        'Label51
        '
        Me.Label51.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label51.AutoSize = True
        Me.Label51.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.Label51.ForeColor = System.Drawing.Color.White
        Me.Label51.Location = New System.Drawing.Point(598, 39)
        Me.Label51.Name = "Label51"
        Me.Label51.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label51.Size = New System.Drawing.Size(72, 19)
        Me.Label51.TabIndex = 17
        Me.Label51.Text = "SEMESTER"
        '
        'Label_kelas
        '
        Me.Label_kelas.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label_kelas.AutoSize = True
        Me.Label_kelas.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.Label_kelas.ForeColor = System.Drawing.Color.White
        Me.Label_kelas.Location = New System.Drawing.Point(763, 8)
        Me.Label_kelas.Name = "Label_kelas"
        Me.Label_kelas.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label_kelas.Size = New System.Drawing.Size(51, 19)
        Me.Label_kelas.TabIndex = 16
        Me.Label_kelas.Text = "-kelas-"
        '
        'Label48
        '
        Me.Label48.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label48.AutoSize = True
        Me.Label48.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.Label48.ForeColor = System.Drawing.Color.White
        Me.Label48.Location = New System.Drawing.Point(745, 8)
        Me.Label48.Name = "Label48"
        Me.Label48.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label48.Size = New System.Drawing.Size(12, 19)
        Me.Label48.TabIndex = 15
        Me.Label48.Text = ":"
        '
        'Label49
        '
        Me.Label49.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label49.AutoSize = True
        Me.Label49.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.Label49.ForeColor = System.Drawing.Color.White
        Me.Label49.Location = New System.Drawing.Point(598, 8)
        Me.Label49.Name = "Label49"
        Me.Label49.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label49.Size = New System.Drawing.Size(47, 19)
        Me.Label49.TabIndex = 14
        Me.Label49.Text = "KELAS"
        '
        'Label_nisSis
        '
        Me.Label_nisSis.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label_nisSis.AutoSize = True
        Me.Label_nisSis.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.Label_nisSis.ForeColor = System.Drawing.Color.White
        Me.Label_nisSis.Location = New System.Drawing.Point(341, 103)
        Me.Label_nisSis.Name = "Label_nisSis"
        Me.Label_nisSis.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label_nisSis.Size = New System.Drawing.Size(38, 19)
        Me.Label_nisSis.TabIndex = 13
        Me.Label_nisSis.Text = "-nis-"
        '
        'Label46
        '
        Me.Label46.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label46.AutoSize = True
        Me.Label46.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.Label46.ForeColor = System.Drawing.Color.White
        Me.Label46.Location = New System.Drawing.Point(323, 103)
        Me.Label46.Name = "Label46"
        Me.Label46.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label46.Size = New System.Drawing.Size(12, 19)
        Me.Label46.TabIndex = 12
        Me.Label46.Text = ":"
        '
        'Label47
        '
        Me.Label47.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label47.AutoSize = True
        Me.Label47.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.Label47.ForeColor = System.Drawing.Color.White
        Me.Label47.Location = New System.Drawing.Point(176, 103)
        Me.Label47.Name = "Label47"
        Me.Label47.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label47.Size = New System.Drawing.Size(30, 19)
        Me.Label47.TabIndex = 11
        Me.Label47.Text = "NIS"
        '
        'Label42
        '
        Me.Label42.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.Label42.ForeColor = System.Drawing.Color.White
        Me.Label42.Location = New System.Drawing.Point(341, 8)
        Me.Label42.Name = "Label42"
        Me.Label42.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label42.Size = New System.Drawing.Size(129, 19)
        Me.Label42.TabIndex = 10
        Me.Label42.Text = "SMKN 1 CIBINONG"
        '
        'Label_namaSiswa
        '
        Me.Label_namaSiswa.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label_namaSiswa.AutoSize = True
        Me.Label_namaSiswa.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.Label_namaSiswa.ForeColor = System.Drawing.Color.White
        Me.Label_namaSiswa.Location = New System.Drawing.Point(341, 70)
        Me.Label_namaSiswa.Name = "Label_namaSiswa"
        Me.Label_namaSiswa.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label_namaSiswa.Size = New System.Drawing.Size(55, 19)
        Me.Label_namaSiswa.TabIndex = 9
        Me.Label_namaSiswa.Text = "-nama-"
        '
        'Label43
        '
        Me.Label43.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label43.AutoSize = True
        Me.Label43.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.Label43.ForeColor = System.Drawing.Color.White
        Me.Label43.Location = New System.Drawing.Point(323, 70)
        Me.Label43.Name = "Label43"
        Me.Label43.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label43.Size = New System.Drawing.Size(12, 19)
        Me.Label43.TabIndex = 8
        Me.Label43.Text = ":"
        '
        'Label44
        '
        Me.Label44.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label44.AutoSize = True
        Me.Label44.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.Label44.ForeColor = System.Drawing.Color.White
        Me.Label44.Location = New System.Drawing.Point(176, 70)
        Me.Label44.Name = "Label44"
        Me.Label44.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label44.Size = New System.Drawing.Size(145, 19)
        Me.Label44.TabIndex = 7
        Me.Label44.Text = "NAMA PESERTA DIDIK"
        '
        'Label30
        '
        Me.Label30.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.Label30.ForeColor = System.Drawing.Color.White
        Me.Label30.Location = New System.Drawing.Point(341, 39)
        Me.Label30.Name = "Label30"
        Me.Label30.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label30.Size = New System.Drawing.Size(226, 19)
        Me.Label30.TabIndex = 6
        Me.Label30.Text = "JL KARADENAN NO 7 KARADENAN"
        '
        'Label34
        '
        Me.Label34.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.Label34.ForeColor = System.Drawing.Color.White
        Me.Label34.Location = New System.Drawing.Point(323, 39)
        Me.Label34.Name = "Label34"
        Me.Label34.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label34.Size = New System.Drawing.Size(12, 19)
        Me.Label34.TabIndex = 5
        Me.Label34.Text = ":"
        '
        'Label35
        '
        Me.Label35.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.Label35.ForeColor = System.Drawing.Color.White
        Me.Label35.Location = New System.Drawing.Point(176, 39)
        Me.Label35.Name = "Label35"
        Me.Label35.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label35.Size = New System.Drawing.Size(62, 19)
        Me.Label35.TabIndex = 4
        Me.Label35.Text = "ALAMAT"
        '
        'Label5
        '
        Me.Label5.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(323, 8)
        Me.Label5.Name = "Label5"
        Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label5.Size = New System.Drawing.Size(12, 19)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = ":"
        '
        'Label4
        '
        Me.Label4.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(176, 8)
        Me.Label4.Name = "Label4"
        Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label4.Size = New System.Drawing.Size(112, 19)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "NAMA SEKOLAH"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(8, Byte), Integer), CType(CType(7, Byte), Integer), CType(CType(7, Byte), Integer))
        Me.Panel2.Controls.Add(Me.Button_firstItem)
        Me.Panel2.Controls.Add(Me.Button_lastItem)
        Me.Panel2.Controls.Add(Me.Label93)
        Me.Panel2.Controls.Add(Me.Label87)
        Me.Panel2.Controls.Add(Me.Button_reset)
        Me.Panel2.Controls.Add(Me.Button_save)
        Me.Panel2.Controls.Add(Me.Label_nis)
        Me.Panel2.Controls.Add(Me.Button_nextItem)
        Me.Panel2.Controls.Add(Me.Button_backItem)
        Me.Panel2.Controls.Add(Me.TextBox_searchNis)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(0, 538)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1050, 62)
        Me.Panel2.TabIndex = 1
        '
        'Button_firstItem
        '
        Me.Button_firstItem.BackgroundImage = Global.Hexa_Report.My.Resources.Resources.firstItem_button
        Me.Button_firstItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button_firstItem.FlatAppearance.BorderSize = 0
        Me.Button_firstItem.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(47, Byte), Integer))
        Me.Button_firstItem.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(32, Byte), Integer))
        Me.Button_firstItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_firstItem.Font = New System.Drawing.Font("Wingdings 3", 21.5!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Button_firstItem.ForeColor = System.Drawing.Color.White
        Me.Button_firstItem.Location = New System.Drawing.Point(164, 13)
        Me.Button_firstItem.Name = "Button_firstItem"
        Me.Button_firstItem.Size = New System.Drawing.Size(50, 40)
        Me.Button_firstItem.TabIndex = 9
        Me.ToolTip1.SetToolTip(Me.Button_firstItem, "Lihat Data Siswa Terawal")
        Me.Button_firstItem.UseVisualStyleBackColor = True
        '
        'Button_lastItem
        '
        Me.Button_lastItem.BackgroundImage = Global.Hexa_Report.My.Resources.Resources.lastItem_button
        Me.Button_lastItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button_lastItem.FlatAppearance.BorderSize = 0
        Me.Button_lastItem.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(47, Byte), Integer))
        Me.Button_lastItem.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(32, Byte), Integer))
        Me.Button_lastItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_lastItem.Font = New System.Drawing.Font("Wingdings 3", 21.5!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Button_lastItem.ForeColor = System.Drawing.Color.White
        Me.Button_lastItem.Location = New System.Drawing.Point(432, 13)
        Me.Button_lastItem.Name = "Button_lastItem"
        Me.Button_lastItem.Size = New System.Drawing.Size(50, 40)
        Me.Button_lastItem.TabIndex = 8
        Me.ToolTip1.SetToolTip(Me.Button_lastItem, "Lihat Data Siswa Terakhir")
        Me.Button_lastItem.UseVisualStyleBackColor = True
        '
        'Label93
        '
        Me.Label93.AutoSize = True
        Me.Label93.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label93.ForeColor = System.Drawing.Color.White
        Me.Label93.Location = New System.Drawing.Point(278, 25)
        Me.Label93.Name = "Label93"
        Me.Label93.Size = New System.Drawing.Size(37, 19)
        Me.Label93.TabIndex = 7
        Me.Label93.Text = "NIS :"
        '
        'Label87
        '
        Me.Label87.AutoSize = True
        Me.Label87.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label87.ForeColor = System.Drawing.Color.White
        Me.Label87.Location = New System.Drawing.Point(23, 23)
        Me.Label87.Name = "Label87"
        Me.Label87.Size = New System.Drawing.Size(137, 19)
        Me.Label87.TabIndex = 6
        Me.Label87.Text = "Pencarian Data Siswa"
        '
        'Button_reset
        '
        Me.Button_reset.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button_reset.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button_reset.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(47, Byte), Integer))
        Me.Button_reset.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(32, Byte), Integer))
        Me.Button_reset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_reset.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Button_reset.ForeColor = System.Drawing.Color.White
        Me.Button_reset.Location = New System.Drawing.Point(897, 17)
        Me.Button_reset.Name = "Button_reset"
        Me.Button_reset.Size = New System.Drawing.Size(105, 28)
        Me.Button_reset.TabIndex = 5
        Me.Button_reset.Text = "Reset"
        Me.ToolTip1.SetToolTip(Me.Button_reset, "Reset")
        Me.Button_reset.UseVisualStyleBackColor = True
        '
        'Button_save
        '
        Me.Button_save.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button_save.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button_save.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(47, Byte), Integer))
        Me.Button_save.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(32, Byte), Integer))
        Me.Button_save.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_save.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Button_save.ForeColor = System.Drawing.Color.White
        Me.Button_save.Location = New System.Drawing.Point(759, 17)
        Me.Button_save.Name = "Button_save"
        Me.Button_save.Size = New System.Drawing.Size(105, 28)
        Me.Button_save.TabIndex = 4
        Me.ToolTip1.SetToolTip(Me.Button_save, "Simpan Data")
        Me.Button_save.UseVisualStyleBackColor = True
        '
        'Label_nis
        '
        Me.Label_nis.AutoSize = True
        Me.Label_nis.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.Label_nis.ForeColor = System.Drawing.Color.White
        Me.Label_nis.Location = New System.Drawing.Point(318, 25)
        Me.Label_nis.Name = "Label_nis"
        Me.Label_nis.Size = New System.Drawing.Size(26, 19)
        Me.Label_nis.TabIndex = 1
        Me.Label_nis.Text = "nis"
        Me.ToolTip1.SetToolTip(Me.Label_nis, "NIS")
        '
        'Button_nextItem
        '
        Me.Button_nextItem.BackgroundImage = Global.Hexa_Report.My.Resources.Resources.next_button
        Me.Button_nextItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button_nextItem.FlatAppearance.BorderSize = 0
        Me.Button_nextItem.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(47, Byte), Integer))
        Me.Button_nextItem.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(32, Byte), Integer))
        Me.Button_nextItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_nextItem.Font = New System.Drawing.Font("Wingdings 3", 21.5!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Button_nextItem.ForeColor = System.Drawing.Color.White
        Me.Button_nextItem.Location = New System.Drawing.Point(379, 13)
        Me.Button_nextItem.Name = "Button_nextItem"
        Me.Button_nextItem.Size = New System.Drawing.Size(50, 40)
        Me.Button_nextItem.TabIndex = 2
        Me.ToolTip1.SetToolTip(Me.Button_nextItem, "Lihat Data Siswa Selanjutnya")
        Me.Button_nextItem.UseVisualStyleBackColor = True
        '
        'Button_backItem
        '
        Me.Button_backItem.BackgroundImage = Global.Hexa_Report.My.Resources.Resources.back_button
        Me.Button_backItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button_backItem.FlatAppearance.BorderSize = 0
        Me.Button_backItem.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(47, Byte), Integer))
        Me.Button_backItem.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(32, Byte), Integer))
        Me.Button_backItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_backItem.Font = New System.Drawing.Font("Wingdings 3", 21.5!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Button_backItem.ForeColor = System.Drawing.Color.White
        Me.Button_backItem.Location = New System.Drawing.Point(217, 13)
        Me.Button_backItem.Name = "Button_backItem"
        Me.Button_backItem.Size = New System.Drawing.Size(50, 40)
        Me.Button_backItem.TabIndex = 0
        Me.ToolTip1.SetToolTip(Me.Button_backItem, "Lihat Data Siswa Sebelumnya")
        Me.Button_backItem.UseVisualStyleBackColor = True
        '
        'TextBox_searchNis
        '
        Me.TextBox_searchNis.Font = New System.Drawing.Font("Segoe UI", 9.5!)
        Me.TextBox_searchNis.Location = New System.Drawing.Point(319, 23)
        Me.TextBox_searchNis.Name = "TextBox_searchNis"
        Me.TextBox_searchNis.Size = New System.Drawing.Size(50, 24)
        Me.TextBox_searchNis.TabIndex = 3
        Me.TextBox_searchNis.Visible = False
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        Me.Panel3.Controls.Add(Me.MetroTabControl1)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(0, 136)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(1050, 402)
        Me.Panel3.TabIndex = 2
        '
        'MetroTabControl1
        '
        Me.MetroTabControl1.Controls.Add(Me.MetroTabPage_mapelNA)
        Me.MetroTabControl1.Controls.Add(Me.MetroTabPage_mapelProd)
        Me.MetroTabControl1.Controls.Add(Me.MetroTabPage_supp)
        Me.MetroTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MetroTabControl1.FontWeight = MetroFramework.MetroTabControlWeight.Regular
        Me.MetroTabControl1.Location = New System.Drawing.Point(0, 0)
        Me.MetroTabControl1.Name = "MetroTabControl1"
        Me.MetroTabControl1.SelectedIndex = 0
        Me.MetroTabControl1.Size = New System.Drawing.Size(1050, 402)
        Me.MetroTabControl1.TabIndex = 0
        Me.MetroTabControl1.TabStop = False
        Me.MetroTabControl1.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MetroTabControl1.UseSelectable = True
        Me.MetroTabControl1.UseStyleColors = True
        '
        'MetroTabPage_mapelNA
        '
        Me.MetroTabPage_mapelNA.AutoScroll = True
        Me.MetroTabPage_mapelNA.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label_hurKetKim)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label_hurPengKim)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.TextBox_ketKim)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.TextBox_pengKim)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label1)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label39)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label2)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label40)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label3)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label41)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.TextBox_pengPai)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label_hurKetFis)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.TextBox_ketPai)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label_hurPengFis)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label_hurPengPai)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.TextBox_ketFis)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label_hurKetPai)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.TextBox_pengFis)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.TextBox_ketPkn)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label36)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label8)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label37)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label7)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label38)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label6)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label_hurKetMul)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.TextBox_pengPkn)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label_hurPengMul)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label_hurPengPkn)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.TextBox_ketMul)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label_hurKetPkn)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.TextBox_pengMul)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label18)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label24)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label17)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label25)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label16)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label29)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.TextBox_pengMtk)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label_hurKetKwh)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.TextBox_ketMtk)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label_hurPengKwh)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label_hurPengMtk)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.TextBox_ketKwh)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label_hurKetMtk)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.TextBox_pengKwh)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label13)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label15)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label12)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label19)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label11)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label20)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.TextBox_pengIndo)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label_hurKetPen)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.TextBox_ketIndo)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label_hurPengPen)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label_hurPengIndo)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.TextBox_ketPen)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label_hurKetIndo)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.TextBox_pengPen)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label23)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label9)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label22)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label10)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label21)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label14)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.TextBox_pengSej)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label_hurKetSbk)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.TextBox_ketSej)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label_hurPengSbk)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label_hurPengSej)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.TextBox_ketSbk)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label_hurKetSej)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.TextBox_pengSbk)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label28)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label31)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label27)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label32)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label26)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label33)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.TextBox_pengIng)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label_hurKetIng)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.TextBox_ketIng)
        Me.MetroTabPage_mapelNA.Controls.Add(Me.Label_hurPengIng)
        Me.MetroTabPage_mapelNA.ForeColor = System.Drawing.Color.Black
        Me.MetroTabPage_mapelNA.HorizontalScrollbar = True
        Me.MetroTabPage_mapelNA.HorizontalScrollbarBarColor = True
        Me.MetroTabPage_mapelNA.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroTabPage_mapelNA.HorizontalScrollbarSize = 10
        Me.MetroTabPage_mapelNA.Location = New System.Drawing.Point(4, 38)
        Me.MetroTabPage_mapelNA.Name = "MetroTabPage_mapelNA"
        Me.MetroTabPage_mapelNA.Size = New System.Drawing.Size(1042, 360)
        Me.MetroTabPage_mapelNA.TabIndex = 0
        Me.MetroTabPage_mapelNA.Text = "Nilai Mapel Normatif/Adatif"
        Me.MetroTabPage_mapelNA.VerticalScrollbar = True
        Me.MetroTabPage_mapelNA.VerticalScrollbarBarColor = True
        Me.MetroTabPage_mapelNA.VerticalScrollbarHighlightOnWheel = False
        Me.MetroTabPage_mapelNA.VerticalScrollbarSize = 10
        '
        'Label_hurKetKim
        '
        Me.Label_hurKetKim.AutoSize = True
        Me.Label_hurKetKim.ForeColor = System.Drawing.Color.Black
        Me.Label_hurKetKim.Location = New System.Drawing.Point(1094, 370)
        Me.Label_hurKetKim.Name = "Label_hurKetKim"
        Me.Label_hurKetKim.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurKetKim.TabIndex = 83
        Me.Label_hurKetKim.Visible = False
        '
        'Label_hurPengKim
        '
        Me.Label_hurPengKim.AutoSize = True
        Me.Label_hurPengKim.ForeColor = System.Drawing.Color.Black
        Me.Label_hurPengKim.Location = New System.Drawing.Point(1094, 331)
        Me.Label_hurPengKim.Name = "Label_hurPengKim"
        Me.Label_hurPengKim.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurPengKim.TabIndex = 82
        Me.Label_hurPengKim.Visible = False
        '
        'TextBox_ketKim
        '
        Me.TextBox_ketKim.ForeColor = System.Drawing.Color.Black
        Me.TextBox_ketKim.Location = New System.Drawing.Point(986, 367)
        Me.TextBox_ketKim.MaxLength = 3
        Me.TextBox_ketKim.Name = "TextBox_ketKim"
        Me.TextBox_ketKim.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_ketKim.TabIndex = 81
        Me.TextBox_ketKim.Visible = False
        '
        'TextBox_pengKim
        '
        Me.TextBox_pengKim.ForeColor = System.Drawing.Color.Black
        Me.TextBox_pengKim.Location = New System.Drawing.Point(986, 328)
        Me.TextBox_pengKim.MaxLength = 3
        Me.TextBox_pengKim.Name = "TextBox_pengKim"
        Me.TextBox_pengKim.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_pengKim.TabIndex = 80
        Me.TextBox_pengKim.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(19, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(179, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Pendidikan Agama Dan Budi Pekerti"
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.ForeColor = System.Drawing.Color.Black
        Me.Label39.Location = New System.Drawing.Point(882, 370)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(68, 13)
        Me.Label39.TabIndex = 79
        Me.Label39.Text = "Keterampilan"
        Me.Label39.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(19, 63)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(71, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Pengetahuan"
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.ForeColor = System.Drawing.Color.Black
        Me.Label40.Location = New System.Drawing.Point(882, 331)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(71, 13)
        Me.Label40.TabIndex = 78
        Me.Label40.Text = "Pengetahuan"
        Me.Label40.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(19, 102)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(68, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Keterampilan"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.ForeColor = System.Drawing.Color.Black
        Me.Label41.Location = New System.Drawing.Point(882, 295)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(32, 13)
        Me.Label41.TabIndex = 77
        Me.Label41.Text = "Kimia"
        Me.Label41.Visible = False
        '
        'TextBox_pengPai
        '
        Me.TextBox_pengPai.Location = New System.Drawing.Point(123, 60)
        Me.TextBox_pengPai.MaxLength = 3
        Me.TextBox_pengPai.Name = "TextBox_pengPai"
        Me.TextBox_pengPai.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_pengPai.TabIndex = 3
        '
        'Label_hurKetFis
        '
        Me.Label_hurKetFis.AutoSize = True
        Me.Label_hurKetFis.ForeColor = System.Drawing.Color.Black
        Me.Label_hurKetFis.Location = New System.Drawing.Point(1094, 238)
        Me.Label_hurKetFis.Name = "Label_hurKetFis"
        Me.Label_hurKetFis.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurKetFis.TabIndex = 76
        Me.Label_hurKetFis.Visible = False
        '
        'TextBox_ketPai
        '
        Me.TextBox_ketPai.Location = New System.Drawing.Point(123, 99)
        Me.TextBox_ketPai.MaxLength = 3
        Me.TextBox_ketPai.Name = "TextBox_ketPai"
        Me.TextBox_ketPai.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_ketPai.TabIndex = 4
        '
        'Label_hurPengFis
        '
        Me.Label_hurPengFis.AutoSize = True
        Me.Label_hurPengFis.ForeColor = System.Drawing.Color.Black
        Me.Label_hurPengFis.Location = New System.Drawing.Point(1094, 199)
        Me.Label_hurPengFis.Name = "Label_hurPengFis"
        Me.Label_hurPengFis.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurPengFis.TabIndex = 75
        Me.Label_hurPengFis.Visible = False
        '
        'Label_hurPengPai
        '
        Me.Label_hurPengPai.AutoSize = True
        Me.Label_hurPengPai.ForeColor = System.Drawing.Color.Black
        Me.Label_hurPengPai.Location = New System.Drawing.Point(231, 63)
        Me.Label_hurPengPai.Name = "Label_hurPengPai"
        Me.Label_hurPengPai.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurPengPai.TabIndex = 5
        '
        'TextBox_ketFis
        '
        Me.TextBox_ketFis.ForeColor = System.Drawing.Color.Black
        Me.TextBox_ketFis.Location = New System.Drawing.Point(986, 235)
        Me.TextBox_ketFis.MaxLength = 3
        Me.TextBox_ketFis.Name = "TextBox_ketFis"
        Me.TextBox_ketFis.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_ketFis.TabIndex = 74
        Me.TextBox_ketFis.Visible = False
        '
        'Label_hurKetPai
        '
        Me.Label_hurKetPai.AutoSize = True
        Me.Label_hurKetPai.ForeColor = System.Drawing.Color.Black
        Me.Label_hurKetPai.Location = New System.Drawing.Point(231, 102)
        Me.Label_hurKetPai.Name = "Label_hurKetPai"
        Me.Label_hurKetPai.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurKetPai.TabIndex = 6
        '
        'TextBox_pengFis
        '
        Me.TextBox_pengFis.ForeColor = System.Drawing.Color.Black
        Me.TextBox_pengFis.Location = New System.Drawing.Point(986, 196)
        Me.TextBox_pengFis.MaxLength = 3
        Me.TextBox_pengFis.Name = "TextBox_pengFis"
        Me.TextBox_pengFis.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_pengFis.TabIndex = 73
        Me.TextBox_pengFis.Visible = False
        '
        'TextBox_ketPkn
        '
        Me.TextBox_ketPkn.Location = New System.Drawing.Point(123, 229)
        Me.TextBox_ketPkn.MaxLength = 3
        Me.TextBox_ketPkn.Name = "TextBox_ketPkn"
        Me.TextBox_ketPkn.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_ketPkn.TabIndex = 11
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.ForeColor = System.Drawing.Color.Black
        Me.Label36.Location = New System.Drawing.Point(882, 238)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(68, 13)
        Me.Label36.TabIndex = 72
        Me.Label36.Text = "Keterampilan"
        Me.Label36.Visible = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ForeColor = System.Drawing.Color.Black
        Me.Label8.Location = New System.Drawing.Point(19, 154)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(34, 13)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "PPKn"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.ForeColor = System.Drawing.Color.Black
        Me.Label37.Location = New System.Drawing.Point(882, 199)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(71, 13)
        Me.Label37.TabIndex = 71
        Me.Label37.Text = "Pengetahuan"
        Me.Label37.Visible = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ForeColor = System.Drawing.Color.Black
        Me.Label7.Location = New System.Drawing.Point(19, 193)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(71, 13)
        Me.Label7.TabIndex = 8
        Me.Label7.Text = "Pengetahuan"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.ForeColor = System.Drawing.Color.Black
        Me.Label38.Location = New System.Drawing.Point(882, 160)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(34, 13)
        Me.Label38.TabIndex = 70
        Me.Label38.Text = "Fisika"
        Me.Label38.Visible = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(19, 232)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(68, 13)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "Keterampilan"
        '
        'Label_hurKetMul
        '
        Me.Label_hurKetMul.AutoSize = True
        Me.Label_hurKetMul.ForeColor = System.Drawing.Color.Black
        Me.Label_hurKetMul.Location = New System.Drawing.Point(1094, 93)
        Me.Label_hurKetMul.Name = "Label_hurKetMul"
        Me.Label_hurKetMul.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurKetMul.TabIndex = 69
        '
        'TextBox_pengPkn
        '
        Me.TextBox_pengPkn.Location = New System.Drawing.Point(123, 190)
        Me.TextBox_pengPkn.MaxLength = 3
        Me.TextBox_pengPkn.Name = "TextBox_pengPkn"
        Me.TextBox_pengPkn.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_pengPkn.TabIndex = 10
        '
        'Label_hurPengMul
        '
        Me.Label_hurPengMul.AutoSize = True
        Me.Label_hurPengMul.ForeColor = System.Drawing.Color.Black
        Me.Label_hurPengMul.Location = New System.Drawing.Point(1094, 54)
        Me.Label_hurPengMul.Name = "Label_hurPengMul"
        Me.Label_hurPengMul.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurPengMul.TabIndex = 68
        '
        'Label_hurPengPkn
        '
        Me.Label_hurPengPkn.AutoSize = True
        Me.Label_hurPengPkn.ForeColor = System.Drawing.Color.Black
        Me.Label_hurPengPkn.Location = New System.Drawing.Point(231, 193)
        Me.Label_hurPengPkn.Name = "Label_hurPengPkn"
        Me.Label_hurPengPkn.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurPengPkn.TabIndex = 12
        '
        'TextBox_ketMul
        '
        Me.TextBox_ketMul.ForeColor = System.Drawing.Color.Black
        Me.TextBox_ketMul.Location = New System.Drawing.Point(986, 90)
        Me.TextBox_ketMul.MaxLength = 3
        Me.TextBox_ketMul.Name = "TextBox_ketMul"
        Me.TextBox_ketMul.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_ketMul.TabIndex = 67
        '
        'Label_hurKetPkn
        '
        Me.Label_hurKetPkn.AutoSize = True
        Me.Label_hurKetPkn.ForeColor = System.Drawing.Color.Black
        Me.Label_hurKetPkn.Location = New System.Drawing.Point(231, 232)
        Me.Label_hurKetPkn.Name = "Label_hurKetPkn"
        Me.Label_hurKetPkn.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurKetPkn.TabIndex = 13
        '
        'TextBox_pengMul
        '
        Me.TextBox_pengMul.ForeColor = System.Drawing.Color.Black
        Me.TextBox_pengMul.Location = New System.Drawing.Point(986, 51)
        Me.TextBox_pengMul.MaxLength = 3
        Me.TextBox_pengMul.Name = "TextBox_pengMul"
        Me.TextBox_pengMul.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_pengMul.TabIndex = 66
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.ForeColor = System.Drawing.Color.Black
        Me.Label18.Location = New System.Drawing.Point(303, 24)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(62, 13)
        Me.Label18.TabIndex = 21
        Me.Label18.Text = "Matematika"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.ForeColor = System.Drawing.Color.Black
        Me.Label24.Location = New System.Drawing.Point(882, 93)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(68, 13)
        Me.Label24.TabIndex = 65
        Me.Label24.Text = "Keterampilan"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.ForeColor = System.Drawing.Color.Black
        Me.Label17.Location = New System.Drawing.Point(303, 63)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(71, 13)
        Me.Label17.TabIndex = 22
        Me.Label17.Text = "Pengetahuan"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.ForeColor = System.Drawing.Color.Black
        Me.Label25.Location = New System.Drawing.Point(882, 54)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(71, 13)
        Me.Label25.TabIndex = 64
        Me.Label25.Text = "Pengetahuan"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.ForeColor = System.Drawing.Color.Black
        Me.Label16.Location = New System.Drawing.Point(303, 102)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(68, 13)
        Me.Label16.TabIndex = 23
        Me.Label16.Text = "Keterampilan"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.ForeColor = System.Drawing.Color.Black
        Me.Label29.Location = New System.Drawing.Point(882, 15)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(36, 13)
        Me.Label29.TabIndex = 63
        Me.Label29.Text = "Mulok"
        '
        'TextBox_pengMtk
        '
        Me.TextBox_pengMtk.Location = New System.Drawing.Point(407, 60)
        Me.TextBox_pengMtk.MaxLength = 3
        Me.TextBox_pengMtk.Name = "TextBox_pengMtk"
        Me.TextBox_pengMtk.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_pengMtk.TabIndex = 24
        '
        'Label_hurKetKwh
        '
        Me.Label_hurKetKwh.AutoSize = True
        Me.Label_hurKetKwh.ForeColor = System.Drawing.Color.Black
        Me.Label_hurKetKwh.Location = New System.Drawing.Point(806, 370)
        Me.Label_hurKetKwh.Name = "Label_hurKetKwh"
        Me.Label_hurKetKwh.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurKetKwh.TabIndex = 62
        '
        'TextBox_ketMtk
        '
        Me.TextBox_ketMtk.Location = New System.Drawing.Point(407, 99)
        Me.TextBox_ketMtk.MaxLength = 3
        Me.TextBox_ketMtk.Name = "TextBox_ketMtk"
        Me.TextBox_ketMtk.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_ketMtk.TabIndex = 25
        '
        'Label_hurPengKwh
        '
        Me.Label_hurPengKwh.AutoSize = True
        Me.Label_hurPengKwh.ForeColor = System.Drawing.Color.Black
        Me.Label_hurPengKwh.Location = New System.Drawing.Point(806, 331)
        Me.Label_hurPengKwh.Name = "Label_hurPengKwh"
        Me.Label_hurPengKwh.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurPengKwh.TabIndex = 61
        '
        'Label_hurPengMtk
        '
        Me.Label_hurPengMtk.AutoSize = True
        Me.Label_hurPengMtk.ForeColor = System.Drawing.Color.Black
        Me.Label_hurPengMtk.Location = New System.Drawing.Point(515, 63)
        Me.Label_hurPengMtk.Name = "Label_hurPengMtk"
        Me.Label_hurPengMtk.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurPengMtk.TabIndex = 26
        '
        'TextBox_ketKwh
        '
        Me.TextBox_ketKwh.ForeColor = System.Drawing.Color.Black
        Me.TextBox_ketKwh.Location = New System.Drawing.Point(698, 367)
        Me.TextBox_ketKwh.MaxLength = 3
        Me.TextBox_ketKwh.Name = "TextBox_ketKwh"
        Me.TextBox_ketKwh.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_ketKwh.TabIndex = 60
        '
        'Label_hurKetMtk
        '
        Me.Label_hurKetMtk.AutoSize = True
        Me.Label_hurKetMtk.ForeColor = System.Drawing.Color.Black
        Me.Label_hurKetMtk.Location = New System.Drawing.Point(515, 102)
        Me.Label_hurKetMtk.Name = "Label_hurKetMtk"
        Me.Label_hurKetMtk.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurKetMtk.TabIndex = 27
        '
        'TextBox_pengKwh
        '
        Me.TextBox_pengKwh.ForeColor = System.Drawing.Color.Black
        Me.TextBox_pengKwh.Location = New System.Drawing.Point(698, 328)
        Me.TextBox_pengKwh.MaxLength = 3
        Me.TextBox_pengKwh.Name = "TextBox_pengKwh"
        Me.TextBox_pengKwh.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_pengKwh.TabIndex = 59
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ForeColor = System.Drawing.Color.Black
        Me.Label13.Location = New System.Drawing.Point(19, 289)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(92, 13)
        Me.Label13.TabIndex = 14
        Me.Label13.Text = "Bahasa Indonesia"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.ForeColor = System.Drawing.Color.Black
        Me.Label15.Location = New System.Drawing.Point(594, 370)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(68, 13)
        Me.Label15.TabIndex = 58
        Me.Label15.Text = "Keterampilan"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.ForeColor = System.Drawing.Color.Black
        Me.Label12.Location = New System.Drawing.Point(19, 328)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(71, 13)
        Me.Label12.TabIndex = 15
        Me.Label12.Text = "Pengetahuan"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.ForeColor = System.Drawing.Color.Black
        Me.Label19.Location = New System.Drawing.Point(594, 331)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(71, 13)
        Me.Label19.TabIndex = 57
        Me.Label19.Text = "Pengetahuan"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.ForeColor = System.Drawing.Color.Black
        Me.Label11.Location = New System.Drawing.Point(19, 367)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(68, 13)
        Me.Label11.TabIndex = 16
        Me.Label11.Text = "Keterampilan"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.ForeColor = System.Drawing.Color.Black
        Me.Label20.Location = New System.Drawing.Point(594, 295)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(126, 13)
        Me.Label20.TabIndex = 56
        Me.Label20.Text = "Prakarya Dan Wirausaha"
        '
        'TextBox_pengIndo
        '
        Me.TextBox_pengIndo.ForeColor = System.Drawing.Color.Black
        Me.TextBox_pengIndo.Location = New System.Drawing.Point(123, 325)
        Me.TextBox_pengIndo.MaxLength = 3
        Me.TextBox_pengIndo.Name = "TextBox_pengIndo"
        Me.TextBox_pengIndo.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_pengIndo.TabIndex = 17
        '
        'Label_hurKetPen
        '
        Me.Label_hurKetPen.AutoSize = True
        Me.Label_hurKetPen.ForeColor = System.Drawing.Color.Black
        Me.Label_hurKetPen.Location = New System.Drawing.Point(806, 238)
        Me.Label_hurKetPen.Name = "Label_hurKetPen"
        Me.Label_hurKetPen.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurKetPen.TabIndex = 55
        '
        'TextBox_ketIndo
        '
        Me.TextBox_ketIndo.ForeColor = System.Drawing.Color.Black
        Me.TextBox_ketIndo.Location = New System.Drawing.Point(123, 364)
        Me.TextBox_ketIndo.MaxLength = 3
        Me.TextBox_ketIndo.Name = "TextBox_ketIndo"
        Me.TextBox_ketIndo.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_ketIndo.TabIndex = 18
        '
        'Label_hurPengPen
        '
        Me.Label_hurPengPen.AutoSize = True
        Me.Label_hurPengPen.ForeColor = System.Drawing.Color.Black
        Me.Label_hurPengPen.Location = New System.Drawing.Point(806, 199)
        Me.Label_hurPengPen.Name = "Label_hurPengPen"
        Me.Label_hurPengPen.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurPengPen.TabIndex = 54
        '
        'Label_hurPengIndo
        '
        Me.Label_hurPengIndo.AutoSize = True
        Me.Label_hurPengIndo.ForeColor = System.Drawing.Color.Black
        Me.Label_hurPengIndo.Location = New System.Drawing.Point(231, 328)
        Me.Label_hurPengIndo.Name = "Label_hurPengIndo"
        Me.Label_hurPengIndo.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurPengIndo.TabIndex = 19
        '
        'TextBox_ketPen
        '
        Me.TextBox_ketPen.ForeColor = System.Drawing.Color.Black
        Me.TextBox_ketPen.Location = New System.Drawing.Point(698, 235)
        Me.TextBox_ketPen.MaxLength = 3
        Me.TextBox_ketPen.Name = "TextBox_ketPen"
        Me.TextBox_ketPen.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_ketPen.TabIndex = 53
        '
        'Label_hurKetIndo
        '
        Me.Label_hurKetIndo.AutoSize = True
        Me.Label_hurKetIndo.ForeColor = System.Drawing.Color.Black
        Me.Label_hurKetIndo.Location = New System.Drawing.Point(231, 367)
        Me.Label_hurKetIndo.Name = "Label_hurKetIndo"
        Me.Label_hurKetIndo.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurKetIndo.TabIndex = 20
        '
        'TextBox_pengPen
        '
        Me.TextBox_pengPen.ForeColor = System.Drawing.Color.Black
        Me.TextBox_pengPen.Location = New System.Drawing.Point(698, 196)
        Me.TextBox_pengPen.MaxLength = 3
        Me.TextBox_pengPen.Name = "TextBox_pengPen"
        Me.TextBox_pengPen.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_pengPen.TabIndex = 52
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.ForeColor = System.Drawing.Color.Black
        Me.Label23.Location = New System.Drawing.Point(303, 157)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(92, 13)
        Me.Label23.TabIndex = 28
        Me.Label23.Text = "Sejarah Indonesia"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.ForeColor = System.Drawing.Color.Black
        Me.Label9.Location = New System.Drawing.Point(594, 238)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(68, 13)
        Me.Label9.TabIndex = 51
        Me.Label9.Text = "Keterampilan"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.ForeColor = System.Drawing.Color.Black
        Me.Label22.Location = New System.Drawing.Point(303, 196)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(71, 13)
        Me.Label22.TabIndex = 29
        Me.Label22.Text = "Pengetahuan"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ForeColor = System.Drawing.Color.Black
        Me.Label10.Location = New System.Drawing.Point(594, 199)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(71, 13)
        Me.Label10.TabIndex = 50
        Me.Label10.Text = "Pengetahuan"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.ForeColor = System.Drawing.Color.Black
        Me.Label21.Location = New System.Drawing.Point(303, 235)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(68, 13)
        Me.Label21.TabIndex = 30
        Me.Label21.Text = "Keterampilan"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.ForeColor = System.Drawing.Color.Black
        Me.Label14.Location = New System.Drawing.Point(594, 160)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(65, 13)
        Me.Label14.TabIndex = 49
        Me.Label14.Text = "Penjasorkes"
        '
        'TextBox_pengSej
        '
        Me.TextBox_pengSej.ForeColor = System.Drawing.Color.Black
        Me.TextBox_pengSej.Location = New System.Drawing.Point(407, 193)
        Me.TextBox_pengSej.MaxLength = 3
        Me.TextBox_pengSej.Name = "TextBox_pengSej"
        Me.TextBox_pengSej.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_pengSej.TabIndex = 31
        '
        'Label_hurKetSbk
        '
        Me.Label_hurKetSbk.AutoSize = True
        Me.Label_hurKetSbk.ForeColor = System.Drawing.Color.Black
        Me.Label_hurKetSbk.Location = New System.Drawing.Point(806, 102)
        Me.Label_hurKetSbk.Name = "Label_hurKetSbk"
        Me.Label_hurKetSbk.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurKetSbk.TabIndex = 48
        '
        'TextBox_ketSej
        '
        Me.TextBox_ketSej.ForeColor = System.Drawing.Color.Black
        Me.TextBox_ketSej.Location = New System.Drawing.Point(407, 232)
        Me.TextBox_ketSej.MaxLength = 3
        Me.TextBox_ketSej.Name = "TextBox_ketSej"
        Me.TextBox_ketSej.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_ketSej.TabIndex = 32
        '
        'Label_hurPengSbk
        '
        Me.Label_hurPengSbk.AutoSize = True
        Me.Label_hurPengSbk.ForeColor = System.Drawing.Color.Black
        Me.Label_hurPengSbk.Location = New System.Drawing.Point(806, 63)
        Me.Label_hurPengSbk.Name = "Label_hurPengSbk"
        Me.Label_hurPengSbk.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurPengSbk.TabIndex = 47
        '
        'Label_hurPengSej
        '
        Me.Label_hurPengSej.AutoSize = True
        Me.Label_hurPengSej.ForeColor = System.Drawing.Color.Black
        Me.Label_hurPengSej.Location = New System.Drawing.Point(515, 196)
        Me.Label_hurPengSej.Name = "Label_hurPengSej"
        Me.Label_hurPengSej.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurPengSej.TabIndex = 33
        '
        'TextBox_ketSbk
        '
        Me.TextBox_ketSbk.ForeColor = System.Drawing.Color.Black
        Me.TextBox_ketSbk.Location = New System.Drawing.Point(698, 99)
        Me.TextBox_ketSbk.MaxLength = 3
        Me.TextBox_ketSbk.Name = "TextBox_ketSbk"
        Me.TextBox_ketSbk.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_ketSbk.TabIndex = 46
        '
        'Label_hurKetSej
        '
        Me.Label_hurKetSej.AutoSize = True
        Me.Label_hurKetSej.ForeColor = System.Drawing.Color.Black
        Me.Label_hurKetSej.Location = New System.Drawing.Point(515, 235)
        Me.Label_hurKetSej.Name = "Label_hurKetSej"
        Me.Label_hurKetSej.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurKetSej.TabIndex = 34
        '
        'TextBox_pengSbk
        '
        Me.TextBox_pengSbk.ForeColor = System.Drawing.Color.Black
        Me.TextBox_pengSbk.Location = New System.Drawing.Point(698, 60)
        Me.TextBox_pengSbk.MaxLength = 3
        Me.TextBox_pengSbk.Name = "TextBox_pengSbk"
        Me.TextBox_pengSbk.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_pengSbk.TabIndex = 45
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.ForeColor = System.Drawing.Color.Black
        Me.Label28.Location = New System.Drawing.Point(304, 292)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(77, 13)
        Me.Label28.TabIndex = 35
        Me.Label28.Text = "Bahasa Inggris"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.ForeColor = System.Drawing.Color.Black
        Me.Label31.Location = New System.Drawing.Point(594, 102)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(68, 13)
        Me.Label31.TabIndex = 44
        Me.Label31.Text = "Keterampilan"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.ForeColor = System.Drawing.Color.Black
        Me.Label27.Location = New System.Drawing.Point(304, 331)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(71, 13)
        Me.Label27.TabIndex = 36
        Me.Label27.Text = "Pengetahuan"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.ForeColor = System.Drawing.Color.Black
        Me.Label32.Location = New System.Drawing.Point(594, 63)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(71, 13)
        Me.Label32.TabIndex = 43
        Me.Label32.Text = "Pengetahuan"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.ForeColor = System.Drawing.Color.Black
        Me.Label26.Location = New System.Drawing.Point(304, 370)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(68, 13)
        Me.Label26.TabIndex = 37
        Me.Label26.Text = "Keterampilan"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.ForeColor = System.Drawing.Color.Black
        Me.Label33.Location = New System.Drawing.Point(594, 24)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(67, 13)
        Me.Label33.TabIndex = 42
        Me.Label33.Text = "Seni Budaya"
        '
        'TextBox_pengIng
        '
        Me.TextBox_pengIng.ForeColor = System.Drawing.Color.Black
        Me.TextBox_pengIng.Location = New System.Drawing.Point(408, 328)
        Me.TextBox_pengIng.MaxLength = 3
        Me.TextBox_pengIng.Name = "TextBox_pengIng"
        Me.TextBox_pengIng.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_pengIng.TabIndex = 38
        '
        'Label_hurKetIng
        '
        Me.Label_hurKetIng.AutoSize = True
        Me.Label_hurKetIng.ForeColor = System.Drawing.Color.Black
        Me.Label_hurKetIng.Location = New System.Drawing.Point(516, 370)
        Me.Label_hurKetIng.Name = "Label_hurKetIng"
        Me.Label_hurKetIng.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurKetIng.TabIndex = 41
        '
        'TextBox_ketIng
        '
        Me.TextBox_ketIng.ForeColor = System.Drawing.Color.Black
        Me.TextBox_ketIng.Location = New System.Drawing.Point(408, 367)
        Me.TextBox_ketIng.MaxLength = 3
        Me.TextBox_ketIng.Name = "TextBox_ketIng"
        Me.TextBox_ketIng.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_ketIng.TabIndex = 39
        '
        'Label_hurPengIng
        '
        Me.Label_hurPengIng.AutoSize = True
        Me.Label_hurPengIng.ForeColor = System.Drawing.Color.Black
        Me.Label_hurPengIng.Location = New System.Drawing.Point(516, 331)
        Me.Label_hurPengIng.Name = "Label_hurPengIng"
        Me.Label_hurPengIng.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurPengIng.TabIndex = 40
        '
        'MetroTabPage_mapelProd
        '
        Me.MetroTabPage_mapelProd.AutoScroll = True
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label_hurKetProd9)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label_hurKetProd11)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label_hurPengProd9)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.TextBox_ketProd9)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label_hurPengProd11)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.TextBox_pengProd9)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label77)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.TextBox_ketProd11)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label78)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label79)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.TextBox_pengProd11)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label_hurKetProd6)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label55)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label_hurPengProd6)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label83)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.TextBox_ketProd6)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label54)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.TextBox_pengProd6)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label84)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label70)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label53)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label71)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label85)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label72)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.TextBox_pengProd1)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label_hurKetProd3)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label_hurKetProd10)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label_hurPengProd3)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.TextBox_ketProd1)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.TextBox_ketProd3)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label_hurPengProd10)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.TextBox_pengProd3)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label_hurPengProd1)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label59)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.TextBox_ketProd10)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label60)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label_hurKetProd1)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label61)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.TextBox_pengProd10)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label58)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label80)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label57)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label81)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label56)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label82)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.TextBox_pengProd2)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.TextBox_ketProd2)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label_hurPengProd2)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label_hurKetProd2)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label64)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label63)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label62)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.TextBox_pengProd4)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label_hurKetProd8)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.TextBox_ketProd4)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label_hurPengProd8)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label_hurPengProd4)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.TextBox_ketProd8)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label_hurKetProd4)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.TextBox_pengProd8)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label67)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label74)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label66)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label75)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label65)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label76)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.TextBox_pengProd5)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label_hurKetProd7)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.TextBox_ketProd5)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label_hurPengProd7)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label_hurPengProd5)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.TextBox_ketProd7)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label_hurKetProd5)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.TextBox_pengProd7)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label73)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label68)
        Me.MetroTabPage_mapelProd.Controls.Add(Me.Label69)
        Me.MetroTabPage_mapelProd.HorizontalScrollbar = True
        Me.MetroTabPage_mapelProd.HorizontalScrollbarBarColor = True
        Me.MetroTabPage_mapelProd.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroTabPage_mapelProd.HorizontalScrollbarSize = 10
        Me.MetroTabPage_mapelProd.Location = New System.Drawing.Point(4, 38)
        Me.MetroTabPage_mapelProd.Name = "MetroTabPage_mapelProd"
        Me.MetroTabPage_mapelProd.Size = New System.Drawing.Size(1042, 360)
        Me.MetroTabPage_mapelProd.TabIndex = 1
        Me.MetroTabPage_mapelProd.Text = "Nilai Mapel Produktif"
        Me.MetroTabPage_mapelProd.VerticalScrollbar = True
        Me.MetroTabPage_mapelProd.VerticalScrollbarBarColor = True
        Me.MetroTabPage_mapelProd.VerticalScrollbarHighlightOnWheel = False
        Me.MetroTabPage_mapelProd.VerticalScrollbarSize = 10
        '
        'Label_hurKetProd9
        '
        Me.Label_hurKetProd9.AutoSize = True
        Me.Label_hurKetProd9.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label_hurKetProd9.ForeColor = System.Drawing.Color.Black
        Me.Label_hurKetProd9.Location = New System.Drawing.Point(898, 385)
        Me.Label_hurKetProd9.Name = "Label_hurKetProd9"
        Me.Label_hurKetProd9.Size = New System.Drawing.Size(37, 13)
        Me.Label_hurKetProd9.TabIndex = 69
        Me.Label_hurKetProd9.Text = "-huruf-"
        Me.Label_hurKetProd9.Visible = False
        '
        'Label_hurKetProd11
        '
        Me.Label_hurKetProd11.AutoSize = True
        Me.Label_hurKetProd11.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label_hurKetProd11.ForeColor = System.Drawing.Color.Black
        Me.Label_hurKetProd11.Location = New System.Drawing.Point(1269, 241)
        Me.Label_hurKetProd11.Name = "Label_hurKetProd11"
        Me.Label_hurKetProd11.Size = New System.Drawing.Size(37, 13)
        Me.Label_hurKetProd11.TabIndex = 83
        Me.Label_hurKetProd11.Text = "-huruf-"
        Me.Label_hurKetProd11.Visible = False
        '
        'Label_hurPengProd9
        '
        Me.Label_hurPengProd9.AutoSize = True
        Me.Label_hurPengProd9.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label_hurPengProd9.ForeColor = System.Drawing.Color.Black
        Me.Label_hurPengProd9.Location = New System.Drawing.Point(898, 346)
        Me.Label_hurPengProd9.Name = "Label_hurPengProd9"
        Me.Label_hurPengProd9.Size = New System.Drawing.Size(37, 13)
        Me.Label_hurPengProd9.TabIndex = 68
        Me.Label_hurPengProd9.Text = "-huruf-"
        Me.Label_hurPengProd9.Visible = False
        '
        'TextBox_ketProd9
        '
        Me.TextBox_ketProd9.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.TextBox_ketProd9.ForeColor = System.Drawing.Color.Black
        Me.TextBox_ketProd9.Location = New System.Drawing.Point(790, 382)
        Me.TextBox_ketProd9.Name = "TextBox_ketProd9"
        Me.TextBox_ketProd9.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_ketProd9.TabIndex = 67
        Me.TextBox_ketProd9.Visible = False
        '
        'Label_hurPengProd11
        '
        Me.Label_hurPengProd11.AutoSize = True
        Me.Label_hurPengProd11.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label_hurPengProd11.ForeColor = System.Drawing.Color.Black
        Me.Label_hurPengProd11.Location = New System.Drawing.Point(1269, 202)
        Me.Label_hurPengProd11.Name = "Label_hurPengProd11"
        Me.Label_hurPengProd11.Size = New System.Drawing.Size(37, 13)
        Me.Label_hurPengProd11.TabIndex = 82
        Me.Label_hurPengProd11.Text = "-huruf-"
        Me.Label_hurPengProd11.Visible = False
        '
        'TextBox_pengProd9
        '
        Me.TextBox_pengProd9.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.TextBox_pengProd9.ForeColor = System.Drawing.Color.Black
        Me.TextBox_pengProd9.Location = New System.Drawing.Point(790, 343)
        Me.TextBox_pengProd9.Name = "TextBox_pengProd9"
        Me.TextBox_pengProd9.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_pengProd9.TabIndex = 66
        Me.TextBox_pengProd9.Visible = False
        '
        'Label77
        '
        Me.Label77.AutoSize = True
        Me.Label77.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label77.ForeColor = System.Drawing.Color.Black
        Me.Label77.Location = New System.Drawing.Point(686, 385)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(68, 13)
        Me.Label77.TabIndex = 65
        Me.Label77.Text = "Keterampilan"
        Me.Label77.Visible = False
        '
        'TextBox_ketProd11
        '
        Me.TextBox_ketProd11.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.TextBox_ketProd11.ForeColor = System.Drawing.Color.Black
        Me.TextBox_ketProd11.Location = New System.Drawing.Point(1161, 238)
        Me.TextBox_ketProd11.Name = "TextBox_ketProd11"
        Me.TextBox_ketProd11.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_ketProd11.TabIndex = 81
        Me.TextBox_ketProd11.Visible = False
        '
        'Label78
        '
        Me.Label78.AutoSize = True
        Me.Label78.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label78.ForeColor = System.Drawing.Color.Black
        Me.Label78.Location = New System.Drawing.Point(686, 346)
        Me.Label78.Name = "Label78"
        Me.Label78.Size = New System.Drawing.Size(71, 13)
        Me.Label78.TabIndex = 64
        Me.Label78.Text = "Pengetahuan"
        Me.Label78.Visible = False
        '
        'Label79
        '
        Me.Label79.AutoSize = True
        Me.Label79.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label79.ForeColor = System.Drawing.Color.Black
        Me.Label79.Location = New System.Drawing.Point(686, 307)
        Me.Label79.Name = "Label79"
        Me.Label79.Size = New System.Drawing.Size(35, 13)
        Me.Label79.TabIndex = 63
        Me.Label79.Text = "Prod9"
        Me.Label79.Visible = False
        '
        'TextBox_pengProd11
        '
        Me.TextBox_pengProd11.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.TextBox_pengProd11.ForeColor = System.Drawing.Color.Black
        Me.TextBox_pengProd11.Location = New System.Drawing.Point(1161, 199)
        Me.TextBox_pengProd11.Name = "TextBox_pengProd11"
        Me.TextBox_pengProd11.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_pengProd11.TabIndex = 80
        Me.TextBox_pengProd11.Visible = False
        '
        'Label_hurKetProd6
        '
        Me.Label_hurKetProd6.AutoSize = True
        Me.Label_hurKetProd6.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label_hurKetProd6.ForeColor = System.Drawing.Color.Black
        Me.Label_hurKetProd6.Location = New System.Drawing.Point(559, 385)
        Me.Label_hurKetProd6.Name = "Label_hurKetProd6"
        Me.Label_hurKetProd6.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurKetProd6.TabIndex = 48
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label55.ForeColor = System.Drawing.Color.Black
        Me.Label55.Location = New System.Drawing.Point(20, 17)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(35, 13)
        Me.Label55.TabIndex = 7
        Me.Label55.Text = "Prod1"
        '
        'Label_hurPengProd6
        '
        Me.Label_hurPengProd6.AutoSize = True
        Me.Label_hurPengProd6.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label_hurPengProd6.ForeColor = System.Drawing.Color.Black
        Me.Label_hurPengProd6.Location = New System.Drawing.Point(559, 346)
        Me.Label_hurPengProd6.Name = "Label_hurPengProd6"
        Me.Label_hurPengProd6.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurPengProd6.TabIndex = 47
        '
        'Label83
        '
        Me.Label83.AutoSize = True
        Me.Label83.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label83.ForeColor = System.Drawing.Color.Black
        Me.Label83.Location = New System.Drawing.Point(1057, 241)
        Me.Label83.Name = "Label83"
        Me.Label83.Size = New System.Drawing.Size(68, 13)
        Me.Label83.TabIndex = 79
        Me.Label83.Text = "Keterampilan"
        Me.Label83.Visible = False
        '
        'TextBox_ketProd6
        '
        Me.TextBox_ketProd6.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.TextBox_ketProd6.ForeColor = System.Drawing.Color.Black
        Me.TextBox_ketProd6.Location = New System.Drawing.Point(451, 382)
        Me.TextBox_ketProd6.MaxLength = 3
        Me.TextBox_ketProd6.Name = "TextBox_ketProd6"
        Me.TextBox_ketProd6.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_ketProd6.TabIndex = 46
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label54.ForeColor = System.Drawing.Color.Black
        Me.Label54.Location = New System.Drawing.Point(20, 56)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(71, 13)
        Me.Label54.TabIndex = 8
        Me.Label54.Text = "Pengetahuan"
        '
        'TextBox_pengProd6
        '
        Me.TextBox_pengProd6.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.TextBox_pengProd6.ForeColor = System.Drawing.Color.Black
        Me.TextBox_pengProd6.Location = New System.Drawing.Point(451, 343)
        Me.TextBox_pengProd6.MaxLength = 3
        Me.TextBox_pengProd6.Name = "TextBox_pengProd6"
        Me.TextBox_pengProd6.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_pengProd6.TabIndex = 45
        '
        'Label84
        '
        Me.Label84.AutoSize = True
        Me.Label84.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label84.ForeColor = System.Drawing.Color.Black
        Me.Label84.Location = New System.Drawing.Point(1057, 202)
        Me.Label84.Name = "Label84"
        Me.Label84.Size = New System.Drawing.Size(71, 13)
        Me.Label84.TabIndex = 78
        Me.Label84.Text = "Pengetahuan"
        Me.Label84.Visible = False
        '
        'Label70
        '
        Me.Label70.AutoSize = True
        Me.Label70.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label70.ForeColor = System.Drawing.Color.Black
        Me.Label70.Location = New System.Drawing.Point(347, 385)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(68, 13)
        Me.Label70.TabIndex = 44
        Me.Label70.Text = "Keterampilan"
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label53.ForeColor = System.Drawing.Color.Black
        Me.Label53.Location = New System.Drawing.Point(20, 95)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(68, 13)
        Me.Label53.TabIndex = 9
        Me.Label53.Text = "Keterampilan"
        '
        'Label71
        '
        Me.Label71.AutoSize = True
        Me.Label71.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label71.ForeColor = System.Drawing.Color.Black
        Me.Label71.Location = New System.Drawing.Point(347, 346)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(71, 13)
        Me.Label71.TabIndex = 43
        Me.Label71.Text = "Pengetahuan"
        '
        'Label85
        '
        Me.Label85.AutoSize = True
        Me.Label85.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label85.ForeColor = System.Drawing.Color.Black
        Me.Label85.Location = New System.Drawing.Point(1057, 163)
        Me.Label85.Name = "Label85"
        Me.Label85.Size = New System.Drawing.Size(41, 13)
        Me.Label85.TabIndex = 77
        Me.Label85.Text = "Prod11"
        Me.Label85.Visible = False
        '
        'Label72
        '
        Me.Label72.AutoSize = True
        Me.Label72.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label72.ForeColor = System.Drawing.Color.Black
        Me.Label72.Location = New System.Drawing.Point(347, 307)
        Me.Label72.Name = "Label72"
        Me.Label72.Size = New System.Drawing.Size(35, 13)
        Me.Label72.TabIndex = 42
        Me.Label72.Text = "Prod6"
        '
        'TextBox_pengProd1
        '
        Me.TextBox_pengProd1.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.TextBox_pengProd1.ForeColor = System.Drawing.Color.Black
        Me.TextBox_pengProd1.Location = New System.Drawing.Point(124, 53)
        Me.TextBox_pengProd1.MaxLength = 3
        Me.TextBox_pengProd1.Name = "TextBox_pengProd1"
        Me.TextBox_pengProd1.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_pengProd1.TabIndex = 10
        '
        'Label_hurKetProd3
        '
        Me.Label_hurKetProd3.AutoSize = True
        Me.Label_hurKetProd3.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label_hurKetProd3.ForeColor = System.Drawing.Color.Black
        Me.Label_hurKetProd3.Location = New System.Drawing.Point(233, 382)
        Me.Label_hurKetProd3.Name = "Label_hurKetProd3"
        Me.Label_hurKetProd3.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurKetProd3.TabIndex = 27
        '
        'Label_hurKetProd10
        '
        Me.Label_hurKetProd10.AutoSize = True
        Me.Label_hurKetProd10.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label_hurKetProd10.ForeColor = System.Drawing.Color.Black
        Me.Label_hurKetProd10.Location = New System.Drawing.Point(1269, 102)
        Me.Label_hurKetProd10.Name = "Label_hurKetProd10"
        Me.Label_hurKetProd10.Size = New System.Drawing.Size(37, 13)
        Me.Label_hurKetProd10.TabIndex = 76
        Me.Label_hurKetProd10.Text = "-huruf-"
        Me.Label_hurKetProd10.Visible = False
        '
        'Label_hurPengProd3
        '
        Me.Label_hurPengProd3.AutoSize = True
        Me.Label_hurPengProd3.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label_hurPengProd3.ForeColor = System.Drawing.Color.Black
        Me.Label_hurPengProd3.Location = New System.Drawing.Point(233, 343)
        Me.Label_hurPengProd3.Name = "Label_hurPengProd3"
        Me.Label_hurPengProd3.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurPengProd3.TabIndex = 26
        '
        'TextBox_ketProd1
        '
        Me.TextBox_ketProd1.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.TextBox_ketProd1.ForeColor = System.Drawing.Color.Black
        Me.TextBox_ketProd1.Location = New System.Drawing.Point(124, 92)
        Me.TextBox_ketProd1.MaxLength = 3
        Me.TextBox_ketProd1.Name = "TextBox_ketProd1"
        Me.TextBox_ketProd1.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_ketProd1.TabIndex = 11
        '
        'TextBox_ketProd3
        '
        Me.TextBox_ketProd3.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.TextBox_ketProd3.ForeColor = System.Drawing.Color.Black
        Me.TextBox_ketProd3.Location = New System.Drawing.Point(125, 379)
        Me.TextBox_ketProd3.MaxLength = 3
        Me.TextBox_ketProd3.Name = "TextBox_ketProd3"
        Me.TextBox_ketProd3.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_ketProd3.TabIndex = 25
        '
        'Label_hurPengProd10
        '
        Me.Label_hurPengProd10.AutoSize = True
        Me.Label_hurPengProd10.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label_hurPengProd10.ForeColor = System.Drawing.Color.Black
        Me.Label_hurPengProd10.Location = New System.Drawing.Point(1269, 63)
        Me.Label_hurPengProd10.Name = "Label_hurPengProd10"
        Me.Label_hurPengProd10.Size = New System.Drawing.Size(37, 13)
        Me.Label_hurPengProd10.TabIndex = 75
        Me.Label_hurPengProd10.Text = "-huruf-"
        Me.Label_hurPengProd10.Visible = False
        '
        'TextBox_pengProd3
        '
        Me.TextBox_pengProd3.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.TextBox_pengProd3.ForeColor = System.Drawing.Color.Black
        Me.TextBox_pengProd3.Location = New System.Drawing.Point(125, 340)
        Me.TextBox_pengProd3.MaxLength = 3
        Me.TextBox_pengProd3.Name = "TextBox_pengProd3"
        Me.TextBox_pengProd3.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_pengProd3.TabIndex = 24
        '
        'Label_hurPengProd1
        '
        Me.Label_hurPengProd1.AutoSize = True
        Me.Label_hurPengProd1.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label_hurPengProd1.ForeColor = System.Drawing.Color.Black
        Me.Label_hurPengProd1.Location = New System.Drawing.Point(232, 56)
        Me.Label_hurPengProd1.Name = "Label_hurPengProd1"
        Me.Label_hurPengProd1.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurPengProd1.TabIndex = 12
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label59.ForeColor = System.Drawing.Color.Black
        Me.Label59.Location = New System.Drawing.Point(21, 382)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(68, 13)
        Me.Label59.TabIndex = 23
        Me.Label59.Text = "Keterampilan"
        '
        'TextBox_ketProd10
        '
        Me.TextBox_ketProd10.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.TextBox_ketProd10.ForeColor = System.Drawing.Color.Black
        Me.TextBox_ketProd10.Location = New System.Drawing.Point(1161, 99)
        Me.TextBox_ketProd10.Name = "TextBox_ketProd10"
        Me.TextBox_ketProd10.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_ketProd10.TabIndex = 74
        Me.TextBox_ketProd10.Visible = False
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label60.ForeColor = System.Drawing.Color.Black
        Me.Label60.Location = New System.Drawing.Point(21, 343)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(71, 13)
        Me.Label60.TabIndex = 22
        Me.Label60.Text = "Pengetahuan"
        '
        'Label_hurKetProd1
        '
        Me.Label_hurKetProd1.AutoSize = True
        Me.Label_hurKetProd1.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label_hurKetProd1.ForeColor = System.Drawing.Color.Black
        Me.Label_hurKetProd1.Location = New System.Drawing.Point(232, 95)
        Me.Label_hurKetProd1.Name = "Label_hurKetProd1"
        Me.Label_hurKetProd1.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurKetProd1.TabIndex = 13
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label61.ForeColor = System.Drawing.Color.Black
        Me.Label61.Location = New System.Drawing.Point(21, 304)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(35, 13)
        Me.Label61.TabIndex = 21
        Me.Label61.Text = "Prod3"
        '
        'TextBox_pengProd10
        '
        Me.TextBox_pengProd10.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.TextBox_pengProd10.ForeColor = System.Drawing.Color.Black
        Me.TextBox_pengProd10.Location = New System.Drawing.Point(1161, 60)
        Me.TextBox_pengProd10.Name = "TextBox_pengProd10"
        Me.TextBox_pengProd10.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_pengProd10.TabIndex = 73
        Me.TextBox_pengProd10.Visible = False
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label58.ForeColor = System.Drawing.Color.Black
        Me.Label58.Location = New System.Drawing.Point(23, 160)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(35, 13)
        Me.Label58.TabIndex = 14
        Me.Label58.Text = "Prod2"
        '
        'Label80
        '
        Me.Label80.AutoSize = True
        Me.Label80.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label80.ForeColor = System.Drawing.Color.Black
        Me.Label80.Location = New System.Drawing.Point(1057, 102)
        Me.Label80.Name = "Label80"
        Me.Label80.Size = New System.Drawing.Size(68, 13)
        Me.Label80.TabIndex = 72
        Me.Label80.Text = "Keterampilan"
        Me.Label80.Visible = False
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label57.ForeColor = System.Drawing.Color.Black
        Me.Label57.Location = New System.Drawing.Point(23, 199)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(71, 13)
        Me.Label57.TabIndex = 15
        Me.Label57.Text = "Pengetahuan"
        '
        'Label81
        '
        Me.Label81.AutoSize = True
        Me.Label81.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label81.ForeColor = System.Drawing.Color.Black
        Me.Label81.Location = New System.Drawing.Point(1057, 63)
        Me.Label81.Name = "Label81"
        Me.Label81.Size = New System.Drawing.Size(71, 13)
        Me.Label81.TabIndex = 71
        Me.Label81.Text = "Pengetahuan"
        Me.Label81.Visible = False
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label56.ForeColor = System.Drawing.Color.Black
        Me.Label56.Location = New System.Drawing.Point(23, 238)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(68, 13)
        Me.Label56.TabIndex = 16
        Me.Label56.Text = "Keterampilan"
        '
        'Label82
        '
        Me.Label82.AutoSize = True
        Me.Label82.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label82.ForeColor = System.Drawing.Color.Black
        Me.Label82.Location = New System.Drawing.Point(1057, 24)
        Me.Label82.Name = "Label82"
        Me.Label82.Size = New System.Drawing.Size(41, 13)
        Me.Label82.TabIndex = 70
        Me.Label82.Text = "Prod10"
        Me.Label82.Visible = False
        '
        'TextBox_pengProd2
        '
        Me.TextBox_pengProd2.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.TextBox_pengProd2.ForeColor = System.Drawing.Color.Black
        Me.TextBox_pengProd2.Location = New System.Drawing.Point(127, 196)
        Me.TextBox_pengProd2.MaxLength = 3
        Me.TextBox_pengProd2.Name = "TextBox_pengProd2"
        Me.TextBox_pengProd2.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_pengProd2.TabIndex = 17
        '
        'TextBox_ketProd2
        '
        Me.TextBox_ketProd2.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.TextBox_ketProd2.ForeColor = System.Drawing.Color.Black
        Me.TextBox_ketProd2.Location = New System.Drawing.Point(127, 235)
        Me.TextBox_ketProd2.MaxLength = 3
        Me.TextBox_ketProd2.Name = "TextBox_ketProd2"
        Me.TextBox_ketProd2.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_ketProd2.TabIndex = 18
        '
        'Label_hurPengProd2
        '
        Me.Label_hurPengProd2.AutoSize = True
        Me.Label_hurPengProd2.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label_hurPengProd2.ForeColor = System.Drawing.Color.Black
        Me.Label_hurPengProd2.Location = New System.Drawing.Point(235, 199)
        Me.Label_hurPengProd2.Name = "Label_hurPengProd2"
        Me.Label_hurPengProd2.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurPengProd2.TabIndex = 19
        '
        'Label_hurKetProd2
        '
        Me.Label_hurKetProd2.AutoSize = True
        Me.Label_hurKetProd2.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label_hurKetProd2.ForeColor = System.Drawing.Color.Black
        Me.Label_hurKetProd2.Location = New System.Drawing.Point(235, 238)
        Me.Label_hurKetProd2.Name = "Label_hurKetProd2"
        Me.Label_hurKetProd2.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurKetProd2.TabIndex = 20
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label64.ForeColor = System.Drawing.Color.Black
        Me.Label64.Location = New System.Drawing.Point(346, 20)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(35, 13)
        Me.Label64.TabIndex = 28
        Me.Label64.Text = "Prod4"
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label63.ForeColor = System.Drawing.Color.Black
        Me.Label63.Location = New System.Drawing.Point(346, 59)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(71, 13)
        Me.Label63.TabIndex = 29
        Me.Label63.Text = "Pengetahuan"
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label62.ForeColor = System.Drawing.Color.Black
        Me.Label62.Location = New System.Drawing.Point(346, 98)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(68, 13)
        Me.Label62.TabIndex = 30
        Me.Label62.Text = "Keterampilan"
        '
        'TextBox_pengProd4
        '
        Me.TextBox_pengProd4.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.TextBox_pengProd4.ForeColor = System.Drawing.Color.Black
        Me.TextBox_pengProd4.Location = New System.Drawing.Point(450, 56)
        Me.TextBox_pengProd4.MaxLength = 3
        Me.TextBox_pengProd4.Name = "TextBox_pengProd4"
        Me.TextBox_pengProd4.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_pengProd4.TabIndex = 31
        '
        'Label_hurKetProd8
        '
        Me.Label_hurKetProd8.AutoSize = True
        Me.Label_hurKetProd8.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label_hurKetProd8.ForeColor = System.Drawing.Color.Black
        Me.Label_hurKetProd8.Location = New System.Drawing.Point(897, 241)
        Me.Label_hurKetProd8.Name = "Label_hurKetProd8"
        Me.Label_hurKetProd8.Size = New System.Drawing.Size(37, 13)
        Me.Label_hurKetProd8.TabIndex = 62
        Me.Label_hurKetProd8.Text = "-huruf-"
        Me.Label_hurKetProd8.Visible = False
        '
        'TextBox_ketProd4
        '
        Me.TextBox_ketProd4.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.TextBox_ketProd4.ForeColor = System.Drawing.Color.Black
        Me.TextBox_ketProd4.Location = New System.Drawing.Point(450, 95)
        Me.TextBox_ketProd4.MaxLength = 3
        Me.TextBox_ketProd4.Name = "TextBox_ketProd4"
        Me.TextBox_ketProd4.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_ketProd4.TabIndex = 32
        '
        'Label_hurPengProd8
        '
        Me.Label_hurPengProd8.AutoSize = True
        Me.Label_hurPengProd8.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label_hurPengProd8.ForeColor = System.Drawing.Color.Black
        Me.Label_hurPengProd8.Location = New System.Drawing.Point(897, 202)
        Me.Label_hurPengProd8.Name = "Label_hurPengProd8"
        Me.Label_hurPengProd8.Size = New System.Drawing.Size(37, 13)
        Me.Label_hurPengProd8.TabIndex = 61
        Me.Label_hurPengProd8.Text = "-huruf-"
        Me.Label_hurPengProd8.Visible = False
        '
        'Label_hurPengProd4
        '
        Me.Label_hurPengProd4.AutoSize = True
        Me.Label_hurPengProd4.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label_hurPengProd4.ForeColor = System.Drawing.Color.Black
        Me.Label_hurPengProd4.Location = New System.Drawing.Point(558, 59)
        Me.Label_hurPengProd4.Name = "Label_hurPengProd4"
        Me.Label_hurPengProd4.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurPengProd4.TabIndex = 33
        '
        'TextBox_ketProd8
        '
        Me.TextBox_ketProd8.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.TextBox_ketProd8.ForeColor = System.Drawing.Color.Black
        Me.TextBox_ketProd8.Location = New System.Drawing.Point(789, 238)
        Me.TextBox_ketProd8.Name = "TextBox_ketProd8"
        Me.TextBox_ketProd8.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_ketProd8.TabIndex = 60
        Me.TextBox_ketProd8.Visible = False
        '
        'Label_hurKetProd4
        '
        Me.Label_hurKetProd4.AutoSize = True
        Me.Label_hurKetProd4.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label_hurKetProd4.ForeColor = System.Drawing.Color.Black
        Me.Label_hurKetProd4.Location = New System.Drawing.Point(558, 98)
        Me.Label_hurKetProd4.Name = "Label_hurKetProd4"
        Me.Label_hurKetProd4.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurKetProd4.TabIndex = 34
        '
        'TextBox_pengProd8
        '
        Me.TextBox_pengProd8.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.TextBox_pengProd8.ForeColor = System.Drawing.Color.Black
        Me.TextBox_pengProd8.Location = New System.Drawing.Point(789, 199)
        Me.TextBox_pengProd8.Name = "TextBox_pengProd8"
        Me.TextBox_pengProd8.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_pengProd8.TabIndex = 59
        Me.TextBox_pengProd8.Visible = False
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label67.ForeColor = System.Drawing.Color.Black
        Me.Label67.Location = New System.Drawing.Point(346, 163)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(35, 13)
        Me.Label67.TabIndex = 35
        Me.Label67.Text = "Prod5"
        '
        'Label74
        '
        Me.Label74.AutoSize = True
        Me.Label74.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label74.ForeColor = System.Drawing.Color.Black
        Me.Label74.Location = New System.Drawing.Point(685, 241)
        Me.Label74.Name = "Label74"
        Me.Label74.Size = New System.Drawing.Size(68, 13)
        Me.Label74.TabIndex = 58
        Me.Label74.Text = "Keterampilan"
        Me.Label74.Visible = False
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label66.ForeColor = System.Drawing.Color.Black
        Me.Label66.Location = New System.Drawing.Point(346, 202)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(71, 13)
        Me.Label66.TabIndex = 36
        Me.Label66.Text = "Pengetahuan"
        '
        'Label75
        '
        Me.Label75.AutoSize = True
        Me.Label75.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label75.ForeColor = System.Drawing.Color.Black
        Me.Label75.Location = New System.Drawing.Point(685, 202)
        Me.Label75.Name = "Label75"
        Me.Label75.Size = New System.Drawing.Size(71, 13)
        Me.Label75.TabIndex = 57
        Me.Label75.Text = "Pengetahuan"
        Me.Label75.Visible = False
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label65.ForeColor = System.Drawing.Color.Black
        Me.Label65.Location = New System.Drawing.Point(346, 241)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(68, 13)
        Me.Label65.TabIndex = 37
        Me.Label65.Text = "Keterampilan"
        '
        'Label76
        '
        Me.Label76.AutoSize = True
        Me.Label76.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label76.ForeColor = System.Drawing.Color.Black
        Me.Label76.Location = New System.Drawing.Point(685, 163)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(35, 13)
        Me.Label76.TabIndex = 56
        Me.Label76.Text = "Prod8"
        Me.Label76.Visible = False
        '
        'TextBox_pengProd5
        '
        Me.TextBox_pengProd5.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.TextBox_pengProd5.ForeColor = System.Drawing.Color.Black
        Me.TextBox_pengProd5.Location = New System.Drawing.Point(450, 199)
        Me.TextBox_pengProd5.MaxLength = 3
        Me.TextBox_pengProd5.Name = "TextBox_pengProd5"
        Me.TextBox_pengProd5.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_pengProd5.TabIndex = 38
        '
        'Label_hurKetProd7
        '
        Me.Label_hurKetProd7.AutoSize = True
        Me.Label_hurKetProd7.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label_hurKetProd7.ForeColor = System.Drawing.Color.Black
        Me.Label_hurKetProd7.Location = New System.Drawing.Point(897, 102)
        Me.Label_hurKetProd7.Name = "Label_hurKetProd7"
        Me.Label_hurKetProd7.Size = New System.Drawing.Size(37, 13)
        Me.Label_hurKetProd7.TabIndex = 55
        Me.Label_hurKetProd7.Text = "-huruf-"
        Me.Label_hurKetProd7.Visible = False
        '
        'TextBox_ketProd5
        '
        Me.TextBox_ketProd5.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.TextBox_ketProd5.ForeColor = System.Drawing.Color.Black
        Me.TextBox_ketProd5.Location = New System.Drawing.Point(450, 238)
        Me.TextBox_ketProd5.MaxLength = 3
        Me.TextBox_ketProd5.Name = "TextBox_ketProd5"
        Me.TextBox_ketProd5.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_ketProd5.TabIndex = 39
        '
        'Label_hurPengProd7
        '
        Me.Label_hurPengProd7.AutoSize = True
        Me.Label_hurPengProd7.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label_hurPengProd7.ForeColor = System.Drawing.Color.Black
        Me.Label_hurPengProd7.Location = New System.Drawing.Point(897, 63)
        Me.Label_hurPengProd7.Name = "Label_hurPengProd7"
        Me.Label_hurPengProd7.Size = New System.Drawing.Size(37, 13)
        Me.Label_hurPengProd7.TabIndex = 54
        Me.Label_hurPengProd7.Text = "-huruf-"
        Me.Label_hurPengProd7.Visible = False
        '
        'Label_hurPengProd5
        '
        Me.Label_hurPengProd5.AutoSize = True
        Me.Label_hurPengProd5.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label_hurPengProd5.ForeColor = System.Drawing.Color.Black
        Me.Label_hurPengProd5.Location = New System.Drawing.Point(558, 202)
        Me.Label_hurPengProd5.Name = "Label_hurPengProd5"
        Me.Label_hurPengProd5.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurPengProd5.TabIndex = 40
        '
        'TextBox_ketProd7
        '
        Me.TextBox_ketProd7.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.TextBox_ketProd7.ForeColor = System.Drawing.Color.Black
        Me.TextBox_ketProd7.Location = New System.Drawing.Point(789, 99)
        Me.TextBox_ketProd7.Name = "TextBox_ketProd7"
        Me.TextBox_ketProd7.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_ketProd7.TabIndex = 53
        Me.TextBox_ketProd7.Visible = False
        '
        'Label_hurKetProd5
        '
        Me.Label_hurKetProd5.AutoSize = True
        Me.Label_hurKetProd5.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label_hurKetProd5.ForeColor = System.Drawing.Color.Black
        Me.Label_hurKetProd5.Location = New System.Drawing.Point(558, 241)
        Me.Label_hurKetProd5.Name = "Label_hurKetProd5"
        Me.Label_hurKetProd5.Size = New System.Drawing.Size(0, 13)
        Me.Label_hurKetProd5.TabIndex = 41
        '
        'TextBox_pengProd7
        '
        Me.TextBox_pengProd7.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.TextBox_pengProd7.ForeColor = System.Drawing.Color.Black
        Me.TextBox_pengProd7.Location = New System.Drawing.Point(789, 60)
        Me.TextBox_pengProd7.Name = "TextBox_pengProd7"
        Me.TextBox_pengProd7.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_pengProd7.TabIndex = 52
        Me.TextBox_pengProd7.Visible = False
        '
        'Label73
        '
        Me.Label73.AutoSize = True
        Me.Label73.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label73.ForeColor = System.Drawing.Color.Black
        Me.Label73.Location = New System.Drawing.Point(685, 24)
        Me.Label73.Name = "Label73"
        Me.Label73.Size = New System.Drawing.Size(35, 13)
        Me.Label73.TabIndex = 49
        Me.Label73.Text = "Prod7"
        Me.Label73.Visible = False
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label68.ForeColor = System.Drawing.Color.Black
        Me.Label68.Location = New System.Drawing.Point(685, 102)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(68, 13)
        Me.Label68.TabIndex = 51
        Me.Label68.Text = "Keterampilan"
        Me.Label68.Visible = False
        '
        'Label69
        '
        Me.Label69.AutoSize = True
        Me.Label69.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label69.ForeColor = System.Drawing.Color.Black
        Me.Label69.Location = New System.Drawing.Point(685, 63)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(71, 13)
        Me.Label69.TabIndex = 50
        Me.Label69.Text = "Pengetahuan"
        Me.Label69.Visible = False
        '
        'MetroTabPage_supp
        '
        Me.MetroTabPage_supp.AutoScroll = True
        Me.MetroTabPage_supp.Controls.Add(Me.MetroComboBox_ekskul)
        Me.MetroTabPage_supp.Controls.Add(Me.Label_kar)
        Me.MetroTabPage_supp.Controls.Add(Me.Label95)
        Me.MetroTabPage_supp.Controls.Add(Me.TextBox_komen)
        Me.MetroTabPage_supp.Controls.Add(Me.Label94)
        Me.MetroTabPage_supp.Controls.Add(Me.Label88)
        Me.MetroTabPage_supp.Controls.Add(Me.TextBox_alpha)
        Me.MetroTabPage_supp.Controls.Add(Me.Label86)
        Me.MetroTabPage_supp.Controls.Add(Me.TextBox_izin)
        Me.MetroTabPage_supp.Controls.Add(Me.Label52)
        Me.MetroTabPage_supp.Controls.Add(Me.TextBox_sakit)
        Me.MetroTabPage_supp.Controls.Add(Me.Label45)
        Me.MetroTabPage_supp.HorizontalScrollbar = True
        Me.MetroTabPage_supp.HorizontalScrollbarBarColor = True
        Me.MetroTabPage_supp.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroTabPage_supp.HorizontalScrollbarSize = 10
        Me.MetroTabPage_supp.Location = New System.Drawing.Point(4, 38)
        Me.MetroTabPage_supp.Name = "MetroTabPage_supp"
        Me.MetroTabPage_supp.Size = New System.Drawing.Size(1042, 360)
        Me.MetroTabPage_supp.TabIndex = 2
        Me.MetroTabPage_supp.Text = "Data Pendukung"
        Me.MetroTabPage_supp.VerticalScrollbar = True
        Me.MetroTabPage_supp.VerticalScrollbarBarColor = True
        Me.MetroTabPage_supp.VerticalScrollbarHighlightOnWheel = False
        Me.MetroTabPage_supp.VerticalScrollbarSize = 10
        '
        'MetroComboBox_ekskul
        '
        Me.MetroComboBox_ekskul.FormattingEnabled = True
        Me.MetroComboBox_ekskul.ItemHeight = 23
        Me.MetroComboBox_ekskul.Items.AddRange(New Object() {"Paskibra", "PMR", "Pramuka", "Rohis", "Rokris", "English Club", "Japanese Club", "Voli", "Futsal", "Pencak Silat", "Basket", "Taekwondo", "Musik"})
        Me.MetroComboBox_ekskul.Location = New System.Drawing.Point(22, 281)
        Me.MetroComboBox_ekskul.Name = "MetroComboBox_ekskul"
        Me.MetroComboBox_ekskul.Size = New System.Drawing.Size(125, 29)
        Me.MetroComboBox_ekskul.TabIndex = 98
        Me.MetroComboBox_ekskul.UseSelectable = True
        '
        'Label_kar
        '
        Me.Label_kar.AutoSize = True
        Me.Label_kar.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label_kar.ForeColor = System.Drawing.Color.Black
        Me.Label_kar.Location = New System.Drawing.Point(339, 262)
        Me.Label_kar.Name = "Label_kar"
        Me.Label_kar.Size = New System.Drawing.Size(13, 13)
        Me.Label_kar.TabIndex = 97
        Me.Label_kar.Text = "0"
        '
        'Label95
        '
        Me.Label95.AutoSize = True
        Me.Label95.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label95.ForeColor = System.Drawing.Color.Black
        Me.Label95.Location = New System.Drawing.Point(248, 262)
        Me.Label95.Name = "Label95"
        Me.Label95.Size = New System.Drawing.Size(79, 13)
        Me.Label95.TabIndex = 96
        Me.Label95.Text = "Sisa Karakter : "
        '
        'TextBox_komen
        '
        Me.TextBox_komen.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.TextBox_komen.ForeColor = System.Drawing.Color.Black
        Me.TextBox_komen.Location = New System.Drawing.Point(252, 62)
        Me.TextBox_komen.MaxLength = 150
        Me.TextBox_komen.Multiline = True
        Me.TextBox_komen.Name = "TextBox_komen"
        Me.TextBox_komen.Size = New System.Drawing.Size(347, 194)
        Me.TextBox_komen.TabIndex = 95
        '
        'Label94
        '
        Me.Label94.AutoSize = True
        Me.Label94.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label94.ForeColor = System.Drawing.Color.Black
        Me.Label94.Location = New System.Drawing.Point(248, 21)
        Me.Label94.Name = "Label94"
        Me.Label94.Size = New System.Drawing.Size(69, 26)
        Me.Label94.TabIndex = 94
        Me.Label94.Text = "Capaian Dan" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Hasil Belajar"
        '
        'Label88
        '
        Me.Label88.AutoSize = True
        Me.Label88.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label88.ForeColor = System.Drawing.Color.Black
        Me.Label88.Location = New System.Drawing.Point(20, 21)
        Me.Label88.Name = "Label88"
        Me.Label88.Size = New System.Drawing.Size(89, 13)
        Me.Label88.TabIndex = 84
        Me.Label88.Text = "Keterangan Sakit"
        '
        'TextBox_alpha
        '
        Me.TextBox_alpha.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.TextBox_alpha.ForeColor = System.Drawing.Color.Black
        Me.TextBox_alpha.Location = New System.Drawing.Point(21, 191)
        Me.TextBox_alpha.Name = "TextBox_alpha"
        Me.TextBox_alpha.Size = New System.Drawing.Size(125, 20)
        Me.TextBox_alpha.TabIndex = 91
        '
        'Label86
        '
        Me.Label86.AutoSize = True
        Me.Label86.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label86.ForeColor = System.Drawing.Color.Black
        Me.Label86.Location = New System.Drawing.Point(17, 240)
        Me.Label86.Name = "Label86"
        Me.Label86.Size = New System.Drawing.Size(77, 26)
        Me.Label86.TabIndex = 92
        Me.Label86.Text = "Kegiatan" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Ekstrakurikuler"
        '
        'TextBox_izin
        '
        Me.TextBox_izin.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.TextBox_izin.ForeColor = System.Drawing.Color.Black
        Me.TextBox_izin.Location = New System.Drawing.Point(22, 115)
        Me.TextBox_izin.Name = "TextBox_izin"
        Me.TextBox_izin.Size = New System.Drawing.Size(125, 20)
        Me.TextBox_izin.TabIndex = 89
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label52.ForeColor = System.Drawing.Color.Black
        Me.Label52.Location = New System.Drawing.Point(17, 169)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(92, 13)
        Me.Label52.TabIndex = 90
        Me.Label52.Text = "Keterangan Alpha"
        '
        'TextBox_sakit
        '
        Me.TextBox_sakit.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.TextBox_sakit.ForeColor = System.Drawing.Color.Black
        Me.TextBox_sakit.Location = New System.Drawing.Point(24, 43)
        Me.TextBox_sakit.Name = "TextBox_sakit"
        Me.TextBox_sakit.Size = New System.Drawing.Size(125, 20)
        Me.TextBox_sakit.TabIndex = 87
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label45.ForeColor = System.Drawing.Color.Black
        Me.Label45.Location = New System.Drawing.Point(21, 93)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(81, 13)
        Me.Label45.TabIndex = 88
        Me.Label45.Text = "Keterangan Izin"
        '
        'grade_form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.DoubleBuffered = True
        Me.Name = "grade_form"
        Me.Size = New System.Drawing.Size(1050, 600)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.MetroTabControl1.ResumeLayout(False)
        Me.MetroTabPage_mapelNA.ResumeLayout(False)
        Me.MetroTabPage_mapelNA.PerformLayout()
        Me.MetroTabPage_mapelProd.ResumeLayout(False)
        Me.MetroTabPage_mapelProd.PerformLayout()
        Me.MetroTabPage_supp.ResumeLayout(False)
        Me.MetroTabPage_supp.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Button_nextItem As Button
    Friend WithEvents Label_nis As Label
    Friend WithEvents Button_backItem As Button
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents TextBox_searchNis As TextBox
    Friend WithEvents Label_hurKetPai As Label
    Friend WithEvents Label_hurPengPai As Label
    Friend WithEvents TextBox_ketPai As TextBox
    Friend WithEvents TextBox_pengPai As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Label_hurKetPkn As Label
    Friend WithEvents Label_hurPengPkn As Label
    Friend WithEvents TextBox_ketPkn As TextBox
    Friend WithEvents TextBox_pengPkn As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label_hurKetMtk As Label
    Friend WithEvents Label_hurPengMtk As Label
    Friend WithEvents TextBox_ketMtk As TextBox
    Friend WithEvents TextBox_pengMtk As TextBox
    Friend WithEvents Label16 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents Label_hurKetIndo As Label
    Friend WithEvents Label_hurPengIndo As Label
    Friend WithEvents TextBox_ketIndo As TextBox
    Friend WithEvents TextBox_pengIndo As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label_hurKetIng As Label
    Friend WithEvents Label_hurPengIng As Label
    Friend WithEvents TextBox_ketIng As TextBox
    Friend WithEvents TextBox_pengIng As TextBox
    Friend WithEvents Label26 As Label
    Friend WithEvents Label27 As Label
    Friend WithEvents Label28 As Label
    Friend WithEvents Label_hurKetSej As Label
    Friend WithEvents Label_hurPengSej As Label
    Friend WithEvents TextBox_ketSej As TextBox
    Friend WithEvents TextBox_pengSej As TextBox
    Friend WithEvents Label21 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents Label_hurKetSbk As Label
    Friend WithEvents Label_hurPengSbk As Label
    Friend WithEvents TextBox_ketSbk As TextBox
    Friend WithEvents TextBox_pengSbk As TextBox
    Friend WithEvents Label31 As Label
    Friend WithEvents Label32 As Label
    Friend WithEvents Label33 As Label
    Friend WithEvents Label_hurKetPen As Label
    Friend WithEvents Label_hurPengPen As Label
    Friend WithEvents TextBox_ketPen As TextBox
    Friend WithEvents TextBox_pengPen As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label_hurKetKwh As Label
    Friend WithEvents Label_hurPengKwh As Label
    Friend WithEvents TextBox_ketKwh As TextBox
    Friend WithEvents TextBox_pengKwh As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents Label_hurKetMul As Label
    Friend WithEvents Label_hurPengMul As Label
    Friend WithEvents TextBox_ketMul As TextBox
    Friend WithEvents TextBox_pengMul As TextBox
    Friend WithEvents Label24 As Label
    Friend WithEvents Label25 As Label
    Friend WithEvents Label29 As Label
    Friend WithEvents Label_hurKetFis As Label
    Friend WithEvents Label_hurPengFis As Label
    Friend WithEvents TextBox_ketFis As TextBox
    Friend WithEvents TextBox_pengFis As TextBox
    Friend WithEvents Label36 As Label
    Friend WithEvents Label37 As Label
    Friend WithEvents Label38 As Label
    Friend WithEvents Label_hurKetKim As Label
    Friend WithEvents Label_hurPengKim As Label
    Friend WithEvents TextBox_ketKim As TextBox
    Friend WithEvents TextBox_pengKim As TextBox
    Friend WithEvents Label39 As Label
    Friend WithEvents Label40 As Label
    Friend WithEvents Label41 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label46 As Label
    Friend WithEvents Label47 As Label
    Friend WithEvents Label42 As Label
    Friend WithEvents Label_namaSiswa As Label
    Friend WithEvents Label43 As Label
    Friend WithEvents Label44 As Label
    Friend WithEvents Label30 As Label
    Friend WithEvents Label34 As Label
    Friend WithEvents Label35 As Label
    Friend WithEvents Label_smes As Label
    Friend WithEvents Label50 As Label
    Friend WithEvents Label51 As Label
    Friend WithEvents Label_kelas As Label
    Friend WithEvents Label48 As Label
    Friend WithEvents Label49 As Label
    Friend WithEvents Label_hurKetProd1 As Label
    Friend WithEvents Label_hurPengProd1 As Label
    Friend WithEvents TextBox_ketProd1 As TextBox
    Friend WithEvents TextBox_pengProd1 As TextBox
    Friend WithEvents Label53 As Label
    Friend WithEvents Label54 As Label
    Friend WithEvents Label55 As Label
    Friend WithEvents Label_hurKetProd2 As Label
    Friend WithEvents Label_hurPengProd2 As Label
    Friend WithEvents TextBox_ketProd2 As TextBox
    Friend WithEvents TextBox_pengProd2 As TextBox
    Friend WithEvents Label56 As Label
    Friend WithEvents Label57 As Label
    Friend WithEvents Label58 As Label
    Friend WithEvents Label_hurKetProd3 As Label
    Friend WithEvents Label_hurPengProd3 As Label
    Friend WithEvents TextBox_ketProd3 As TextBox
    Friend WithEvents TextBox_pengProd3 As TextBox
    Friend WithEvents Label59 As Label
    Friend WithEvents Label60 As Label
    Friend WithEvents Label61 As Label
    Friend WithEvents Label_hurKetProd4 As Label
    Friend WithEvents Label_hurPengProd4 As Label
    Friend WithEvents TextBox_ketProd4 As TextBox
    Friend WithEvents TextBox_pengProd4 As TextBox
    Friend WithEvents Label62 As Label
    Friend WithEvents Label63 As Label
    Friend WithEvents Label64 As Label
    Friend WithEvents Label_hurKetProd6 As Label
    Friend WithEvents Label_hurPengProd6 As Label
    Friend WithEvents TextBox_ketProd6 As TextBox
    Friend WithEvents TextBox_pengProd6 As TextBox
    Friend WithEvents Label70 As Label
    Friend WithEvents Label71 As Label
    Friend WithEvents Label72 As Label
    Friend WithEvents Label_hurKetProd5 As Label
    Friend WithEvents Label_hurPengProd5 As Label
    Friend WithEvents TextBox_ketProd5 As TextBox
    Friend WithEvents TextBox_pengProd5 As TextBox
    Friend WithEvents Label65 As Label
    Friend WithEvents Label66 As Label
    Friend WithEvents Label67 As Label
    Friend WithEvents Label_hurKetProd8 As Label
    Friend WithEvents Label_hurPengProd8 As Label
    Friend WithEvents TextBox_ketProd8 As TextBox
    Friend WithEvents TextBox_pengProd8 As TextBox
    Friend WithEvents Label74 As Label
    Friend WithEvents Label75 As Label
    Friend WithEvents Label76 As Label
    Friend WithEvents Label_hurKetProd7 As Label
    Friend WithEvents Label_hurPengProd7 As Label
    Friend WithEvents TextBox_ketProd7 As TextBox
    Friend WithEvents TextBox_pengProd7 As TextBox
    Friend WithEvents Label68 As Label
    Friend WithEvents Label69 As Label
    Friend WithEvents Label73 As Label
    Friend WithEvents Label_hurKetProd9 As Label
    Friend WithEvents Label_hurPengProd9 As Label
    Friend WithEvents TextBox_ketProd9 As TextBox
    Friend WithEvents TextBox_pengProd9 As TextBox
    Friend WithEvents Label77 As Label
    Friend WithEvents Label78 As Label
    Friend WithEvents Label79 As Label
    Friend WithEvents Label_hurKetProd10 As Label
    Friend WithEvents Label_hurPengProd10 As Label
    Friend WithEvents TextBox_ketProd10 As TextBox
    Friend WithEvents TextBox_pengProd10 As TextBox
    Friend WithEvents Label80 As Label
    Friend WithEvents Label81 As Label
    Friend WithEvents Label82 As Label
    Friend WithEvents Label_hurKetProd11 As Label
    Friend WithEvents Label_hurPengProd11 As Label
    Friend WithEvents TextBox_ketProd11 As TextBox
    Friend WithEvents TextBox_pengProd11 As TextBox
    Friend WithEvents Label83 As Label
    Friend WithEvents Label84 As Label
    Friend WithEvents Label85 As Label
    Friend WithEvents Label86 As Label
    Friend WithEvents TextBox_alpha As TextBox
    Friend WithEvents Label52 As Label
    Friend WithEvents TextBox_izin As TextBox
    Friend WithEvents Label45 As Label
    Friend WithEvents TextBox_sakit As TextBox
    Friend WithEvents Label88 As Label
    Friend WithEvents Label_thnPel As Label
    Friend WithEvents Label89 As Label
    Friend WithEvents Label90 As Label
    Friend WithEvents Label_jur As Label
    Friend WithEvents Label91 As Label
    Friend WithEvents Label92 As Label
    Friend WithEvents Timer1 As Timer
    Friend WithEvents Button_reset As Button
    Friend WithEvents Button_save As Button
    Friend WithEvents Label87 As Label
    Friend WithEvents Label93 As Label
    Friend WithEvents TextBox_komen As TextBox
    Friend WithEvents Label94 As Label
    Friend WithEvents Label_kar As Label
    Friend WithEvents Label95 As Label
    Friend WithEvents Button_lastItem As Button
    Friend WithEvents Button_firstItem As Button
    Friend WithEvents Label_nisSis As Label
    Friend WithEvents MetroComboBox_smes As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroTabControl1 As MetroFramework.Controls.MetroTabControl
    Friend WithEvents MetroTabPage_mapelNA As MetroFramework.Controls.MetroTabPage
    Friend WithEvents MetroTabPage_mapelProd As MetroFramework.Controls.MetroTabPage
    Friend WithEvents MetroTabPage_supp As MetroFramework.Controls.MetroTabPage
    Friend WithEvents MetroComboBox_ekskul As MetroFramework.Controls.MetroComboBox
End Class
